import scapy.all as scapy
from datetime import datetime, date, time

def compare_pcap(name, expected_in, received_in, itercount=1,
                 error_on_pause=True, exclude_bytes=None):
    """ Compares two pcaps. Prints and records differences. """
    if not (received_in or expected_in):
        return
    start = datetime.now()
    print("OUTPUT", "Comparing %s\n", name)
    if not expected_in:
        tar = True
    else:
        tar = False

    expected = expected_in
    received = received_in

    expected = [str(i) for i in expected]
    received = [str(i) for i in received]
    print("expected %d", len(expected))
    print("received %d", len(received))

    # Replace excluded bytes with 0xa5 filler
    if exclude_bytes:
        for i in range(len(expected)):
            for j in exclude_bytes:
                expected[i] = expected[i][:j] + '\xa5' + expected[i][j + 1:]
        for i in range(len(received)):
            for j in exclude_bytes:
                received[i] = received[i][:j] + '\xa5' + received[i][j + 1:]

    badones = []
    unexpected = []
    missing = expected * itercount
    orig = expected * itercount
    cnt=0
    for i in received:
        if i in missing:
            missing.remove(i)
        else:
            badones.append(cnt)
            unexpected.append(i)
        cnt=cnt+1

    print("unexpected %d", len(unexpected))
    print("missing %d", len(missing))

    # remove pause frames from unexpected
    pause_count = 0
    pause_start = '\x01\x80\xc2\x00\x00\x01\x00\x00\x00\x00\x00\x00\x88\x08\x00\x01'
    for i in range(len(unexpected)):
        if str(unexpected[i])[:16] == pause_start:
            #del unexpected[i]
            pause_count += 1

    # convert back to packets
    unexpected = [scapy.Packet(i) for i in unexpected]
    missing = [scapy.Packet(i) for i in missing]

    #unexpected[0].show()
    #missing[0].show()

    if pause_count > 0:
        print("OUTPUT", "DETECTED %d PAUSE FRAMES\n", pause_count)

    #  SUCCESS is here don't post logs
    if not (missing or unexpected):
        print("OUTPUT", "\t%s passes integrity check", name)
        print("DEBUG", '\t(comparison took %s)\n', str(datetime.now() - start))
        return missing, unexpected

    #print('%s-received.pcap' % name, received_in, tar)
    #print('%s-expected.pcap' % name, expected_in, tar)

    if missing:
        ##print('%s-missing.pcap' % name, missing, tar)
        print( 'Detected %d missing packet(s)' % len(missing))
    if ((error_on_pause and unexpected) or
        (not error_on_pause and len(unexpected) != pause_count)):
        #print('%s-unexpected.pcap' % name, unexpected, tar)
        print( 'Received %d unexpected packet(s)' % len(unexpected))

    fo = open('orig3.pcap1', 'w')
    fr = open('received3.pcap', 'w')
    fl = open('packet_info3.log', 'w')
    info="Index \t pkt expected length \t received length \t \n"
    fl.write(info)

    for p in badones:
        fo.write(orig[p])
        fr.write(received[p])
        info="%d \t %d \t %d \n" % (p, len(orig[p]), len(received[p]))
        fl.write(info)

    fo.close()
    fr.close()
    fl.close()

    return missing, unexpected


r=scapy.rdpcap("r3.pcap")
e=scapy.rdpcap("e3.pcap")

m,e = compare_pcap("wire", e,r,100,False,range(12))

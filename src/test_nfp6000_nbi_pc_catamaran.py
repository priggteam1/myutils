#! /usr/bin/env python
#==============================================================================
# Module: test_nfp6000_nbi_pc_catamaran.py
""" test_nfp6000_nbi_pc_catamaran """
#==============================================================================
from command import *
from hardware import *
import scapy.all as sc

#------------------------------------------------------------------------------
class test_nfp6000_nbi_pc_catamaran (TestV1):
    """ test_nfp6000_nbi_pc_catamaran """

    # Test configuration settings
    test_config = {}

    test_config["abort_on_error"]  = True
    test_config["capture_waves"]   = False
    test_config["hl_ttl"]          = [0, 1, 2, 64, 255]
    test_config["ipv4_da"]         = "10.10.10.10"
    test_config["ipv4_sa"]         = "20.20.20.20"
    test_config["ipv6_da"]         = "0000:1111:2222:3333:4444:5555:6666:7777"
    test_config["ipv6_sa"]         = "8888:9999:aaaa:bbbb:cccc:dddd:eeee:ffff"
    test_config["mac_da"]          = "00:01:02:03:04:05"
    test_config["mac_sa"]          = "00:aa:bb:cc:dd:ee"
    test_config["mpls_m_label"]    = 0xaaaaa
    test_config["mpls_m_oam"]      = 0xa
    test_config["mpls_u_label"]    = 0x55555
    test_config["mpls_u_oam"]      = 0x5
    # note: account for prepend in packet size
    test_config["pkt_sizes"]       = [60, 184, 185, 1976, 1977, 10168]
    test_config["pkts_per_iter"]   = 10
    test_config["prepend_len"]     = 0 # half-words, 0B
    test_config["show_counters"]   = False
    test_config["show_init"]       = False
    test_config["show_pds"]        = False
    test_config["show_scapy_pkts"] = False
    # note: set to True to use only minimum-sized packets
    test_config["small_pkts_only"] = True
    test_config["t1_mac_da"]       = "00:ff:dd:bb:99:77"
    test_config["t1_mac_sa"]       = "00:ee:cc:aa:88:66"
    test_config["tcp_dp"]          = 0xabcd
    test_config["tcp_sp"]          = 0x6789
    test_config["udp_dp"]          = 0xbeef
    test_config["udp_sp"]          = 0xfeed
    test_config["verbose"]         = False
    test_config["vlan"]            = 0x123
    test_config["vxlan_vni"]       = 0xeeeeee

    # Default expected values for the Catamaran NPFW metadata
    def_exp_metadata_val = {}

    def_exp_metadata_val["if_err"]       = 0
    def_exp_metadata_val["intrasystem"]  = 0
    def_exp_metadata_val["match"]        = 0
    def_exp_metadata_val["pol_flag"]     = 0
    def_exp_metadata_val["pol_op_id"]    = 0
    def_exp_metadata_val["seq_override"] = 0
    def_exp_metadata_val["syn_pol_flag"] = 0
    def_exp_metadata_val["tcam_tag"]     = 0
    def_exp_metadata_val["t_l2"]         = 0
    def_exp_metadata_val["t_l3"]         = 0
    def_exp_metadata_val["t_l4"]         = 0
    def_exp_metadata_val["valid"]        = 1

    def info(self):
        """ Info function """

        self.entity = ["nfp6000"]
        if self.prefix is not None:
            NFP = self.prefix
            nfp = tfentity(NFP)

            mac_config = nfp.parent.get_maccfg()
            self.param = paramize(product(mac_config))
        else:
            self.param = []
        self.testdep = []
        self.tags = ["AUTHOR_shong", "DUT_NBI_PC", "TYPE_FEAT", "PRIO_HIGH",
                     "STATE_READY", "MEI1_PRES", "MU_ISLAND1_PRES",
                     "NBII1_PRES"]
        self.repeat = 1

    def main(self, NFP,
             mac_config=None,          # 2x2 tuple: (('3x40GE','3x40GE'), ('3x40GE,'3x40GE'))
                                       #            None = platform default
             qid_config=None,          # 2xX list of integer QIDs (one list per NBI)
             mac_loopback_mode=None,   # None or "SYSTEM_PMA_LOOPBACK"
             mac_loopback_lanes=None,  # Only used if in mac_loopback_mode (None = all)
             mei_src_num=None,         # Tuple of ME islands for source
             mei_snk_num=None,         # Tuple of ME islands for sink
             nbii_num=None,            # Tuple of NBIs for traffic
             mui_num=None):            # Tuple of MUs for packet storage(ingress and egress)
        """ Packet Source Sink """

        err_cnt = 0
        nfp     = tfentity(NFP)
        rev     = nfp.rev.rstrip('0123456789')
        PKTSS   = NFP + "pktss_"
        pktss   = tfentity(PKTSS)

        l1_enc = { "Other": 0x0, "GFP": 0x1 }
        l2_enc = { "Other": 0x0, "ETH": 0x4, "ETH-PPPoE": 0x6 }
        l3_enc = { "Other": 0x0, "ARP":  0x1, "MPLS-Other": 0x2, "FCoE": 0x3,
                   "IPv4":  0x4, "IPv6": 0x5 }
        l4_enc = { "Other": 0x0, "UDP":       0x2, "TCP": 0x3, "NFF": 0x5,
                   "GRE":   0xd, "UDP-VXLAN": 0xe }

        # Set the L3 types for A0
        #
        # Note: For Harrier A0, an issue in the characterizer(THB-1324) causes
        #       the innerIncomplete flag to never clear once set.  Therefore,
        #       the "SHORT_IPv4" test case should be disabled until fixed in
        #       Harrier B0+ -- or else tested as the very last test iteration,
        #       since it will never clear w/ Harrier A0 w/o a reset.
        l3_types = ["ARP", "FCoE", "IPv4", "IPv6", "MPLS_M", "MPLS_U",
                    "PPPoED", "PPP_Bridge"]

        if rev >= "B":
            l3_types.append("SHORT_IPv4")

        # Suppress excess output, if desired
        if not self.test_config["verbose"]:
            stpush("DEBUG", "/dev/null")

        # Determine test configuration based on parameters and chip resources
        pktss_cfg_dict = { "mac_config"         : mac_config,
                           "qid_config"         : qid_config,
                           "mac_loopback_mode"  : mac_loopback_mode,
                           "mac_loopback_lanes" : mac_loopback_lanes,
                           "mei_src_num"        : mei_src_num,
                           "mei_snk_num"        : mei_snk_num,
                           "nbii_num"           : nbii_num,
                           "mui_num"            : mui_num }
        pktss_cfgtx_dict = { "mode" : "once", "packet_config" : None }
        pktss.config(pktss_cfg_dict, pktss_cfgtx_dict)
        cfg = portcfgr(PKTSS)

        # Turn on waves (optional)
        if self.test_config["capture_waves"]:
            pktss.waves(1)

        # Initialize hardware
        if not self.test_config["show_init"]:
            stpush("OUTPUT", "/dev/null")

        if pktss.initialize :
            pktss.init()
            self.nbi_pc_catamaran_init(pktss)

        # Build/Load microcode
        if pktss.loadme :
            elffile = pktss.buildapp()
            pktss.stop()
            pktss.load(elffile)

        if not self.test_config["show_init"]:
            stpop("OUTPUT")

        # Check Link
        if not portlinkd(PKTSS) :
            return

        # ***  Iterate through the protocol combinations  ***

        # Send packets with and without VLANs
        iter = 0

        num_hl_ttl = len(self.test_config["hl_ttl"])

        if ( self.test_config["small_pkts_only"] ) :
            nsizes = 1
        else :
            nsizes = len(self.test_config["pkt_sizes"])

        # Load packet settings for test
        ipv4_da   = self.test_config["ipv4_da"]
        ipv4_sa   = self.test_config["ipv4_sa"]
        ipv6_da   = self.test_config["ipv6_da"]
        ipv6_sa   = self.test_config["ipv6_sa"]
        mac_da    = self.test_config["mac_da"]
        mac_sa    = self.test_config["mac_sa"]
        mpls_mid  = self.test_config["mpls_m_label"]
        mpls_uid  = self.test_config["mpls_u_label"]
        npkts     = self.test_config["pkts_per_iter"]
        oam_mid   = self.test_config["mpls_m_oam"]
        oam_uid   = self.test_config["mpls_u_oam"]
        t1_mac_da = self.test_config["t1_mac_da"]
        t1_mac_sa = self.test_config["t1_mac_sa"]
        tcp_dp    = self.test_config["tcp_dp"]
        tcp_sp    = self.test_config["tcp_sp"]
        udp_dp    = self.test_config["udp_dp"]
        udp_sp    = self.test_config["udp_sp"]
        vlan      = self.test_config["vlan"]
        vxlan_vni = self.test_config["vxlan_vni"]

        # Go through L2 types
        exp_metadata_val = {}
        last_recv_cnt    = 0
        last_stat        = None

        for l2_type in ["BAD_GFP", "ETH", "LONG_ETH"] :
            # Configure the interfaces according to the L2 type
            if l2_type == "BAD_GFP" :
                # Configure all characterizer channels for PPP w/ prepends
                print("\n\n--- Configuring ports for GFP mode ---")
                self.set_port_cfg(pktss, self.test_config["prepend_len"], 0)
            elif l2_type in ["ETH", "LONG_ETH"] :
                # Configure all characterizer channels for Ethernet w/ prepends
                print("\n\n--- Configuring ports for ETH mode ---")
                self.set_port_cfg(pktss, self.test_config["prepend_len"], 1)

            # Go through L2 options
            for vlan_present in [False, True] :
                # Skip VLAN formats for bad Ethernet
                if ( (l2_type == "BAD_GFP") and vlan_present ) :
                    continue
                # Skip non-VLAN formats for long Ethernet
                elif ( (l2_type == "LONG_ETH") and (not vlan_present) ) :
                    continue


                # Go through L3 types
                for l3_type in l3_types :
                    # Skip most L3 types for bad or long Ethernet
                    if ( (l2_type in ["BAD_GFP", "LONG_ETH"]) and
                         (l3_type != "PPPoED") ) :
                        continue

                    # Go through L3 options
                    for frag_opt in ["NF", "FF", "MF", "LF"] :
                        # Skip other fragment options for non-IP
                        if ( (l3_type not in ["IPv4"]) and
                             (frag_opt != "NF") ) :
                            continue

                        # Go through L4 types
                        for l4_type in ["none", "TCP", "UDP", "GRE-ETH",
                                        "UDP-VXLAN-ETH", "ICMP"] :
                            # Skip if L4 type does not match with L3 type
                            if ( (l3_type not in ["IPv4", "IPv6",
                                                  "SHORT_IPv4"]) and
                                 (l4_type != "none") ) :
                                # Note: Skip any L4 testing if not IP
                                continue
                            # Skip if behavior is identical to other cases
                            elif ( (l3_type in ["IPv4"]) and
                                   (frag_opt == "NF")    and
                                   (l4_type not in ["TCP", "UDP",
                                                    "GRE-ETH"]) ) :
                                # Note: No need to repeat protocols for
                                #         both non-fragment case and first-
                                #         fragment case
                                continue
                            elif ( (l3_type in ["IPv4"]) and
                                   (frag_opt == "FF")    and
                                   (l4_type not in ["UDP-VXLAN-ETH",
                                                    "ICMP"]) ) :
                                # Note: No need to repeat protocols for
                                #         both non-fragment case and first-
                                #         fragment case
                                continue
                            elif ( (l3_type in ["IPv4"]) and
                                   (frag_opt == "MF")    and
                                   (l4_type not in ["TCP"]) ) :
                                # Note: Use TCP as an example of an L4
                                #         packet
                                continue
                            elif ( (l3_type in ["IPv4"]) and
                                   (frag_opt == "LF")    and
                                   (l4_type not in ["UDP", "ICMP"]) ) :
                                # Note: Use UDP as an example of an L4
                                #         packet
                                #       Use ICMP as an example of
                                #         ETH-IP-Other packet
                                continue
                            elif ( (l3_type in ["IPv6"]) and
                                   (l4_type not in ["ICMP"]) ) :
                                # Note: Test only ICMP with IPv6 for now
                                continue
                            elif ( (l3_type in ["SHORT_IPv4"]) and
                                   (l4_type not in ["UDP"]) ) :
                                # Note: Test only UDP with bad IPv4 for now
                                continue


                            # ***  Create the packets for each iteration  ***

                            if (self.test_config["abort_on_error"] and
                                (err_cnt > 0)):
                                continue

                            print("\nIter " + str(iter + 1) +
                                  ": Injecting packet of type " + l2_type +
                                  "-" +
                                  ("PPPoE-PPP-ETH"           if
                                   (l3_type == "PPP_Bridge") else
                                   l3_type) +
                                  (""                if
                                   l4_type == "none" else
                                   ("-" + l4_type)) +
                                  " (" +
                                  ("1 VLAN"     if
                                   vlan_present else
                                   "untagged") +
                                  (""                              if
                                   l3_type not in ["IPv4", "IPv6"] else
                                   (", " + frag_opt)) +
                                  (""                                  if
                                   l3_type not in ["IPv4", "IPv6",
                                                   "MPLS_M", "MPLS_U"] else
                                   (", HL/TTL=" + str(hl_ttl))) +
                                  ")...\n")

                            # Load per-iteration settings
                            hl_ttl   = self.test_config["hl_ttl"][iter %
                                                                  num_hl_ttl]
                            pkt_size = (60 if (l3_type in ["SHORT_IPv4"]) else
                                        self.test_config["pkt_sizes"][iter %
                                                                      nsizes])

                            # Add L2 header
                            if ( l2_type == "LONG_ETH" ) :
                                # Create long Ethernet header
                                hdr = sc.Ether(dst=mac_da, src=mac_sa)/ \
                                      sc.Dot1Q(vlan=vlan)/              \
                                      sc.Dot1Q(vlan=vlan)/              \
                                      sc.Dot1Q(vlan=vlan)
                            elif ( l3_type in ["FCoE", "MPLS_M", "MPLS_U"] ) :
                                # Determine the Ethertype
                                ether_type = 0x0

                                if ( l3_type == "FCoE" ) :
                                    ether_type = 0x8906
                                elif ( l3_type == "MPLS_M" ) :
                                    ether_type = 0x8848
                                elif ( l3_type == "MPLS_U" ) :
                                    ether_type = 0x8847

                                # Create ETH header w/ specified Ethertype
                                if ( vlan_present ) :
                                    hdr = sc.Ether(dst=mac_da, src=mac_sa)/ \
                                          sc.Dot1Q(vlan=vlan,
                                                   type=ether_type)
                                else :
                                    hdr = sc.Ether(dst =mac_da,
                                                   src =mac_sa,
                                                   type=ether_type)
                            else :
                                # Create standard Ethernet header
                                hdr = sc.Ether(dst=mac_da, src=mac_sa)

                                if ( vlan_present ) :
                                    hdr = hdr/sc.Dot1Q(vlan=vlan)


                            # Add L3 header
                            if ( l3_type == "ARP" ) :
                                hdr = hdr/sc.ARP()
                            elif ( l3_type == "FCoE" ) :
                                # Form the raw FCoE header
                                hdr_byte = [0x00, 0x00,
                                            0x00, 0x00, 0x00, 0x00,
                                            0x00, 0x00, 0x00, 0x00,
                                            0x00, 0x00, 0x00, 0x28]

                                raw_hdr = ""

                                for data in hdr_byte :
                                    raw_hdr += "%c" % (data)

                                hdr = hdr/raw_hdr
                            elif ( l3_type == "IPv4" ) :
                                if ( frag_opt == "NF" ) :
                                    hdr = hdr/sc.IP(src=ipv4_sa,
                                                    dst=ipv4_da,
                                                    ttl=hl_ttl)
                                elif ( frag_opt == "FF" ) :
                                    hdr = hdr/sc.IP(src  =ipv4_sa,
                                                    dst  =ipv4_da,
                                                    ttl  =hl_ttl,
                                                    flags="MF")
                                elif ( frag_opt == "MF" ) :
                                    hdr = hdr/sc.IP(src  =ipv4_sa,
                                                    dst  =ipv4_da,
                                                    ttl  =hl_ttl,
                                                    flags="MF",
                                                    frag =64)
                                elif ( frag_opt == "LF" ) :
                                    hdr = hdr/sc.IP(src  =ipv4_sa,
                                                    dst  =ipv4_da,
                                                    ttl  =hl_ttl,
                                                    frag=128)
                            elif ( l3_type == "SHORT_IPv4" ) :
                                hdr = hdr/sc.IP(src=ipv4_sa,
                                                dst=ipv4_da,
                                                ihl=15,
                                                ttl=hl_ttl)
                            elif ( l3_type == "IPv6" ) :
                                hdr = hdr/sc.IPv6(src =ipv6_sa,
                                                  dst =ipv6_da,
                                                  hlim=hl_ttl)
                            elif ( l3_type in ["MPLS_M", "MPLS_U"] ) :
                                # Convert MPLS label format for raw header
                                mpls = [0, 0, 0]
                                oam  = [0, 0, 0]

                                if ( l3_type == "MPLS_M" ) :
                                    mpls = [(mpls_mid >> 12) & 0xff,
                                            (mpls_mid >>  4) & 0xff,
                                            (mpls_mid <<  4) & 0xf0]
                                    oam  = [ (oam_mid >> 12) & 0xff,
                                             (oam_mid >>  4) & 0xff,
                                            ((oam_mid <<  4) & 0xf0) | 0x1]
                                elif ( l3_type == "MPLS_U" ) :
                                    mpls = [(mpls_uid >> 12) & 0xff,
                                            (mpls_uid >>  4) & 0xff,
                                            (mpls_uid <<  4) & 0xf0]
                                    oam  = [ (oam_uid >> 12) & 0xff,
                                             (oam_uid >>  4) & 0xff,
                                            ((oam_uid <<  4) & 0xf0) | 0x1]

                                # Form the raw MPLS header
                                hdr_byte = [mpls[0], mpls[1], mpls[2], hl_ttl,
                                            oam[0],  oam[1],  oam[2],  hl_ttl]

                                raw_hdr = ""

                                for data in hdr_byte :
                                    raw_hdr += "%c" % (data)

                                hdr = hdr/raw_hdr
                            elif ( l3_type == "PPPoED" ) :
                                hdr = hdr/sc.PPPoED()
                            elif ( l3_type == "PPP_Bridge" ) :
                                # Form the raw bridging header
                                hdr_byte = [0x00, 0x01] # Ethernet, no pad

                                raw_hdr = ""

                                for data in hdr_byte :
                                    raw_hdr += "%c" % (data)

                                # Append bridging PDU
                                hdr = hdr/sc.PPPoE()/             \
                                          sc.PPP(proto=0x31)/     \
                                          raw_hdr/                \
                                          sc.Ether(src=t1_mac_sa,
                                                   dst=t1_mac_da)


                            # Add L4 header
                            if ( l4_type == "TCP" ) :
                                hdr = hdr/sc.TCP(sport=tcp_sp, dport=tcp_dp)
                            elif ( l4_type == "UDP" ) :
                                hdr = hdr/sc.UDP(sport=udp_sp, dport=udp_dp)
                            elif ( l4_type == "GRE-ETH" ) :
                                hdr = hdr/sc.GRE(key_present=1,
                                                 proto=0x6558)/ \
                                          sc.Ether(src=t1_mac_sa,
                                                   dst=t1_mac_da)
                            elif ( l4_type == "UDP-VXLAN-ETH" ) :
                                # Form the raw VXLAN header
                                vni = [(vxlan_vni >> 16) & 0xff,
                                       (vxlan_vni >> 8)  & 0xff,
                                        vxlan_vni        & 0xff]

                                hdr_byte = [  0x08,   0x00,   0x00, 0x00,
                                            vni[0], vni[1], vni[2], 0x00]

                                raw_hdr = ""

                                for data in hdr_byte :
                                    raw_hdr += "%c" % (data)

                                hdr = hdr/sc.UDP(sport=4789, dport=4789,)/  \
                                          raw_hdr/                          \
                                          sc.Ether(src=t1_mac_sa,
                                                   dst=t1_mac_da)
                            elif ( l4_type == "ICMP" ) :
                                if ( l3_type == "IPv4" ) :
                                    hdr = hdr/sc.ICMP()
                                elif ( l3_type == "IPv6" ) :
                                    hdr = hdr/sc.ICMPv6EchoRequest()

                            # Add payload
                            pay      = ""
                            pay_size = pkt_size - len(hdr)

                            if ( (l3_type != "FCoE") and (pay_size <= 0) ) :
                                pay_size = 1 # Guarantee a min size payload
                            elif ( (l3_type == "FCoE") and (pay_size <= 4) ) :
                                pay_size = 5 # Make room for FCoE trailer

                            for i in range(pay_size) :
                                # Account for FCoE trailer
                                if ( (l3_type == "FCoE") and
                                     ((i + 4) == pay_size) ) :
                                    pay += "%c" % (0x41) # Append EOF
                                elif ( (l3_type == "FCoE") and
                                       ((i + 4) > pay_size) ) :
                                    pay += "%c" % (0x0)  # Append reserved
                                else :
                                    pay += "%c" % (~(i % 256) & 0xFF)

                            pkt = hdr/pay

                            if self.test_config["show_scapy_pkts"]:
                                pkt.show()

                            # Configure the test to use the new-formed packets
                            if not self.test_config["show_pds"]:
                                stpush("OUTPUT", "/dev/null")

                            pktss_cfgtx_dict["packet_config"] = sc.PacketList(
                                pkt * npkts)
                            pktss.config(pktss_cfg_dict, pktss_cfgtx_dict)

                            if not self.test_config["show_pds"]:
                                stpop("OUTPUT")


                            # ***  Set the expected metadata values  ***

                            # Set the expected early termination flag value
                            if ((l2_type == "LONG_ETH") or
                                (l3_type == "SHORT_IPv4")):
                                exp_metadata_val["early_term"] = 1
                            else:
                                exp_metadata_val["early_term"] = 0

                            # Set the expected fragment flag value
                            if frag_opt in ["FF", "MF", "LF"]:
                                exp_metadata_val["frag"] = 1
                            else:
                                exp_metadata_val["frag"] = 0

                            # Set the expected LB and packet error flag values
                            if ((l2_type in ["BAD_GFP", "LONG_ETH"]) or
                                (l3_type == "SHORT_IPv4")):
                                exp_metadata_val["lb_err"]  = 1
                                exp_metadata_val["pkt_err"] = 1
                            else:
                                exp_metadata_val["lb_err"]  = 0
                                exp_metadata_val["pkt_err"] = 0

                            # Set the expected multicast flag value
                            if l3_type == "MPLS_M":
                                exp_metadata_val["mc_flag"] = 1
                            else:
                                exp_metadata_val["mc_flag"] = 0

                            # Set the expected outer L1 type
                            if l2_type == "BAD_GFP":
                                exp_metadata_val["o_l1"] = l1_enc["GFP"]
                            else:
                                exp_metadata_val["o_l1"] = l1_enc["Other"]

                            # Set the expected outer L2 type
                            if l3_type == "PPP_Bridge":
                                exp_metadata_val["o_l2"] = l2_enc["ETH-PPPoE"]
                            elif l2_type == "ETH":
                                exp_metadata_val["o_l2"] = l2_enc["ETH"]
                            else:
                                exp_metadata_val["o_l2"] = l2_enc["Other"]

                            # Set the expected outer L3 type
                            if l3_type in ["ARP", "FCoE", "IPv4", "IPv6"]:
                                exp_metadata_val["o_l3"] = l3_enc[l3_type]
                            elif l3_type in ["MPLS_M", "MPLS_U"]:
                                exp_metadata_val["o_l3"] = l3_enc["MPLS-Other"]
                            else:
                                exp_metadata_val["o_l3"] = l3_enc["Other"]

                            # Set the expected outer L4 type
                            if l3_type == "SHORT_IPv4":
                                exp_metadata_val["o_l4"] = l4_enc["Other"]
                            elif frag_opt in ["MF", "LF"]:
                                exp_metadata_val["o_l4"] = l4_enc["NFF"]
                            elif l4_type in ["TCP", "UDP"]:
                                exp_metadata_val["o_l4"] = l4_enc[l4_type]
                            elif l4_type == "GRE-ETH":
                                exp_metadata_val["o_l4"] = l4_enc["GRE"]
                            elif l4_type == "UDP-VXLAN-ETH":
                                exp_metadata_val["o_l4"] = l4_enc["UDP-VXLAN"]
                            else:
                                exp_metadata_val["o_l4"] = l4_enc["Other"]

                            # Set the expected packet warning flag value
                            if (l3_type in ["IPv4", "IPv6"]) and (hl_ttl <= 1):
                                # Note: Triggered on low HL/TTL
                                exp_metadata_val["pkt_warn"] = 1
                            else:
                                exp_metadata_val["pkt_warn"] = 0

                            # Set the expected special flag value
                            if l3_type in ["MPLS_M", "MPLS_U"]:
                                exp_metadata_val["spec_flag"] = 1
                            else:
                                exp_metadata_val["spec_flag"] = 0

                            # Set the expected TCP SYN/RST flag value
                            if ((l4_type == "TCP") and
                                (frag_opt in ["NF", "FF"])):
                                exp_metadata_val["tcp_syn_rst"] = 1
                            else:
                                exp_metadata_val["tcp_syn_rst"] = 0

                            # Set the expected tunnel-layer depth
                            if l4_type in ["GRE-ETH", "UDP-VXLAN-ETH"]:
                                exp_metadata_val["tld"] = 1
                            else:
                                exp_metadata_val["tld"] = 0

                            # Set the expected VLAN tag count
                            if l2_type == "LONG_ETH":
                                exp_metadata_val["vlan_cnt"] = 3
                            elif vlan_present:
                                exp_metadata_val["vlan_cnt"] = 1
                            else:
                                exp_metadata_val["vlan_cnt"] = 0

                            # Calculate expected offsets
                            curr_off = self.test_config["prepend_len"] << 1

                            if l3_type in ["IPv4", "IPv6", "MPLS_M", "MPLS_U"]:
                                curr_off = (curr_off + 14 +
                                            (exp_metadata_val["vlan_cnt"] * 4))

                            exp_metadata_val["o_off"] = curr_off

                            if l2_type == "BAD_GFP":
                                curr_off = curr_off + 8
                            elif l2_type == "LONG_ETH":
                                pass
                            elif l3_type in ["ARP", "FCoE", "PPPoED",
                                             "SHORT_IPv4"]:
                                curr_off = (curr_off + 14 +
                                            (exp_metadata_val["vlan_cnt"] * 4))
                            elif l3_type == "IPv4":
                                curr_off = curr_off + 20
                                if l4_type == "GRE-ETH":
                                    curr_off = curr_off + 8
                            elif l3_type == "IPv6":
                                curr_off = curr_off + 40
                            elif l3_type in ["MPLS_M", "MPLS_U"]:
                                curr_off = curr_off + 8
                            elif l3_type == "PPP_Bridge":
                                curr_off = (curr_off + 20 +
                                            (exp_metadata_val["vlan_cnt"] * 4))

                            exp_metadata_val["hp_off0"] = curr_off

                            if ((l3_type == "IPv4") and (l4_type == "TCP") and
                                (frag_opt in ["NF", "FF"])):
                                curr_off = curr_off + 20
                                exp_metadata_val["hp_off1"] = curr_off
                            elif ((l3_type == "IPv4") and
                                  (l4_type == "UDP") and
                                (frag_opt in ["NF", "FF"])):
                                curr_off = curr_off + 8
                                exp_metadata_val["hp_off1"] = curr_off
                            elif ((l3_type == "IPv4") and
                                  (l4_type == "UDP-VXLAN-ETH") and
                                (frag_opt in ["NF", "FF"])):
                                curr_off = curr_off + 16
                                exp_metadata_val["hp_off1"] = curr_off
                            else:
                                exp_metadata_val["hp_off1"] = 0


                            # ***  Run the test iteration  ***

                            # Clear Stats
                            portstatclr(PKTSS)

                            # Start Traffic
                            portstartrx(PKTSS)
                            portstarttx(PKTSS)

                            # Make sure that packets were received
                            self.check_packets_received(
                                pktss, self.test_config["pkts_per_iter"],
                                last_recv_cnt)

                            # Stop Traffic
                            portstoprx(PKTSS)

                            # Dump Counters
                            if not self.test_config["show_counters"]:
                                stpush("OUTPUT", "/dev/null")

                            stat = portstatd(PKTSS)

                            if not self.test_config["show_counters"]:
                                stpop("OUTPUT")

                            # Check counters for errors
                            if not pktss.continuous_mode :
                                pktss.check(stat, last_stat)

                            # Validate the metadata
                            err_cnt = err_cnt + self.check_npfw_metadata(
                                NFP, cfg["mei_snk_num"], exp_metadata_val)

                            last_recv_cnt = stat[pktss.NBII[0] +
                                                 "nbi_pc_char_seq"]
                            last_stat     = stat
                            iter += 1
                        # end for l4_type loop
                    # end for frag_opt loop
                # end for l3_type loop
            # end for vlan_present loop
        # end for l2_type loop

        # Stop MEs
        if pktss.stopme == 1 and not pktss.continuous_mode :
            pktss.stop()

        # Turn off waves (optional)
        if self.test_config["capture_waves"]:
            pktss.waves(0)

        # Unsuppress excess output, if suppressed
        if not self.test_config["verbose"]:
            stpop("DEBUG")


    # Keep track of what metadata has already been seen
    metadata_history = {}

    def check_npfw_metadata(self, NFP, mei_snk_arr, exp_metadata_val):
        """Function to check the Catamaran NPFW metadata"""

        err_cnt = 0

        # Retrieve all the packet metadata left in CTM
        metadata_list = []
        packet_list   = []

        for mei_snk_num in mei_snk_arr:
            MEI_SNK = NFP + ("mei%d_" % (mei_snk_num))
            metadata = []
            packet   = []

            for i in range(2):
                metadata.append(memr(MEI_SNK + "ctm_mem", 0x8 + (i << 3)))

            for i in range(28):
                packet.append(memr(MEI_SNK + "ctm_mem", 0x20 + (i << 3)))

            # Check if this packet has already been seen
            if metadata[0] not in self.metadata_history:
                # Add this packet to the list
                metadata_list.append(metadata)
                packet_list.append(packet)

                # Update the packet history
                self.metadata_history[metadata[0]] = 1

        # Check for the latest sequence number
        latest_metadata = {}
        latest_packet   = {}
        latest_seq_num  = {}

        for i in range(len(metadata_list)):
            seq_num = (metadata_list[i][0] >> 16) & 0xffff
            seqr    = (metadata_list[i][0] >>  8) & 0x7
            valid   = (metadata_list[i][0] >>  3) & 0x1

            if (valid == 1) and ((seqr not in latest_seq_num) or
                                 (seq_num >= latest_seq_num[seqr])):
                latest_metadata[seqr] = metadata_list[i]
                latest_seq_num[seqr]  = seq_num
                latest_packet[seqr]   = packet_list[i]

        # Parse the metadata information
        metadata_info = {}

        for seqr,metadata in latest_metadata.iteritems():
            prev_err_cnt = err_cnt

            # Record the already parsed info
            metadata_info["seqr"]  = seqr
            metadata_info["seq"]   = latest_seq_num[seqr]
            metadata_info["valid"] = 1

            # Parse the metadata
            metadata_info["t_l4"]         = (metadata[0] >> 60) & 0xf
            metadata_info["t_l3"]         = (metadata[0] >> 56) & 0xf
            metadata_info["t_l2"]         = (metadata[0] >> 51) & 0x1f
            metadata_info["early_term"]   = (metadata[0] >> 50) & 0x1
            metadata_info["tld"]          = (metadata[0] >> 48) & 0x3
            metadata_info["o_l4"]         = (metadata[0] >> 44) & 0xf
            metadata_info["o_l3"]         = (metadata[0] >> 40) & 0xf
            metadata_info["o_l2"]         = (metadata[0] >> 35) & 0x1f
            metadata_info["o_l1"]         = (metadata[0] >> 33) & 0x3
            metadata_info["intrasystem"]  = (metadata[0] >> 32) & 0x1
            metadata_info["tnfp"]         = (metadata[0] >> 14) & 0x3
            metadata_info["seq_override"] = (metadata[0] >> 11) & 0x7
            metadata_info["lb_err"]       = (metadata[0] >>  7) & 0x1
            metadata_info["mtype"]        = (metadata[0] >>  4) & 0x7
            metadata_info["bp"]           =  metadata[0]        & 0x7

            metadata_info["pkt_err"]      = (metadata[1] >> 63) & 0x1
            metadata_info["if_err"]       = (metadata[1] >> 62) & 0x1
            metadata_info["pkt_warn"]     = (metadata[1] >> 61) & 0x1
            metadata_info["spec_flag"]    = (metadata[1] >> 60) & 0x1
            metadata_info["vlan_cnt"]     = (metadata[1] >> 58) & 0x3
            metadata_info["tcam_tag"]     = (metadata[1] >> 57) & 0x1
            metadata_info["table_set"]    = (metadata[1] >> 56) & 0x1
            metadata_info["o_off"]        = (metadata[1] >> 48) & 0xff
            metadata_info["mode"]         = (metadata[1] >> 44) & 0xf
            metadata_info["match"]        = (metadata[1] >> 42) & 0x3
            metadata_info["lif"]          = (metadata[1] >> 32) & 0x3ff
            metadata_info["port"]         = (metadata[1] >> 24) & 0xff
            metadata_info["hp_off1"]      = (metadata[1] >> 16) & 0xff
            metadata_info["hp_off0"]      = (metadata[1] >>  8) & 0xff
            metadata_info["frag"]         = (metadata[1] >>  7) & 0x1
            metadata_info["mc_flag"]      = (metadata[1] >>  6) & 0x1
            metadata_info["tcp_syn_rst"]  = (metadata[1] >>  5) & 0x1
            metadata_info["syn_pol_flag"] = (metadata[1] >>  4) & 0x1
            metadata_info["pol_flag"]     = (metadata[1] >>  3) & 0x1
            metadata_info["pol_op_id"]    = (metadata[1] >>  0) & 0x7

            # Make sure the MType value is valid
            if metadata_info["mtype"] not in [0, 1]:
                error(NFP, "Metadata field 'mtype' has invalid value "
                      "(got=%d, exp=0 or 1)" % (metadata_info["mtype"]))
                err_cnt += 1

            # Validate that all default expected settings are correct
            for k,v in self.def_exp_metadata_val.iteritems():
                if metadata_info[k] != v:
                    error(NFP, "Metadata field '{0:s}' has unexpected value "
                          "(got=0x{1:x}({1:d}), exp=0x{2:x}({2:d}))".format(
                              k, metadata_info[k], v))
                    err_cnt += 1

            # Validate that all specified expected settings are correct
            for k,v in exp_metadata_val.iteritems():
                if metadata_info[k] != v:
                    error(NFP, "Metadata field '{0:s}' has incorrect value "
                          "(got=0x{1:x}({1:d}), exp=0x{2:x}({2:d}))".format(
                              k, metadata_info[k], v))
                    err_cnt += 1

            # Dump the packet content if there was an error
            if prev_err_cnt != err_cnt:
                print("0x----------------")

                for metadata in latest_metadata[seqr]:
                    print("0x%08x %08x" % ( metadata        & 0xffffffff,
                                           (metadata >> 32) & 0xffffffff))

                print("0x----------------")

                for data in latest_packet[seqr]:
                    print("0x%08x %08x" % ( data        & 0xffffffff,
                                           (data >> 32) & 0xffffffff))

        return err_cnt

    def check_packets_received(self, pktss, exp_num, last_cnt):
        """Function to check that packets arrived at the sink MEs"""

        # Poll the sink ME counters
        timeout_cnt = 1000

        for iter in range(timeout_cnt):
            cnt = 0

            for i in range(pktss.num_of_me_snk):
                cnt += regr(pktss.ME_SNK[i] + "mailbox3")

            if (cnt - last_cnt) >= exp_num:
                break

        if (cnt - last_cnt) < exp_num:
            print("WARNING: %d/%d packets received at NBI Preclassifier" %
                  (cnt - last_cnt, exp_num))

    def nbi_pc_catamaran_init(self, pktss):
        """Function to load the Catamaran NPFW"""

        for NBII in pktss.NBII:
            pc = tfentity(NBII + "nbi_pc_")
            pc.init("catamaran")

    def set_port_cfg(self, pktss, prepend_len, oper, channels=range(128)):
        """Function to set NBI Preclassifier Characterizer Port Config"""

        for NBII in pktss.NBII:
            PC = NBII + "nbi_pc_"

            for chan in channels:
                regw(PC + "char_portcfg%d" % chan,
                     (0 << 5) | (prepend_len << 2) | oper)

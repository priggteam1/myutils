from datetime import datetime
from icalendar import Calendar, Event
import csv

with open("daily_activities.csv") as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    line_count = 0
    for row in csv_reader:
        if line_count == 0:
            print('Column names are %s' % row)
        else:
            time_start,time_end,room,activity,description= row
            print("Entry %s %s %s %s " % (time_start, time_end, room, description))
            datetime_start_object = datetime.strptime(time_start, '%b %d %Y %I:%M%p')
            datetime_end_object = datetime.strptime(time_end, '%b %d %Y %I:%M%p')

            cal = Calendar()
            event = Event()
      
            event.add('summary', activity)
            event.add('dtstart', datetime_start_object)
            event.add('dtend', datetime_end_object)
            event.add('description', description)
            event.add('location', room)
            cal.add_component(event)
 
            f = open('prigg_schedule.ics', 'wb')
            f.write(cal.to_ical())
            f.close()
        line_count += 1
    print('Processed %d lines.' % line_count)


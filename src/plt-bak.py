#!/usr/bin/env python

import os
import sys
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab
import matplotlib.figure as mfig

cmd = os.path.abspath(sys.argv[0])
cmddir = os.path.dirname(cmd)

if len(sys.argv) < 2:
    print >> sys.stderr, "Usage:  <files...> ending in .csv"
    sys.exit(1)

files = sys.argv[1:]

color_dict = { 64 : 'y', 
              128 : 'g', 
              256 : "c", 
              512 : 'm',
              1024 : 'k',
              1500 : 'r'
              }

for f in files:

    fig = plt.figure()

    # control pixel/dpi for image
    defsize = fig.get_size_inches()
    fig.set_size_inches((defsize[0]*3, defsize[1]*3))

    plt.xlabel("CLK Frequency")
    plt.ylabel("CTM Peak")
    plt.title("CTM Buffer Utilization")

    csvfile = os.path.join(cmddir, f)

    # find our case line brute force from file
    case = f
    for line in open(csvfile, "r"):
        if line.find('MAC') >= 0:
            tmp = line.split(',')
            for s in tmp:
                if s.find('Split') >= 0:
                    case = s
                    break

    print 'Processing case=%s' % case

    r = mlab.csv2rec(csvfile)
    # r.sort()

    # temprature, ignore lines that look invalid temperature
    # if ((x > 0) and (x < 110))]
    #
    # xdata=CLK_frequency ydata=ctm_peak_utilization
    #
    xdata=[]
    ydata=[]
    #
    # x_sdata=CLK_frequency y_sdata=ctm_peak <--- Array: Appended for each setpoint
    #
    x_sdata=[]
    y_sdata=[]
    #
    # x_last=CLK_frequency y_last=ctm_peak   <--- Tracks just before setpoint
    #
    x_last=r[1][0]
    y_last=r[1][3]

    #
    # setpoint_info temperature  <--- Array item: [time, setpoint, fluke, thermal chamber]
    #
    setpoint_info=[]
    #
    # z  <--- Array of setpoint_info none!
    z=[]
    cur_size = 0
    prev_size = 0
    for x in r :
        print x
        if (prev_size != 0) and (prev_size != cur_size) :
           label = "%s"  % (prev_size)
           save_x=xdata[-1]
           save_y=ydata[-1]
           print save_x,save_y
           xdata=xdata[0:-1]
           ydata=ydata[0:-1]
           print xdata,ydata
           plt.plot(xdata, ydata, color=color_dict[prev_size], marker='o', label=label)
           xdata=[]
           ydata=[]
           xdata.append(save_x)
           ydata.append(save_y)

        if x[0] != 0 :
           prev_size = cur_size
           cur_size = x[2]
           # frequency
           xdata.append(x[0])
           # buffer utilization
           ydata.append(x[1])

    slabel=""
    #
    # Plot NFP Temperature to Power
    #
    #plt.plot(xdata, ydata, color='g', marker='o', label=label) 
    #
    # Plot NFP Temperature to Power at time setpoint is put in place
    # don't connect points with lines
    #
    plt.plot(x_sdata, y_sdata, color='r', marker='s', label=slabel, linestyle='None')

    plt.legend(loc='lower right')
    plt.axis([ 500,1200,  0,300 ])

    # png
    png = f.replace('.csv', '.png')
    fig.savefig(png, format='png')
    print 'png --> %s' % png

    # pdf
    pdf = f.replace('.csv', '.pdf')
    fig.savefig(pdf, format='pdf')
    print 'png --> %s' % pdf

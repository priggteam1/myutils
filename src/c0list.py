#!/usr/bin/env python

import os
import sys
from operator import itemgetter, attrgetter
import subprocess as sp
import time
from collections import OrderedDict
from optparse import OptionParser
import getpass
try :
    from jira.client import JIRA
    jira_module=1
except :
    jira_module=0

QUERY_JIRA = True


#
# JIRA data
#
jtix = [
    # (jissue, 'Category', 'Impact', 'Workaround Suggested, 'Priority', 'B0 Errata', 'Workaround Implemented')
    #     summary and related issues filled in by making JIRA queries
    ['THS-126', 'Showstopper',    '', '', '1', '', ''],
    ['THS-121', 'Performance',    '', 'Beagle repos', '1', '', ''],
    ['THS-120', 'SW usability',    '', '', '3', '', '<a href=https://hg.netronome.com/cgi-bin/hgweb.cgi/data/hg/repos/nfp-bsp-boardconfig.hg/rev/028a798632a6>boardconfig</a>'],
    ['THS-119', 'Performance',    '', 'WIP', '1', '', 'Beagle repos'],
    ['THS-118', 'Compliance',    'Issue moved to THC-1557', 'WIP', '1', '', ''],
    ['THS-117', 'Performance',    '', 'WIP', '1', '', 'Beagle repos'],
    ['THS-116', 'SW usability',    '', '', '1', '', '<a href=https://netronome.atlassian.net/browse/THS-116?focusedCommentId=186772&page=com.atlassian.jira.plugin.system.issuetabpanels:comment-tabpanel#comment-186772>In JIRA</a>'],
    ['THS-112', 'SW usability, Compliance',    'Protocol Implemented in software (difficult)', 'WIP', '1', '', ''],
    ['THS-110', 'Performance',    '', '', '1', '', '<a href=https://hg.netronome.com/cgi-bin/hgweb.cgi/data/hg/repos/nfp-bsp-boardconfig.hg/rev/1642>boardconfig</a>'],
    ['THS-90', 'Compliance',     '', 'WIP', '2', '', ''],
    ['THS-76', 'Compliance',     '', 'WIP', '2', '', ''],
    ['THS-87', 'SW usability',     'Complex, high touch reset procedures with lots of software assist', '', '1', '', '<a href=https://hg.netronome.com/cgi-bin/hgweb.cgi/data/hg/repos/nfp-bsp-tools.hg/file/tip/nfp-shutils.in>nfp-shutils.in</a>'],
    ['THS-109', 'SW usability',    'Requires ME to proxy for loader services', '', '1', 'ERR54', '<a href=http://hg.netronome.com/gitweb/?p=nfp-drv-user.git;a=commit;h=7bb74426fa8652b4e99fd87a5f7974a917c64874>Use ME in ARM island as proxy</a>'],
    ['THS-69', 'Performance',    '', 'TODO', '1', '', ''],
    ['THS-62', 'Performance',    '', 'TODO', '1', '', ''],
    ['THS-61', 'Performance',    '', 'TODO', '1', '', ''],
    ['THC-1726', 'Performance',    '', 'TODO', '1', '', ''],
    ['TH-12491', 'SW usability',   '', '', '1', 'ERR48', ''],
    ['TH-12686', 'Performance',   '', '', '1', 'ERR49', ''],
    ['TH-13108', 'SW usability',   '', '', '1', 'ERR50', ''],
    ['TH-12937', 'Approved',   '', '', '1', 'ERR51', ''],
    ['TH-13150', 'SW usability',   '', '', '2', 'ERR52', ''],
    ['TH-11940', 'SW usability',   '', '', '1', 'ERR53', ''],
    ['TH-11602', 'Performance',   '', '', '1', '', ''],
    ['TH-13111', 'Approved',   'Need to disable pause frames with enough scheduler support', '', '1', 'ERRPENDING', 'setting 0 pause quanta solves SDN-1253'],
    ['TH-13147', 'SW usability',   '', '', '3', '', ''],
    ['THDOC-341', 'SW usability',  "In the event an external access command becomes corrupted by a direct access command, the external operation will not properly receive TCache tag information, but rather will instead absorb the local command tag and address information and execute to the local DCache location(s) specified for that command. This issue exists only for the External Memory Unit, as it is the only one that supports accesses to external memory.", "Reserve the low 4MB of the addressable memory address range for direct access operations only. The supported address range for external memory would then be 4MB to 8GB.", '2', 'ERR01', ''],
    ['TH-12184', '',   '', '', 'ILA', 'ERR02', ''],
    ['TH-11509', 'Approved',   'Small packets received back-to-back may contain the same timestamp', 'If an application requires unique timestamps per packet, then it could compare the current timestamp to the previous timestamp and add one if they are equal.', '2', 'ERR03', ''],
    ['TH-11743', 'Approved',   'User will not be able to inject one-shot ECC or data errors in the CTM, Internal Memory Unit or External Memory Unit Data Caches.', "User will need to use the 'inject permanent ECC or data errors' capability, rather than the 'inject one-shot ECC or data error' capability of the ECC Monitor", '3', 'ERR04', ''],
    ['TH-11949', 'Performance',   'PushStatClear and LBLookup to the same address cannot be done reliably in close proximity.', '<a href="http://mahome.netronome.com/~ydcad/thornham_docbook_b0/DBERR_nfp-6xxx-xc.pdf">DBERR_nfp-6xxx-xc.pdf</a>', '3', 'ERR05', ''],
    ['TH-11987', 'Approved',   'If you insert timestamp for Interlaken, the wrong timestamp value will result', 'Do not use timestamp insertion for Interlaken egress packets', '1', 'ERR06', ''],
    ['TH-12098', '',   '', '', 'ILA', 'ERR07', ''],
    ['TH-12103', 'SW usability',   'MDIO using virtual mode forces some peculiar (and incorrect) timing on clock versus write data and read data.', "For MDIO operation, use only manual mode. Do not use virtual mode MDIO. Virtual mode is only currently used out of NFP reset once the ARM has been also brought out of reset and is configured to fetch instructions from EEPROM, if this was a requirement it would likely be the only case where manual mode wasn't the preferred method/workaround for using the SPI/MDIO hardware.", '3', 'ERR08', ''],
    ['TH-12387', 'SW usability',   'When ME receives the signal on the pull, it will interpret the command as complete with match found for compare_and_write_or_incr command. And Signal(s) sent with push from MU may end up interfering with unrelated commands that reuses those signal(s) in the ME.', '<a href="http://mahome.netronome.com/~ydcad/thornham_docbook_b0/DBERR_nfp-6xxx-xc.pdf">DBERR_nfp-6xxx-xc.pdf</a>', '1', 'ERR09', ''],
    ['TH-11305', 'SW usability',   'A modscr error is detected and the packet modifications are not performed. The packet is passed as it is onto the minipkt bus to the MAC.', 'By setting the CSR NbiPmDisableErrors[DisableIncrDecrOffsetError] = 1], we can disable the contribution of this error. Since, NbiPmDisableErrors[DisableIncrDecrOffsetError] resets to 1b0, it needs to be set by the software to 1b1 to avoid this incorrectly implemented error detection', '2', 'ERR10', ''],
    ['TH-12471', 'Fixed',   'If an odd DataRef (i.e., DataRef[2] = 1) is used when writing to CPP addressable locations within the NBI, the implication is that the last 32-bits of the data write operation will not get written to the NBI memory.', 'Software must write the CPP addressable memory locations within NBI using an even DataRef (DataRef[2] = 0).', '1', 'ERR11', ''],
    ['TH-12429', 'SW usability',   '1) The queue controller will NOT perform atomic read/modify/write operations when address bit 10 is set. 2) The event manager EventConfig CSR ClockDivide bits will be cleared by a read. Writes work as expected.', 'Implication 1: There is no workaround for Implication 1. Implication 2: There is no need to read the event manager EventConfig CSR. Software may choose to keep a shadow copy in memory.', '1', 'ERR13', ''],
    ['TH-12835', 'SW usability',   'Parity Error/Corrupted data in transfer registers of the ME would not translate into poisoned data to Dcache', '', '1', 'ERR18', ''],
    ['TH-12018', '',   '', '', 'ILA', 'ERR24', ''],
    ['TH-12597', '',   '', '', 'ILA', 'ERR29', ''],
    ['TH-12420', '',   '', '', 'ILA', 'ERR30', ''],
    ['TH-12071', '',   '', '', 'ILA', 'ERR31', ''],
    ['TH-12635', 'SW usability',   'MSI-X interrupts that would have been expected to be recorded in the Pending Bit Array (PBA) functionality are dropped and not delivered.', 'For MSI-X requests from ME code, it is therefore the responsibility of the ME code to re-issue the MSI-X when it is being unmasked (and the interrupt condition still applies). Thus hardware support of PBA is effectively defeatured.', '1', 'ERR32', ''],
    ['TH-12841', 'Fixed',   'You get an incorrect indication that an ECC error was not corrected. However, the detection of the error is still valid and the ECC monitor will still send a single bit ECC error event.', '<a href="http://mahome.netronome.com/~ydcad/thornham_docbook_b0/DBERR_nfp-6xxx-xc.pdf">DBERR_nfp-6xxx-xc.pdf</a>', '2', 'ERR34', ''],
    ['THS-5', 'Fixed',      'Manual mode does not consistently work unless the software driver polls the SPI port busy bit.', 'For a large set of SPI transaction types and use cases the polling workaround is sufficient and even with spin locks around transactions to ensure any hard timing requirements isn\'t likely to have any impact on system scheduling or performance. Performing long reads, kilobytes or greater, of EEPROM devices is one common scenario where problems may arise if used in conjuction with locks, though this issue would still exist if polling was not required.', '3', 'ERR36', ''],
    ['THS-71', 'Performance',     'At high data rates, in particular 200Gb traffic as a result of using both NBI interfaces on the NFP-6xxx at 100Gb each, may result in a deadlock condition that hangs traffic flow through the NFP-6xxx', "Target push data from the CTM must be load balanced such that data leaving the island use 2 of the 4 push data buses, while data staying on the island use the other 2 buses. For CTM's, separate target push data leaving the island versus target push data staying within the island (see patch).", '1', 'ERR42', '<a href=https://hg.netronome.com/cgi-bin/hgweb.cgi/data/hg/repos/nfp-bsp-tools.hg/file/tip/nfp-shutils.in>CTMPushLoadBal</a>, PACKET_READY, many (see attachment in JIRA)'],
    ['TH-13090', 'SW usability',   'When packets are being dropped by Traffic Manager, one might see the following: Total Packets != (Packets sent out to Mac) + (Drop count read from Drop count memory)', 'There is no workaround to completely solve this problem. The impact can be reduced by reducing the frequency of XPB reads to the Drop count memory. The fewer the XPB reads, the less the chances of counts being off.', '1', 'ERR44', ''],
    ['TH-13057', 'SW usability',   'Traffic may stop or packets may unexpectedly get stuck in the traffic manager.', 'Do not use a queue size of 16,384 and only use a queue size of 8,192 when one queue is enabled. QueueConfig Register limitations: QueueSize setting of 0xD = 8,192 : Only allowed when only one queue is used. QueueSize setting of 0xE = 16,384 : Not allowed', '1', 'ERR45', ''],
    ['THS-10', 'Showstopper',     'A kernel error message "Dazed and Confused" may be seen on some linux systems when the PCIe bus is enumerated.', 'A patch to systemd is available so that only 64 bytes of config space is read instead of 4k.', '1', 'ERR47', '<a href=https://bugs.freedesktop.org/show_bug.cgi?id=90262>systemd patch</a>'],
    ['TF-314', 'Approved',     'Improved LED support', '', '1', '', ''],
    ['TF-794', 'SW usability',     'Peripheral IO, GPIO, SPI', '', '1', '', ''],
    ['TF-938', 'SW usability',     '', '', '1', '', ''],
    ['NFDH-137', 'Performance',     'PCIe Perfformance @ slower clocks', '', '1', '', ''],
    ['THDEVPLATFORMS-699', 'Performance',     'Latency measurements and review', '', '2', '', ''],
    ['THC-1590', 'Performance',      'CTM meta data change to include CTM pkt size. SDN-1229', 'Less CTM reads/writes on per packet basis', '1', '', ''],
    ['THC-25', 'SW usability',        'MU only packet ready', '', '2', '', ''],
    ['THS-122', 'Performance',       'Improve performance on per packet encapsulation', '', '1', '', ''],
    ['JIRACHECK', 'Performance',     'Ability to push more pkt header info into xfers', '', '1', '', ''],
    ['THC-54', 'Performance',        'Add packet feature in CTM', '', '1', '', ''],
    ['THS-123', 'Performance',       'NFP NBI Ingress and TM DMA Performance Visibility', '', '2', '', ''],
    ['THS-124', 'Performance',       'NFP PCIe DMA Performance Visibility', '', '2', '', ''],
    ['JIRACHECK', 'Performance',     'HASH Engine Usage and Performance', '', '1', '', ''],
    ['JIRACHECK', 'Performance',     'CTM address versus CTM packet number', '', '1', '', ''],
    ['THS-125', 'SW usability',       'Better visibility into RX Adapt results', '', '2', '', ''],
    ['THC-1749', 'HW usability',       'Layout', '', '1', '', ''],
    ['TH-9350', 'Approved',       'Layout', '', '1', '', ''],
    ['TF-943', 'Showstopper',       'Supporting 10 and 1G on the same macro', '', '1', '', ''],
    ['TF-942', 'SW usability, HW usability',       '', '', '1', '', ''],
    ['TF-937', 'SW usability',       'Automatic RX Adapt', '', '1', '', ''],
    ['THC-138', 'HW usability',       '', '', '1', '', ''],
    ['THC-1572', 'SW usability',     'Reduce ME work needed for MAC stats', '', '1', '', ''],
    ['', '',     '', '', '', '', ''],
]

#------------------------------------------------------------------------------
def genwiki(user='', passwd=''):
    if not jira_module :
        print "ERROR: You need the JIRA python package from pypi"
        print "Please consider these steps for Ubuntu (YMMV):"
        print "    # get easy_install"
        print "    sudo apt-get install python-setuptools"
        print "    sudo easy_install pip"
        print "    sudo pip install -U pip"
        print "    sudo pip install jira"
        sys.exit(1)
    jira = JIRA(server='http://netronome.atlassian.net', basic_auth=(user, passwd))

    out = sys.stdout
    out.write("""<meta http-equiv="content-type" content="text/html"; charset= UTF-8)">

<TITLE> Issues in B0 Harrier/Osprey for consideration for future revisions (e.g. C0/falcon etc.) </TITLE>
<!-- load jQuery and tablesorter scripts -->
<script type="text/javascript" src="./jquery-1.11.1.js"></script>
<script type="text/javascript" src="./jquery.tablesorter.mod.js"></script>

<!-- tablesorter widgets (optional) -->
<script type="text/javascript" src="./jquery.tablesorter.widgets.js"></script>
<STYLE TYPE="text/css">
<!--
H1 { font-size:24pt; font-weight:900; font-style:normal; }
H2 { font-size:18pt; font-weight:900; font-style:normal; }
H3 { font-size:12pt; font-weight:900; font-style:normal; }
-->
</STYLE>
<STYLE>
table, TH, TD {
    border: 1px solid black;
    border-collapse: collapse;
}
TH, TD {
    padding: 5px;
    text-align: left;
}
</STYLE>
<H1> Issues in B0 Harrier/Osprey for consideration for future revisions (e.g. C0) </H1>
<p>%s</p>
<p>B0 Errata Reference Document: <a href="http://mahome.netronome.com/~ydcad/thornham_docbook_b0/DBERR_nfp-6xxx-xc.pdf">DBERR_nfp-6xxx-xc.pdf</a></p>
<p>C0 JIRA dashboard: <a href=https://netronome.atlassian.net/secure/Dashboard.jspa?selectPageId=11824>Enhancements Considered for Harrier C0 </a>.  Is C0 based on the harrier_b0 or falcon code base?  Validation tasks might need to be scoped accordingly ... </p>
<p>Major Features not represented in the issues list below:</p>
<ul>
<li>25G MAC support</li>
<li>PCIe x16 </li>
<li>Trade Crypto islands for ME</li>
<li>Trade ILA islands for ME</li>
</ul>
    """ % time.ctime())

    # sorted in order of the tables look in HTML
    buckets = [
        'Showstopper',
        'HW usability',
        'Compliance',
        'Performance',
        'SW usability',
        'Approved',
        'Fixed',
        'Unknown',
        ]

    jtix_buckets = {}
    for b in buckets:
        jtix_buckets[b] = []
    for jt in jtix:
        if jt[0] == '':
            continue
        if jt[1] == '':
            jt[1] = 'Unknown'
        for b in jt[1].split(','):
            b = b.lstrip().rstrip()
            jtix_buckets[b].append(jt)

    lasttime = time.time()
    total = 0.0
    for b in buckets:
        out.write("""<TABLE ID='%s' CLASS='grid tablesorter' BORDER=0 CELLPADDING=0 CELLSPACING=1 STYLE='font-size:95%%' width='100%%'>""" % b)
        out.write("<THEAD>")
        out.write("<TR>")
        out.write(""" <TH bgcolor='#a0a0a0'> Issue </TH> """ +
                  """ <TH bgcolor='#a0a0a0'> Related Issue(s) </TH> """ +
                  """ <TH bgcolor='#a0a0a0'> Category  </TH> """ +
                  """ <TH bgcolor='#a0a0a0'> Summary  </TH> """ +
                  """ <TH bgcolor='#a0a0a0'> Impact  </TH> """ +
                  """ <TH bgcolor='#a0a0a0'> Workaround Suggested </TH> """ +
                  """ <TH bgcolor='#a0a0a0'> Workaround Implemented </TH> """ +
                  """ <TH bgcolor='#a0a0a0'> Priority  </TH> """ +
                  """ <TH bgcolor='#a0a0a0'> B0 Errata  </TH> """ +
                  """ <TH bgcolor='#a0a0a0'> Component </TH> """
              )
        out.write("</TR>")
        out.write("</THEAD>")

        out.write("<TBODY>")
        issues = sorted(jtix_buckets[b], key=itemgetter(4), reverse=False)
        for (jissue, category, impact, workaround_suggested, priority, b0errata, workaround_implemented) in issues:
            summary = ''
            ilinks = ''
            components = ''
            if QUERY_JIRA:
                # XXX Not sure if this really saves any time, 400 seconds versus 376 without the fields
                # XXX although it was just one comparison
                # XXX i = jira.issue(jissue, fields='summary,issuelinks,components')
                try:
                    i = jira.issue(jissue)
                    summary = i.fields.summary
                    issuelinks = i.fields.issuelinks
                    # Get the key from the issuelink
                    outlinks = [jira.issue_link(x).outwardIssue.key for x in issuelinks]
                    inlinks = [jira.issue_link(x).inwardIssue.key for x in issuelinks]
                    ilinks = outlinks + inlinks
                    ilinks = list(OrderedDict.fromkeys(ilinks))
                    ilinks = ["<a href=https://netronome.atlassian.net/browse/%s> %s </a>" % (x.rstrip().lstrip(), x.rstrip().lstrip()) for x in ilinks]
                    ilinks = ', '.join(ilinks)
                    components = ''
                    if len(i.fields.components) >= 1:
                        components = i.fields.components[0]
                    curtime = time.time()
                    diff = curtime - lasttime
                    total = total + diff
                    print >> sys.stderr, "%s %f %f" % (jissue, diff, total)
                    lasttime = curtime
                except:
                    pass
            out.write("<TR>")
            out.write(""" <TD> <a href=https://netronome.atlassian.net/browse/%s> %s </a> </TD> """ % (jissue,jissue) +
                      """ <TD> %s </TD> """ % ilinks +
                      """ <TD> %s </TD> """ % category +
                      """ <TD> %s </TD> """ % summary +
                      """ <TD> %s </TD> """ % impact +
                      """ <TD> %s </TD> """ % workaround_suggested +
                      """ <TD> %s </TD> """ % workaround_implemented +
                      """ <TD> %s </TD> """ % priority +
                      """ <TD> %s </TD> """ % b0errata +
                      """ <TD> %s </TD> """ % components)
            out.write("</TR>")

        out.write("</TBODY>")
        out.write("""<caption>%s (%d issues)</caption>""" % (b, len(issues)))
        out.write("</TABLE><br>")

#------------------------------------------------------------------------------
usage = ("usage: %s -u %s -p %s ") % (sys.argv[0], "<jira username>", "<jira passwd>")
parser = OptionParser(usage=usage)
parser.add_option("-u", "--username", type="string", dest="un", action="store", default='automation', help="JIRA username")
parser.add_option("-p", "--passwd", type="string", dest="pd", action="store", default='x8t5p2g6w5m3g8r1', help="JIRA password")

(options, args) = parser.parse_args()

if not options.un:
    print >> sys.stderr, "username is required"
    print usage
    sys.exit(1)
un = options.un

if not options.pd:
    pd = getpass.getpass("Password: ")
else:
    pd = options.pd

# Check for DATA errors
for j in jtix:
    if len(j) != 7:
        print >> sys.stderr, "%s does not have the right fields" % j

genwiki(user=un, passwd=pd)

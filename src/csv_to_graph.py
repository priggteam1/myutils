#!/usr/bin/env python

from command import *
import sys
import os
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab

MAX_DAC_INDEX=101

# Case lookup
casename_to_description = {
   "case_1a" : "1.4Ghz/DDR1333/QDR233/VCORE1.05/VME1.125/Power",
   "case_1b" : "1.4Ghz/DDR1333/QDR233/VCORE1.05/VME1.125/Power-Crypto",
   "case_1c" : "1.4Ghz/DDR1333/QDR233/VCORE1.05/VME1.125/Power -Crypto -TCAM",
   "case_2a" : "800Mhz/DDR1066/QDR200/VCORE1.0/VME1.0/Power -Crypto",
   "case_2b" : "800Mhz/DDR1066/QDR200/VCORE1.0/VME1.0/Power -Crypto -TCAM",
   "case_3a" : "1.2Ghz/DDR1333/QDR300/VCORE1.0/VME1.05/Power -Crypto",
   "case_3b" : "1.2Ghz/DDR1333/QDR200/VCORE1.0/VME1.05/Power -Crypto",
   "case_3c" : "1.2Ghz/DDR1066/QDR200/VCORE1.0/VME1.05/Power -Crypto",
   "case_4a" : "1.0Ghz/DDR1066/QDR250/VCORE1.0/VME1.0/Power -Crypto",
   "case_4b" : "1.0Ghz/DDR1066/QDR250/VCORE1.0/VME1.0/Power -Crypto -TCAM",
}

# XXX hack for imon reading bug
correct_current_multiplier = {
   "budvar" : 3,
   # as per Ed, we do need a multiplier on cloudshield even with mod
   "cloudshield1" : 1.48
}

def save_data(title='Cayenne Power', case='', temp=None, watts=None, dpm=None, vpm=None, v12=None, v33=None):
   # file to save in
   pf = "cayenne_power_%s%s" % (case.replace(' ', '_'), '.csv')
   df = os.path.abspath(os.path.join(os.environ['NFP_VALIDATION_BASE'],
                                     'power-test/results/cayenne/%s' % pf))
   # csv header
   header  = 'TEMPERATURE (C), V12, V3.3, POWER (W), DPM, VMON, COMMENTS'
   lines = [header]
   # transform dpm and vpm to be a list of strings instead of list of lists
   d = [', '.join(item) for item in dpm]
   v = [', '.join(item) for item in vpm]
   # zip up the actual data for easier extraction
   zipped_data = zip(temp,v12,v33,watts,d,v)
   for (t,c12,c33,w,d,v) in zipped_data:
      row = '%d, %f, %f, %f, "%s", "%s",,' % (t, c12, c33, w, d, v)
      lines.append(row)

   # append the case so that the saved file tracks it
   # currently formatted as a row with an empty line
   lines.append(os.linesep)
   de = casename_to_description[case]
   lines.append(',,,,,"","",%s,' % de)

   # write to file
   f = open(df, "w")
   f.write('\n'.join(lines))
   f.close()

   return

def generate_plot(title='Cayenne Power', case='', temp=None, watts=None, dpm=None, vpm=None, v12=None, v33=None):
   """ Generate plot for temperature versus power with dpm and vpm meta-data"""

   fig = plt.figure()

   # size
   defsize = fig.get_size_inches()
   fig.set_size_inches((defsize[0]*3, defsize[1]*3))

    # labels
   plt.xlabel("Temperature")
   plt.ylabel("Power")
   plt.title(title)

   plt.plot(temp, watts, color='r', marker='D', label=case)

   plt.legend()

   pngf = "cayenne_power_%s" % (case.replace(' ', '_'))
   f = os.path.abspath(os.path.join(os.environ['NFP_VALIDATION_BASE'],
                                    'power-test/results/cayenne/%s' % pngf))

   fig.savefig(f)

   return f

def wait_cdp(ip) :
   i = 0
   while True:
      ret = shell("ping -c 1 %s" % ip)
      if ret[0] == 0:
         break
      print "%d" % i
      i = i + 1
   print "Linux host %s is up" % ip

def nfp_cpp_output(ssh_cmd) :
   ret = shell(ssh_cmd)
   W1=ret[1][0].split(":")
   result_word=W1[1].strip()
   x=int(result_word, 16)
   return x

def nfp_temp_output(ssh_cmd) :
   ret = shell(ssh_cmd)
   W1=ret[1][1].split("=")
   result_word=W1[1].strip()
   x=int(result_word)
   return x

def nfp_raw_output(ssh_cmd) :
   ret = shell(ssh_cmd)
   return ret[1]

def validate_incr(ssh_cmd, retry_times=3) :
   num_retries=retry_times
   old_val = nfp_cpp_output(ssh_cmd)
   while (num_retries > 0) :
      cur_val = nfp_cpp_output(ssh_cmd)
      if (cur_val <= old_val) :
         return False;
      num_retries = num_retries - 1
      old_val = nfp_cpp_output(ssh_cmd)
   return True;

def logoutput(logstring) :
   stprintf("OUTPUT", "%s\n", logstring)


def capture_power(ip, nfp, case, stop_temp=0x63) :
   load("ad7928")
   load("nfp3200_therm")
   index=0
   # read_temp = "ssh root@%s nfp-temp --nfp=%d " % (ip, nfp)
   read_temp = "ssh root@%s nfp-cpp --len=4 --nfp=%d ddr:0x10000044 " % (ip, nfp)
   read_dpm  = "ssh root@%s LD_LIBRARY_PATH=/root/install-nfparm/usr/lib /root/install-nfparm/usr/bin/cayenne-dpm --nfp=%d " % (ip, nfp)
   read_vpm  = "ssh root@%s LD_LIBRARY_PATH=/root/install-nfparm/usr/lib /root/install-nfparm/usr/bin/cayenne-vmon --nfp=%d " % (ip, nfp)
   t = []
   w = []
   v12 = []
   v33 = []
   d = []
   v = []
   while True:
      # Read DAC number until limit is reached
      cur_dac = nfp_cpp_output(read_temp)
      cur_temp = nfp3200_therm.getdactemp(cur_dac)

      # Get fields from rsp environment we need
      # run("get_rsvp_env", "bd0_", 0,0,0,0,0,1,0)
      (foo0, foo1, ch2_120, ch3_33, foo4, foo5, foo6, foo7) = ad7928.current_mon("bd0_imon1_")

      # cur_dpm = nfp_raw_output(read_dpm)
      # cur_vpm = nfp_raw_output(read_vpm)

      cur_dpm = []
      cur_vpm = []

      # Calculate watts
      # watts = I*V for 12v + I*V for 3.3v
      # correct_current adjusts for imon
      # reading bug (CRPHW-XXX)
      v33_curr = correct_current_multiplier[CDPNAME] * ch3_33
      v12_curr = correct_current_multiplier[CDPNAME] * ch2_120
      cur_watts=v12_curr*12 + v33_curr*3.3

      # Retrieve DPM and VPM output
      print("Temperature %d DAC %d Consumption %f" % (cur_temp, cur_dac, cur_watts))
      print cur_dpm
      print cur_vpm

      t.append(cur_temp)
      w.append(cur_watts)
      d.append(cur_dpm)
      v.append(cur_vpm)
      v12.append(v12_curr)
      v33.append(v33_curr)

      save_data(title='Cayenne Power', case=case, temp=t, watts=w, dpm=d, vpm=v, v12=v12, v33=v33)

      # pf = generate_plot(title='Cayenne Power', case=case, temp=t, watts=w, dpm=d, vpm=v, v12=v12, v33=v33)
      # print 'plot file is %s' % pf

      if (cur_dac <= stop_temp) :
         print("Final temperature reached")
         print("Power consumption %f" % cur_watts)
         print("-----------------------------------------------------")
         print("-----------------------------------------------------")
         print("-----------------------------------------------------")
         print("-----------------------------------------------------")
         print("-----------------------------------------------------")
         print("-----------------------------------------------------")
         print("-----------------------------------------------------")
         print("-----------------------------------------------------")
         print("-----------------------------------------------------")
         print("-----------------------------------------------------")
         print("-----------------------------------------------------")
         print("-----------------------------------------------------")
         print("-----------------------------------------------------")
         print("-----------------------------------------------------")
         print("-----------------------------------------------------")
         break

   consistent = (len(t) == len(w) == len(d) == len(v))
   if not consistent:
      print '!! ERROR, inconsistent data set !!'
      exit

   return (t, w, d, v)
               
#----------------------------------------------------------------------# 
#----------------------------------------------------------------------# 
max_dac_index=MAX_DAC_INDEX

config = sys.argv[1]
ip = sys.argv[2]
nfp = int(sys.argv[3])
init_sys = sys.argv[4]
case = sys.argv[5]
# case = 'case 1a'
print "Using config %s, ip %s nfp=%d" % (config, ip, nfp)
run("%s" % config)

if (init_sys == "INIT") :
   print("init_system")
   run("init_system")
else :
   print("new_system")
   run("new_system")

(foo, CDPNAME, access) = config.split("_")

# First, make sure linux is running on cdp

wait_cdp(ip)

# Linux is up, now wait for power application to be started
# One indicator is pki is running....wait till pki is running

pki_state = "ssh root@%s nfp-cpp --nfp=%d gs:0x100" % (ip, nfp)

pki_is_running = False
pki_is_running = True

while (not pki_is_running) :
   pki_is_running = validate_incr(pki_state)
   print("Waiting for power app to be started...")

# Power application is up

print("Power application is running collect power data ")

(temp, watts, dpm, vpm) = capture_power(ip, nfp, case, max_dac_index)


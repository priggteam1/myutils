
#   Good reference:  https://en.wikipedia.org/wiki/Ethernet_frame
#   Ethernet specific (20 bytes)
#   12 bytes = inter-frame gap (https://en.wikipedia.org/wiki/Interpacket_gap)
#   8 bytes = MAC preamble
#   Ethernet frame (64 bytes)
#   14 bytes = MAC header
#   46 bytes = Minimum payload size
#   4 bytes = Ethernet CRC
#   Len <= 1500 Ethernet 802.3 frame (very old)
#   Len >= 1536   Ethernet II frame

ipg=12          # Physical Layer idle time between packets
mac_preamble=8  # Physical Layer 7 octets preamble + 1 start frame delimiter
mac_header=14   # DST_MAC=6 SRC_MAC=6, LEN=2 (length could be 
#        64, 68, 128, 192, 256, 512, 1024, 1500, 9000
payload=[46, 50, 110, 174, 238, 494, 1006, 1482, 8982]
vlan_tag=[4,8]
eth_crc=4
#   
#   Packet rate calculations
#   
#   Peak packet rate calculated as:  (10*10^9) bits/sec / (84 bytes * 8) = 14,880,952 pps
#   1500 MTU packet rate calculated as: (10*10^9) bits/sec / (1538 bytes * 8) = 812,744 pps
def port_bit_speed(port_speed):
    x=port_speed*(10**9)
    return x

def pps_calc(port_speed, packet_size):
    bits_speed=port_bit_speed(port_speed)
    pps=bits_speed/(packet_size*8)
    return pps

#   
#   Time budget
#   This is the important part to wrap-your-head around.
#   
#   With 14.88 Mpps the time budget for processing a single packet is:
#   
#   67.2 ns (nanosecond) (calc as: 1/14880952*10^9 ns)
def budget_time(pps):
    packet_time_ns=float(float(1.0)/(pps * 10.0**9))
    return float(packet_time_ns*10.0**9)

def cpu_cycles(processor_speed, packet_time_ns):
    instr_second=processor_speed*float(1024*1024*1024)
    cycles=(packet_time_ns)*float(instr_second)
    return float(cycles)

rates=[10, 25, 40, 50, 100]
processors=[3.0, 0.500, 0.600, 0.700, 0.800, 0.900]
for processor_speed in processors:

    print "Host Processor Speed -- Port Speed"
    for rate_gps in rates:
        print "%f Ghz -- %d Gbps\n" % (processor_speed,rate_gps)
        t=0.0
        print "\tP Size \t\tFPS \t\tRate \tTime(ns) \t\tCPU_CYCLES\n"
        for p in payload:
            packet_length=ipg+mac_preamble+mac_header+p+eth_crc
            eth_pkt=p+mac_header+eth_crc
            pps = pps_calc(rate_gps, packet_length)
            gbps = float(float((pps * eth_pkt * 8)) / (float(1000*1000*1000)))
            t=budget_time(pps)
            cycles=cpu_cycles(processor_speed,t)
            if (rate_gps == 100) and (p==46) :
                print "\t%5d \t%12d  \t%5.6f \t%3.12f \t\t%3.12f\n" % (eth_pkt, pps, gbps, t*10**9, cycles)
            else :
                print "\t%5d \t%12d \t%5.6f \t%3.12f \t%3.12f\n" % (eth_pkt, pps, gbps, t*10**9, cycles)


    
#   
#   This corrospond to approx: 201 CPU cycles on a 3GHz CPU (assuming only one instruction per cycle, disregarding superscalar/pipelined CPUs). Only having 201 clock-cycles processing time per packet is very little.
#   
#   Relate these numbers to something
#   This 67.2ns number is hard to use for anything, if we cannot relate this to some other time measurements.
#   
#   cache-misses
#   A single cache-miss takes: 32 ns (measured on a E5-2650 CPU). Thus, with just two cache-misses (2x32=64ns), almost the total 67.2 ns budget is gone. The Linux skb (sk_buff) is 4 cache-lines (on 64-bit), and the kernel e.g. insists on writing zeros to these cache-lines, during allocation of an skb.
#   
#   cache-references
#   We might not "suffer" a full cache-miss, sometimes the memory is available in L2 or L3 cache.  Thus, it is useful to know these time measurements.  Measured on my E5-2630 CPU (with lmbench command "lat_mem_rd 1024 128"), L2 access costs 4.3ns, and L3 access costs 7.9ns.
#   

import csv
import re

cmu_base_addr=-2
rxtx_base_addr=-2
regs=0
c_lines = {}
for csv_name in ['Serdes_Regs_cmu.csv', 'Serdes_Regs_rxtx.csv'] :
    with open(csv_name, 'rb') as csvfile:
        invecas_reader = csv.reader(csvfile, delimiter=',', quotechar='|')
        for row in invecas_reader:
           if (len(row) > 4) :
              if not re.search("FIELDNAME", row[5].upper()) and not re.search("RESERVED", row[5].upper()) :
                 if re.search("CMU_reg", row[3]) :
                    cmu_base_addr=cmu_base_addr+2
                    prefix_tag="CMU_"
                    regs=regs+1
                 if re.search("rxtx_reg", row[3]) :
                    rxtx_base_addr=rxtx_base_addr+2
                    regs=regs+1
                    prefix_tag="RXTX_"
                 if (row[5] != "") :
                    field_name = row[5]
                    if re.search("\[", field_name) :
                        r = re.compile('[ \[ : \]]+')
                        x = r.split(row[5])
                        field_name=x[0]
                    if re.search("\[", row[6]) :
                        r = re.compile('[ \[ : \]]+')
                        x = r.split(row[6])
                        start = x[2]
                        end = x[1]
                    else :
                       start=row[6]
                       end = start
                 else :
                    field="row"+row[0]
                 field_name=field_name.replace("_","")
                 c_key=prefix_tag+field_name.upper()
                 if (prefix_tag == "CMU_") :
                     field=c_key+"\t"+hex(cmu_base_addr) + "\t" + start + "\t" + end
                 if (prefix_tag == "RXTX_") :
                     field=c_key+"\t"+hex(rxtx_base_addr) + "\t" + start + "\t" + end
                 in_use = c_lines.get(c_key)
                 if ( in_use == None ) :
                    c_lines[c_key] = field

    print "Total Number of registers ", regs

    for x in c_lines.itervalues() :
        print x
            
            

#!/usr/bin/env python

#
#  Plot
#    x axis - Clk Frequency
#    y axis - CTM Peak Utilization
#
#    assume data format:
#    Test #,ClkFreq,ME Islands,PCIE MEs,PktSz,PktNum,DMA Speed,CTMPeak,ActiveCnt,Result
#    1,800,1,4,64,50000,7.7Gbps,331,0,PASS
#

#
import os
import sys
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab
import matplotlib.figure as mfig

cmd = os.path.abspath(sys.argv[0])
cmddir = os.path.dirname(cmd)

if len(sys.argv) < 2:
    print >> sys.stderr, "Usage:  <files...> ending in .csv"
    sys.exit(1)

files = sys.argv[1:]

color_dict = { 64 : 'r', 
              128 : 'b', 
              192 : 'g', 
              256 : "c", 
              384 : "m", 
              512 : 'y',
              1024 : 'k',
              1518 : '0.60',
              2048 : '0.70',
              4096 : '0.80'
              }

for f in files:

    fig = plt.figure()

    # control pixel/dpi for image
    defsize = fig.get_size_inches()
    fig.set_size_inches((defsize[0]*3, defsize[1]*3))

    plt.xlabel("CLK Frequency")
    plt.ylabel("CTM Peak")
    mytitle="CTM Buffer Utilization - "

    csvfile = os.path.join(cmddir, f)

    # find our case line brute force from file
    case = f
    for line in open(csvfile, "r"):
        if line.find('MAC') >= 0:
            tmp = line.split(',')
            for s in tmp:
                if s.find('Split') >= 0:
                    case = s
                    break

    print 'Processing case=%s' % case
    mytitle=mytitle+case
    plt.title(mytitle)

    r = mlab.csv2rec(csvfile)
    #
    # xdata=CLK_frequency ydata=ctm_peak_utilization
    #
    xdata=[]
    ydata=[]
    #
    x_sdata=[]
    y_sdata=[]
    #
    #
    # find all stats based on each packet size
    #
    for p in 64, 128, 256, 384, 512, 1024, 1518, 2048, 4096 :
     xdata=[]
     ydata=[]
     label = "%s"  % (p)
     for x in r :
        # ignore any TBD lines
        if (x[4] == p) and (x[6] != "TBD"):
           print p, x[4], x[7]
           xdata.append(x[1])
           ydata.append(int(x[7]))
     # Plot out this packet size before moving to next one
     plt.plot(xdata, ydata, color=color_dict[p], marker='o', label=label)

    slabel=""
    #
    plt.plot(x_sdata, y_sdata, color='r', marker='s', label=slabel, linestyle='None')

    #
    #  Define x axis range for clock frequency  500 - 1000
    #  Define y axis range for ctm peak numbers 0 - 400
    #
    plt.legend(loc='upper right')
    plt.axis([ 600,1000,  0,400 ])

    # png
    png = f.replace('.csv', '.png')
    fig.savefig(png, format='png')
    print 'png --> %s' % png

    # pdf
    pdf = f.replace('.csv', '.pdf')
    fig.savefig(pdf, format='pdf')
    print 'png --> %s' % pdf

/*
 * Copyright (C) 2009-2013 Netronome Systems, Inc.  All rights reserved.
 */

#ifndef __NBI_WIRE_UC__
#define __NBI_WIRE_UC__

#include <aggregate.uc>
#include <nfp_chipres.h>
#include <stdmac.uc>
#include <ring_utils.uc>

#include "wsystem.h"
#include "nfp_blm_iface.h"
#include "nfp_blm_api.uc"
#include "wmemory.h"
#include "system_init.uc"

#define xpb_csr_addr(island, slave_idx, tgt_dev, csr_offset)    ((0 <<31)       | \
                                                                (0 <<30)        | \
                                                                (8 <<24)        | \
                                                                (island <<24)   | \
                                                                (slave_idx <<22)| \
                                                                (tgt_dev <<16)  | \
                                                                (csr_offset))

#define  WIRE_STATS 1
#ifdef WIRE_STATS_ENABLE
    #define  WIRE_STATS 1
#endif
.alloc_mem WIRE_DID8_TABLE_BASE   i28.imem+0x3f0000 global (128*8) 8
#if (__nfp_has_island(29))
.alloc_mem WIRE_DID9_TABLE_BASE   i29.imem+0x3f0000 global (128*8) 8
#endif

#define_eval __PORT 0
#while (__PORT < 128)
    #define_eval _DID_OFFSET_LO    (__PORT <<3)
    #define_eval _DID_OFFSET_HI    (_DID_OFFSET_LO + 4)
    /* NBI-8 DID-0 */
    .init WIRE_DID8_TABLE_BASE+_DID_OFFSET_LO   (0x00000000)
    .init WIRE_DID8_TABLE_BASE+_DID_OFFSET_HI   (0x40000000)
    /* NBI-9 DID-0 */
    #if (__nfp_has_island(29))
        .init WIRE_DID9_TABLE_BASE+_DID_OFFSET_LO   (0x00000000)
        .init WIRE_DID9_TABLE_BASE+_DID_OFFSET_HI   (0xc0000000)
    #endif

    .init_csr xpb:Nbi0IsldXpbMap.NbiTopXpbMap.PktPreclassifier.Characterization.PortCfg/**/__PORT.Analysis 0x1 const
#if (__nfp_has_island(9))
    .init_csr xpb:Nbi1IsldXpbMap.NbiTopXpbMap.PktPreclassifier.Characterization.PortCfg/**/__PORT.Analysis 0x1 const
#endif
    #define_eval __PORT (__PORT + 1)
#endloop
#undef __PORT
#undef _DID_OFFSET_LO
#undef _DID_OFFSET_HI

#macro wire_stats(COUNTER)
#ifdef WIRE_STATS
.begin
    .reg _addr
    .reg addrhi

    #define __ADDRHI        ((WIRE_STATS_BASE >>8) & 0xFF000000)
    #define __ADDRLO        ((WIRE_STATS_BASE >>0) & 0xFFFFFFFF)
    move(addrhi, __ADDRHI)
    move(_addr, __ADDRLO)
    alu[_addr, _addr, +, COUNTER]
    mem[incr64, --, addrhi, <<8, _addr]
    #undef __ADDRHI
    #undef __ADDRLO
.end
#endif
#endm /* wire_stats */


#ifdef ENABLE_WIRE_INTERFACE
 #include "wire_if.uc"
#endif

    .reg $header[16]
    .xfer_order $header
    .reg $pmscript[2]
    .xfer_order $pmscript
    .reg moddata
    .reg addr
    .reg offset
    .reg iid meid
    .reg a
    .reg cid
    .reg qid
    .reg nbi
    .reg ctm_split_len
    .sig volatile sig_workq
    .sig volatile sig_ctm_pkt_free
    .sig volatile sig_ring_put
    .sig volatile sig_pkt_wr
    .sig volatile sig_pkt_rd
    .sig volatile sig_rps
    .sig volatile sig_did_read
    .reg packetnum
    .reg seqnum
    .reg seqrnum
    .reg len
    .reg mask
    .reg csr_data
    .reg override_data
    .reg PmScriptOffset
    .reg tmp
    .reg @count
    .reg @dbg_count

#ifdef ENABLE_WIRE_INTERFACE
    .reg $work[NBI_WIRE_INTF_WORK_Q_SIZE]
    .xfer_order $work
#endif

#define WIRE_STATS_SIZE 128
.alloc_mem WIRE_STATS_BASE        i24.emem_cache_upper                 global WIRE_STATS_SIZE 64
.declare_resource WIRE_STATS_OFFSETS global WIRE_STATS_SIZE
.alloc_resource WIRE_RCVD                WIRE_STATS_OFFSETS global 8
.alloc_resource WIRE_RCVD_IPV4           WIRE_STATS_OFFSETS global 8
.alloc_resource WIRE_RCVD_IPV4_EXCEPTION WIRE_STATS_OFFSETS global 8
.alloc_resource WIRE_RCVD_ARP            WIRE_STATS_OFFSETS global 8
.alloc_resource WIRE_RCVD_NON_IP         WIRE_STATS_OFFSETS global 8
.alloc_resource WIRE_RCVD_ERR            WIRE_STATS_OFFSETS global 8
.alloc_resource WIRE_RCVD_ACT_FWD        WIRE_STATS_OFFSETS global 8
.alloc_resource WIRE_RCVD_ACT_DROP       WIRE_STATS_OFFSETS global 8
.alloc_resource WIRE_RCVD_IP_PROTO_TCP   WIRE_STATS_OFFSETS global 8
.alloc_resource WIRE_RCVD_IP_PROTO_UDP   WIRE_STATS_OFFSETS global 8
.alloc_resource WIRE_RCVD_IP_PROTO_OTHER WIRE_STATS_OFFSETS global 8

    local_csr_rd[ACTIVE_CTX_STS]
    immed[tmp, 0]

    alu[iid, --, b, tmp, >>25]
    alu[meid, --, b, tmp, >>3]
    alu[iid, iid, AND, 0x3f]
    alu[meid, meid, AND, 0xf]
    .if (iid == 0x20)
           .if (meid == 0x4)
                .if (ctx() == 0)

                    addr = (0x1 << 25)             //addr used as temp variable
                    addr = addr | (0x7 << 8) //addr used as temp variable
                    addr = addr | 0x1        //addr used as temp variable
                    $header[0] = addr | ( 0x1 <<16)
                    $header[1] = addr | ( 0x2 <<16)
                    $header[2] = addr | ( 0x4 <<16)
                    $header[3] = addr | ( 0x8 <<16)
                    addr = (0x1 << 26)             //addr used as temp variable
                    addr = addr | (0x7 << 8) //addr used as temp variable
                    addr = addr | 0x1        //addr used as temp variable
                    $header[4] = addr | ( 0x1 <<16)
                    $header[5] = addr | ( 0x2 <<16)
                    $header[6] = addr | ( 0x4 <<16)
                    $header[7] = addr | ( 0x8 <<16)
                    addr      = 0x08140044
                    ct[xpb_write,$header[0],addr,0,4],sig_done[sig_rps]
                    ct[xpb_write,$header[4],addr,0,4],sig_done[sig_did_read]
                    ctx_arb[sig_rps,sig_did_read],all
                .endif
            .endif
   .endif

    .if (ctx() == 0)
        @count = 0
        @dbg_count = 0
        local_csr_wr[mailbox0, @count]
        local_csr_wr[mailbox1, @count]
        local_csr_wr[mailbox3, @count]

        // default NoOp PM script
        immed[@count,0x00]
        immed_w1[@count, 0x80e0]
        local_csr_wr[mailbox2, @count]
        @count = 0

        // NBI-Wire Interface initialization
        #ifdef ENABLE_WIRE_INTERFACE
          nbi_wire_if_init_module(iid)
        #endif

        // system-wide initialization
        #if WIRE == 0
          system_init()
        #endif
    .endif

    #ifdef ENABLE_WIRE_INTERFACE
        nbi_wire_if_init_context()
    #endif


    #if 0
    #define _IND_REF      (1       | \ /* override signal_master (OSM) */
                          (1 << 1) | \ /* override (OM) data_master, data_master_island */
                          (1 << 3) | \ /* override (OD) */
                          (1 << 6) | \ /* override (OB) */
                          (1 << 7) | \ /* override (OL) */
                          (6 << 8) | \ /* LEN=56bytes 0=8B, 1=16B, 2=24B,...6=56B */
                          (1 << 15))   /* override (OV_TAT) */

   #define_eval _TAT_CSR  (1        | \ /* override Token */
                          (1 << 1)  | \ /* override Action */
                          (1 << 2)  | \ /* override Target */
                          (1 << 5)  | \ /* Token = 1 = unicast */
                          (3 << 7)  | \ /* Action = 3 = Packet_ready */
                          (1 << 12))    /* Target = 1 = NBI */
    #endif
#ifdef WIRE_PACKET_READY_CMD
    #define_eval _IND_REF (1       | \
                          (1 << 1) | \
                          (1 << 3) | \
                          (1 << 6) | \
                          (1 << 7) | \
                          (6 << 8) | \
                          (1 << 15))
#else
    #define_eval _IND_REF (1       | \
                          (1 << 1) | \
                          (1 << 3) | \
                          (1 << 6) | \
                          (1 << 7) | \
                          (6 << 8))
#endif


   #define_eval _TAT_CSR  (1        | \
                          (1 << 1)  | \
                          (1 << 2)  | \
                          (1 << 5)  | \
                          (3 << 7)  | \
                          (1 << 12))

   #define_eval _TAT_CSR_MULTICAST  (1        | \
                                    (1 << 1)  | \
                                    (1 << 2)  | \
                                    (2 << 5)  | \
                                    (3 << 7)  | \
                                    (1 << 12))

    //#define PE_WORK_Q_TEST 1
    #ifdef PE_WORK_Q_TEST
        // Add This Islands ME to neighboring Islands Work Q
        alu[iid, iid, XOR, 1]
        alu[iid, --, b, iid, <<24]

        immed[offset, 0x8000, <<16]
        // oofset will be used in 'packet_add_thread' & 'packet_processing_complete' & PM Script update
        alu[offset, offset, OR, iid]
        local_csr_wr[mailbox1, offset]
    #else
        immed[offset, 0x0]
    #endif

.begin
    .reg $x[2]
    .xfer_order $x
    .sig nbi_sig
    #define_eval bpool 0
    #define_eval NbiNum 0
    #define_eval __bp_offset      (0x20 + (bpool <<2))
    #define_eval __addr           xpb_csr_addr(NbiNum, 0, 16, __bp_offset)
    move(addr, __addr)
    ct[xpb_read, $x[0], addr, 0, 1], sig_done[nbi_sig]
    wait_bp_read#:
      br_!signal[nbi_sig, wait_bp_read#]

    #if (__REVISION_MIN < __REVISION_B0)
        #define_eval PM_SCRIPT_OFFSET   32
    #else
        #define_eval PM_SCRIPT_OFFSET   128
    #endif
    /* If bit is set, Packet_Offset = 64 (A0 or B0) */
    br_bset[$x[0], 12, offset_64b#]
    /* If bit is clear, Packet_Offset = 32 in A0, 128 in B0 */
    immed[PmScriptOffset, (PM_SCRIPT_OFFSET)]
    br[PmScriptOffset_done#]
offset_64b#:
    immed[PmScriptOffset, 64]
PmScriptOffset_done#:
    #undef bpool
    #undef NbiNum
    #undef __bp_offset
    #undef __addr
.end

    .while 1
        offset = 0
        addr = 16
        /* Add Thread to packet engine workQ */
        immed[len, 128]
        alu[len, len, OR, 0xf, <<8]
        mem[packet_add_thread, $header[0], addr, offset,<<8, max_8], indirect_ref, ctx_swap[sig_workq]

    #ifndef ENABLE_WIRE_INTERFACE

        /* ME Wakes up when signal arrives. It now has the header and metadata in first 6 transfer registers */
        //Header[1 ;63] Split - Indicates the packet is split between
        //Header[2 ;61] Reserved
        //Header[29;32] Mu Ptr - Pointer to the memory buffer    @2KB boundaries.
        //Header[6 ;26] CTM Number - CTM that contains the    packet.
        //Header[10;16] Packet Number - Used to address the packet    in the CTM.
        //Header[2 ;14] Blist - Buffer List Queue associated with the Mu Ptr.
        //Header[14; 0] Packet Length in bytes.

        //wire_stats(WIRE_RCVD)
        /* Get Packet Number */
        packetnum = $header[0] >> 16
        mask = 0x3ff
        packetnum = packetnum & mask

        #if (streq('PCLOAD', 'pe_ipv4_fwd_npc'))
            ;; Read packet status - Through which NBI packet arrived
            rps_retry#:
            mem[packet_read_packet_status, $pmscript[0], packetnum, 0], sig_done[sig_rps]
            /* Wait for RPS */
            ctx_arb[sig_rps]
            .if ($pmscript[0] == 0xffffffff)
                br[rps_retry#]
            .endif
        #endif

        .reg fwd_action
        /* Lookup */
        #if (streq('PCLOAD', 'pe_ipv4_fwd_npc'))
            /* Extract Meta data */
            .reg pkt_type
            .reg did
            .reg did_base
            .reg did_offset

            alu[pkt_type, 0xff, AND, $header[4]]
            .if (pkt_type == IPV4_EXCEPTION_PKT)
                alu[did, --, b, $header[3]]
                wire_stats(WIRE_RCVD_IPV4_EXCEPTION)
            .elif (pkt_type == IPV4_PKT)
                ld_field_w_clr[did, 0011, $header[5],>>16]
                wire_stats(WIRE_RCVD_IPV4)
            .elif (pkt_type == ARP_PKT)
                alu[did, --, b, $header[3]]
                wire_stats(WIRE_RCVD_ARP)
            .elif (pkt_type == NON_IPV4_PKT)
                alu[did, --, b, $header[3]]
                wire_stats(WIRE_RCVD_NON_IP)
            .else /* Errored packets */
                immed[did, 1] /* DID action for DROP */
                wire_stats(WIRE_RCVD_ERR)
            .endif

            alu[nbi, 0x3,  AND, $pmscript[0], >>24]
            alu[nbi, --, b, nbi, >>1]
            alu[ctm_split_len, 0x3, AND, $pmscript[0], >>16]
            ld_field_w_clr[cid, 0001, $header[5]]

            .if (nbi == 0)
                move(did_base, 0x9C000000)
            .else
                move(did_base, 0x9D000000)
            .endif
            move(did_offset, 0x003f0000)
            alu[did, --, b, cid, <<3]
            alu[did_offset, did_offset, +, did] /* DID entry is 8 bytes */
            mem[read, $pmscript[0],  did_base, <<8, did_offset, 1],sig_done[sig_did_read]
            ctx_arb[sig_did_read]

            /* Through which NBI packet need to exit */
            alu[nbi, 0x1, AND, $pmscript[1], >>31]
            /* check if forward packets to programmed QID */
            alu[tmp, 0x1, AND, $pmscript[1], >>29]
            .if(tmp)
                /* Destination QID - Software must make sure this QID is valid */
                ld_field_w_clr[qid, 0011, $pmscript[1], >>16]
                immed[did_offset, 0x3ff]
                alu[qid, qid, AND, did_offset]
            .else
                alu[qid, --, b, cid, <<3]
            .endif

            /* Forward action */
            alu[fwd_action, 0x1, AND, $pmscript[1], >>30]

        #else
            // Meta data Bytes[7:4] = 1;31 = NBI#, 30;0 = channel#
            alu[nbi, 0x1, AND, $header[3], >>31]
            ld_field_w_clr[cid, 0001, $header[3]]
            alu[qid, --, b, cid, <<3]
            immed[fwd_action, 0x1]
        #endif

        /* Prepare to send packet to egress */
        /* Generate Rewrite Script */
        local_csr_rd[mailbox2]
        immed[moddata,0]
        alu[ $pmscript[0], --, b, moddata]
        local_csr_rd[mailbox3]
        immed[moddata,0]
        alu[ $pmscript[1], --, b, moddata]

        /* Write Rewrite Script */
        //[8;32] Don'Care
        //[1;31] AddressMode
        //[6;25] Don'tCare
        //[9;16] Packet Number
        //[5;11] Don'tCare
        //[11;0] Offset in Bytes
        addr = PmScriptOffset - 8
        tmp  = packetnum << 16
        addr = addr | tmp
        addr = addr | (1<<31)
        mem[write, $pmscript[0], addr, offset, <<8, 1], sig_done[sig_pkt_wr]
        mem[read, $pmscript[0], addr, offset, <<8, 1], sig_done[sig_pkt_rd]

        /* DEBUG: NFPBSP-2513 */
        .reg mu_pkt_addr_hi
        .reg mu_pkt_addr_lo
        .reg ip_proto
        .reg pkt_len
        .reg $pkt_data[8]
        .xfer_order $pkt_data
        .sig mu_pkt_read_sig

        alu[tmp, --, b, $header[1], <<3]
        ld_field_w_clr[mu_pkt_addr_hi, 1000, tmp]
        ld_field_w_clr[mu_pkt_addr_lo, 1110, tmp, <<8]
        //offset at CTM_SPLIT_LEN where pkt data starts in MU
        pkt_len = 256
        alu[--,ctm_split_len, OR, 0]
        alu[tmp, --, b, pkt_len, <<indirect]
        alu[mu_pkt_addr_lo, mu_pkt_addr_lo, +, tmp]
        mem[read, $pkt_data[0], mu_pkt_addr_hi, <<8, mu_pkt_addr_lo, 2], sig_done[mu_pkt_read_sig]

        alu[ip_proto, $header[11], AND, 0xff]

        /* Generate Packet Processing Complete */
        //len = len + ctm_offset
        addr = $header[0] + PmScriptOffset
#ifdef WIRE_PACKET_READY_CMD
        immed_w0[tmp, 0x0]
        immed_w1[tmp, 0xfc00]
        alu[addr, addr, AND~, tmp]
        offset = offset | nbi << 30
        offset = offset | iid << 24
        .if (meid & 1)
          offset = offset | 3 << 20
        .else
          offset = offset | 2 << 20
        .endif
#endif
        seqnum = $header[2] >> 16
        mask = 0xfff
        seqnum = seqnum & mask
        seqrnum = $header[2] >> 8
        mask = 0x1f
        seqrnum = seqrnum & mask

        //ctx_arb[sig_pkt_wr,sig_pkt_rd],all
        ctx_arb[sig_pkt_wr,sig_pkt_rd, mu_pkt_read_sig],all

        #define IP_PROTO_TCP   0x6
        #define IP_PROTO_UDP   0x11
        .reg tcp_pattern
        .reg udp_pattern
        move(tcp_pattern, 0x8a8b8c8d)
        move(udp_pattern, 0x004b004c)
        immed[tmp, 0x3fff]
        alu[pkt_len, tmp, AND, $header[0]]
        .if(pkt_len > 192)
          .if(ip_proto == IP_PROTO_TCP)
            wire_stats(WIRE_RCVD_IP_PROTO_TCP)
            .if($pkt_data[0] != tcp_pattern)
              immed[fwd_action, 0x0]
            .endif
          .elif(ip_proto == IP_PROTO_UDP)
            wire_stats(WIRE_RCVD_IP_PROTO_UDP)
            .if($pkt_data[0] != udp_pattern)
              immed[fwd_action, 0x0]
            .endif
          .else
              wire_stats(WIRE_RCVD_IP_PROTO_OTHER)
              immed[fwd_action, 0x1]
          .endif
        .endif

        csr_data = (seqnum << 16) | seqrnum
        local_csr_wr[cmd_indirect_ref_0, csr_data]
#ifdef WIRE_PACKET_READY_CMD
        .if (fwd_action)
            csr_data = _TAT_CSR
        .else
            //csr_data = _TAT_CSR_MULTICAST
            csr_data = _TAT_CSR
        .endif
        local_csr_wr[cmd_indirect_ref_1, csr_data]
#endif

        /* Send Packet Processing Complete */
        //[2 cycles] MEM[packet_complete,--,$M[1]], Indirect
        /* Override Data */
        /********************************
         * TODO: REPLACE ARITHMATIC EXPRESSIONS AND OPTIMIZE
         *********************************/
        override_data = _IND_REF
        .if ( PmScriptOffset > 64)
            override_data =  (override_data | (1 <<11)) /* bit[3] of len field adds +64 */
        .endif
        override_data = override_data | (qid<<16); txqueue=0, retry=0, nbi=0[QID=10, NBI=2;12, Retry=1:10, Res=1;11]
#ifdef WIRE_PACKET_READY_CMD
        override_data = override_data | (ctm_split_len<<28); txqueue=0, retry=0, nbi=0[QID=10, NBI=2;12, Retry=1:10, Res=1;11]
#else
        override_data = override_data | (nbi<<28); txqueue=0, retry=0, nbi=0[QID=10, NBI=2;12, Retry=1:10, Res=1;11]
#endif
        .if (fwd_action)
            alu[--,--,B,override_data]
            mem[packet_complete_unicast, --, addr, offset,<<8], indirect_ref
            wire_stats(WIRE_RCVD_ACT_FWD)
        .else
            .reg que_no
            .reg muptr
            .reg ring_addr
            wire_stats(WIRE_RCVD_ACT_DROP)
            #define_eval _NBIX   (8)  //+ BLM_INSTANCE_ID)
            //alu[--,--,B,override_data]
            //mem[packet_complete_drop, --, addr, offset,<<8], indirect_ref
            //alu[que_no, 0x3, AND, $header[0],>>14]
            immed[que_no, 3]
            alu[que_no, que_no, +, BLM_NBI/**/_NBIX/**/_BLQ0_EMU_QID]
            alu[muptr, --, b, $header[1], <<3]
            alu[$header[0], --, b, $header[0]]
            alu[$header[1], --, b, muptr, >>3]
            alu[$header[2], --, b, $pkt_data[0]]
            alu[$header[3], --, b, 0xff]
            move(ring_addr, ((_BLM_NBI/**/_NBIX/**/_BLQ3_EMU_Q_BASE >>8)&0xFF000000))
            #undef _NBIX
            mem[put, $header[0], ring_addr, <<8, que_no, 4], sig_done[sig_ring_put]
            //mem[packet_free_and_signal, --, packetnum, 0], sig_done[sig_ctm_pkt_free]
            //ctx_arb[sig_ctm_pkt_free, sig_ring_put]
            ctx_arb[sig_ring_put]
            @dbg_count = @dbg_count + 1
            local_csr_wr[mailbox1, @count]
        .endif
        /* Check for Lookup Table updates */

    #else  // !defined(ENABLE_WIRE_INTERFACE)

        // NBI I/F - NBI_WIRE_INTF_WORK_Q_SIZE 32-bit words
        // word 1: header[0]
        //         header[0][ 6;26] CTM Number - CTM that contains the packet.
        //         header[0][10;16] Packet Number - Used to address the packet in the CTM.
        //         header[0][ 2;14] Blist - Buffer List Queue associated with the Mu Ptr.
        //         header[0][14; 0] Packet Length in bytes.

        // word 2: header[1]
        //         header[1][ 1;63] Split - Indicates the packet is split between
        //         header[1][ 2;61] Reserved
        //         header[1][29;32] Mu Ptr - Pointer to the memory buffer @2KB boundaries.

        // word 3: header[2] - (metadata) -
        //         header[2][16;16] Sequence Number
        //         header[2][ 8; 8] Sequencer
        //         header[2][ 8; 0] Buffer Pool

        // word 4: header[3] - (metadata) -
        //         header[3][32; 0] Port Number (top bit is the NBI)


        // Add 1st NBI_WIRE_INTF_WORK_Q_SIZE words retrieved from NBI to the
        // NBI-Wire Interface work queue

        // NOTES: 1) It is up to the application to service the work queue and to
        //           ensure it does not overflow.
        //        2) Application must also ensure that buffer memory associated with
        //           each retrieved work element must be freed (for both the CTM-based
        //           packet header and metadata and the IM/EM-based remaining packet
        //           data

        // copy read transfer registers for writing
        #define_eval _ITEM 0
        #while (_ITEM < NBI_WIRE_INTF_WORK_Q_SIZE)
            alu[$work[_ITEM], --, B, $header[_ITEM]]
            #define_eval _ITEM (_ITEM + 1)
        #endloop

        nbi_wire_if_add_work($work[0])

    #endif // !defined(ENABLE_WIRE_INTERFACE)

        /* Update stats */
        @count = @count + 1
        local_csr_wr[mailbox0, @count]

    .endw

    #undef _IND_REF
#endif /*__NBI_WIRE_UC__*/

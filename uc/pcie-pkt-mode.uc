#ifndef __PCIE_PKT_MODE_UC__
#define __PCIE_PKT_MODE_UC__

#include <aggregate.uc>
#include <nfp_chipres.h>
#include <stdmac.uc>
#include <cycle.uc>
#include <ring_utils.uc>
#include <event_autopush.uc>
#include <threads.uc>

#include <nfp6000/nfp_em.h>
#include <nfp6000/nfp_pcie.h>
#include <nfp6000/nfp_nbi_tm.h>
#include <nfp6000/nfp_xpb.h>

#include "../lib/pcie_thornham.uc"

#include "wsystem.h"
#include "nfp_blm_iface.h"
#include "nfp_blm_api.uc"
#include "wmemory.h"
#include "wire_interface.h"

; -----------------------------------------------------------------------------
; Definitions
; -----------------------------------------------------------------------------

#if !defined(PCIE_DMA_DIR_TO_HOST) && !defined(PCIE_DMA_DIR_FROM_HOST) && !defined(PCIE_DMA_RC_MODE)
 #error "One of {PCIE_DMA_DIR_TO_HOST, PCIE_DMA_DIR_FROM_HOST, PCIE_DMA_RC_MODE} must be defined!"
#endif

; -----------------------------------------------------------------------------
; REQUIRED Endpoint ME Options
; -----------------------------------------------------------------------------

;;; REMOVE ONCE PYTHON PROVIDES
#define MAX_PCIE_DMA_SIGNALS 5
;;; END REMOVE


#if defined(PCIE_DMA_DIR_TO_HOST) || defined(PCIE_DMA_DIR_FROM_HOST)

 #ifndef NUM_PCIE_PKT_MODE_MES
  #error "NUM_PCIE_PKT_MODE_MES must be defined!"
 #endif

 #if !defined(PCIEI4_ENDPOINT) && !defined(PCIEI5_ENDPOINT) \
  && !defined(PCIEI6_ENDPOINT) && !defined(PCIEI7_ENDPOINT)
  #error "A PCIe endpoint must be defined"
 #endif

 #ifndef PCIE_PKT_MODE_ME_INDEX
  #error "PCIE_PKT_MODE_ME_INDEX must be defined!"
 #endif

 #if PCIE_PKT_MODE_ME_INDEX < 0 || PCIE_PKT_MODE_ME_INDEX >= NUM_PCIE_PKT_MODE_MES
  #error "PCIE_PKT_MODE_ME_INDEX out of range!"
 #endif 

 #if !defined(CTM_OFFSET)
  #error "CTM_OFFSET must be defined!"
 #endif

 #ifndef PCIE_DMA_CREDITS
  #error "PCIE_DMA_CREDITS must be defined!"
 #endif

 #ifndef MAX_PCIE_DMA_SIGNALS
  #error "MAX_PCIE_DMA_SIGNALS must be defined!"
 #endif

 #if MAX_PCIE_DMA_SIGNALS != 5
  #error "MAX_PCIE_DMA_SIGNALS must be 5"
 #endif

 #if PCIE_DMA_CREDITS < MAX_PCIE_DMA_SIGNALS
  #error "PCIE_DMA_CREDITS must be atleast MAX_PCIE_DMA_SIGNALS"
 #endif

 #if defined(ENABLE_PCIE_REORDER) || defined(ENABLE_PCIE_REORDER_VM_QOS)
  #ifndef NBI_NUMBER
   #error "NBI_NUMBER [0,1] must be defined"
  #endif

  #if NBI_NUMBER != 0 && NBI_NUMBER != 1
   #error "Invalid NBI_NUMBER provided. Must be [0,1]"
  #endif
 #endif

#endif


; -----------------------------------------------------------------------------
; Freelist/RX descriptors
; (each descriptor is 4 LW)
; -----------------------------------------------------------------------------

#define FL_BUF_DESC_SZ          2
#define FL_PKT_RING_SZ          (2048 << 2)

#info "PCIe EP island num is "PCIE_EP_ISLAND_NUM

#if (PCIE_EP_ISLAND_NUM == 4)

 #if !defined(FL_PKT_RING4_ISLAND)

  ;;;#if   (__nfp_has_island(26))
  ;;; #define FL_PKT_RING4_ISLAND 26
  ;;;#elif (__nfp_has_island(25))
  ;;; #define FL_PKT_RING4_ISLAND 25
  ;;;#else
   #define FL_PKT_RING4_ISLAND 24
  ;;;#endif
 #else
  #if !(__nfp_has_island(FL_PKT_RING4_ISLAND))
   #error "FL_PKT_RING4_ISLAND "FL_PKT_RING4_ISLAND" not present"
  #endif
 #endif

 #define_eval _EMEM_INDEX (FL_PKT_RING4_ISLAND-24)

 #if !defined(FL_PKT_RING4_MEM_TYPE)
  #define FL_PKT_RING4_MEM_TYPE emem_cache_upper
 #endif

 #if !defined(FL_PKT_RING4_LOC)
  #define FL_PKT_RING4_LOC MU_LOCALITY_HIGH
 #endif

 ; ----------------------------------------------------------------------------
 ; allocate ring numbner 
 ; ----------------------------------------------------------------------------

 .alloc_resource FL_PKT_RING4_NUM emem/**/_EMEM_INDEX/**/_queues global 1

 ; ----------------------------------------------------------------------------
 ; allocate queue descriptor and base address for work queue
 ; ----------------------------------------------------------------------------

 .alloc_mem FL_PKT_RING4_QD_BASE \
    i/**/FL_PKT_RING4_ISLAND/**/./**/FL_PKT_RING4_MEM_TYPE global 16 16 reserved


 .alloc_mem FL_PKT_RING4_Q_BASE  \
    i/**/FL_PKT_RING4_ISLAND/**/./**/FL_PKT_RING4_MEM_TYPE global (4*FL_PKT_RING_SZ)  (4*FL_PKT_RING_SZ) reserved

 ; ----------------------------------------------------------------------------
 ; initialize the work queue
 ; ----------------------------------------------------------------------------

 .init_mu_ring FL_PKT_RING4_NUM FL_PKT_RING4_Q_BASE

 #undef _EMEM_INDEX
 
#elif (PCIE_EP_ISLAND_NUM == 5)

 #if !defined(FL_PKT_RING5_ISLAND)
  #if   (__nfp_has_island(26))
   #define FL_PKT_RING5_ISLAND 26
  #elif (__nfp_has_island(25))
   #define FL_PKT_RING5_ISLAND 25
  #else
   #define FL_PKT_RING5_ISLAND 24
  #endif
 #else
  #if !(__nfp_has_island(FL_PKT_RING5_ISLAND))
   #error "FL_PKT_RING5_ISLAND "FL_PKT_RING5_ISLAND" not present"
  #endif
 #endif

 #define_eval _EMEM_INDEX (FL_PKT_RING5_ISLAND-24)

 #if !defined(FL_PKT_RING5_MEM_TYPE)
  #define FL_PKT_RING5_MEM_TYPE emem_cache_upper
 #endif

 #if !defined(FL_PKT_RING5_LOC)
  #define FL_PKT_RING5_LOC MU_LOCALITY_HIGH
 #endif

 ; ----------------------------------------------------------------------------
 ; allocate ring numbner 
 ; ----------------------------------------------------------------------------

 .alloc_resource FL_PKT_RING5_NUM emem/**/_EMEM_INDEX/**/_queues global 1

 ; ----------------------------------------------------------------------------
 ; allocate queue descriptor and base address for work queue
 ; ----------------------------------------------------------------------------

 .alloc_mem FL_PKT_RING5_QD_BASE \
    i/**/FL_PKT_RING5_ISLAND/**/./**/FL_PKT_RING5_MEM_TYPE global 16 16 reserved

 .alloc_mem FL_PKT_RING5_Q_BASE  \
    i/**/FL_PKT_RING5_ISLAND/**/./**/FL_PKT_RING5_MEM_TYPE global (4*FL_PKT_RING_SZ) (4*FL_PKT_RING_SZ) reserved

 ; ----------------------------------------------------------------------------
 ; initialize the work queue
 ; ----------------------------------------------------------------------------

 .init_mu_ring FL_PKT_RING5_NUM FL_PKT_RING5_Q_BASE

 #undef _EMEM_INDEX
 
#elif (PCIE_EP_ISLAND_NUM == 6)

 #if !defined(FL_PKT_RING6_ISLAND)
  #if   (__nfp_has_island(26))
   #define FL_PKT_RING6_ISLAND 26
  #elif (__nfp_has_island(25))
   #define FL_PKT_RING6_ISLAND 25
  #else
   #define FL_PKT_RING6_ISLAND 24
  #endif
 #else
  #if !(__nfp_has_island(FL_PKT_RING6_ISLAND))
   #error "FL_PKT_RING6_ISLAND "FL_PKT_RING6_ISLAND" not present"
  #endif
 #endif

 #define_eval _EMEM_INDEX (FL_PKT_RING6_ISLAND-24)

 #if !defined(FL_PKT_RING6_MEM_TYPE)
  #define FL_PKT_RING6_MEM_TYPE emem_cache_upper
 #endif

 #if !defined(FL_PKT_RING6_LOC)
  #define FL_PKT_RING6_LOC MU_LOCALITY_HIGH
 #endif

 ; ----------------------------------------------------------------------------
 ; allocate ring numbner 
 ; ----------------------------------------------------------------------------

 .alloc_resource FL_PKT_RING6_NUM emem/**/_EMEM_INDEX/**/_queues global 1

 ; ----------------------------------------------------------------------------
 ; allocate queue descriptor and base address for work queue
 ; ----------------------------------------------------------------------------

 .alloc_mem FL_PKT_RING6_QD_BASE \
    i/**/FL_PKT_RING6_ISLAND/**/./**/FL_PKT_RING6_MEM_TYPE global 16 16 reserved

 .alloc_mem FL_PKT_RING6_Q_BASE  \
    i/**/FL_PKT_RING6_ISLAND/**/./**/FL_PKT_RING6_MEM_TYPE global (4*FL_PKT_RING_SZ) (4*FL_PKT_RING_SZ) reserved

 ; ----------------------------------------------------------------------------
 ; initialize the work queue
 ; ----------------------------------------------------------------------------
 
 .init_mu_ring FL_PKT_RING6_NUM FL_PKT_RING6_Q_BASE

 #undef _EMEM_INDEX

#elif (PCIE_EP_ISLAND_NUM == 7)

 #if !defined(FL_PKT_RING7_ISLAND)
  #if   (__nfp_has_island(26))
   #define FL_PKT_RING7_ISLAND 26
  #elif (__nfp_has_island(25))
   #define FL_PKT_RING7_ISLAND 25
  #else
   #define FL_PKT_RING7_ISLAND 24
  #endif
 #else
  #if !(__nfp_has_island(FL_PKT_RING7_ISLAND))
   #error "FL_PKT_RING5_ISLAND "FL_PKT_RING7_ISLAND" not present"
  #endif
 #endif

 #define_eval _EMEM_INDEX (FL_PKT_RING7_ISLAND-24)

 #if !defined(FL_PKT_RING7_MEM_TYPE)
  #define FL_PKT_RING7_MEM_TYPE emem_cache_upper
 #endif

 #if !defined(FL_PKT_RING7_LOC)
  #define FL_PKT_RING7_LOC MU_LOCALITY_HIGH
 #endif

 ; ----------------------------------------------------------------------------
 ; allocate ring numbner 
 ; ----------------------------------------------------------------------------

 .alloc_resource FL_PKT_RING7_NUM emem/**/_EMEM_INDEX/**/_queues global 1

 ; ----------------------------------------------------------------------------
 ; allocate queue descriptor and base address for work queue
 ; ----------------------------------------------------------------------------

 .alloc_mem FL_PKT_RING7_QD_BASE \
    i/**/FL_PKT_RING7_ISLAND/**/./**/FL_PKT_RING7_MEM_TYPE global 16 16 reserved

 .alloc_mem FL_PKT_RING7_Q_BASE  \
    i/**/FL_PKT_RING7_ISLAND/**/./**/FL_PKT_RING7_MEM_TYPE global (4*FL_PKT_RING_SZ) (4*FL_PKT_RING_SZ) reserved

 ; ----------------------------------------------------------------------------
 ; initialize the work queue
 ; ----------------------------------------------------------------------------

 .init_mu_ring FL_PKT_RING7_NUM FL_PKT_RING7_Q_BASE

 #undef _EMEM_INDEX
 
#endif

; -----------------------------------------------------------------------------
; RX/TX descriptors
; (each descriptor is 4 LW)
; -----------------------------------------------------------------------------

#define TX_BUF_DESC_SZ		4
#define TX_PKT_RING_SZ          (2048 << 2)

#if   (PCIE_EP_ISLAND_NUM == 4)

 #if !defined(TX_PKT_RING4_ISLAND)
  #define TX_PKT_RING4_ISLAND 24
 #else
  #if !(_nfp_has_island(TX_PKT_RING4_ISLAND))
   #error "TX_PKT_RING4_ISLAND "TX_PKT_RING4_ISLAND" not present"
  #endif
 #endif

 #define_eval _EMEM_INDEX (TX_PKT_RING4_ISLAND-24)

 #if !defined(TX_PKT_RING4_MEM_TYPE)
  #define TX_PKT_RING4_MEM_TYPE emem_cache_upper
 #endif

 #if !defined(TX_PKT_RING4_LOC)
  #define TX_PKT_RING4_LOC MU_LOCALITY_HIGH
 #endif

 ; ----------------------------------------------------------------------------
 ; allocate ring numbner 
 ; ----------------------------------------------------------------------------

 .alloc_resource TX_PKT_RING4_NUM emem/**/_EMEM_INDEX/**/_queues global 1

 ; ----------------------------------------------------------------------------
 ; allocate queue descriptor and base address for work queue
 ; ----------------------------------------------------------------------------

 .alloc_mem TX_PKT_RING4_QD_BASE \
    i/**/TX_PKT_RING4_ISLAND/**/./**/TX_PKT_RING4_MEM_TYPE global 16 16 reserved

 .alloc_mem TX_PKT_RING4_Q_BASE  \
    i/**/TX_PKT_RING4_ISLAND/**/./**/TX_PKT_RING4_MEM_TYPE global (4*TX_PKT_RING_SZ) (4*TX_PKT_RING_SZ) reserved

 ; ----------------------------------------------------------------------------
 ; initialize the work queue
 ; ----------------------------------------------------------------------------

 .init_mu_ring TX_PKT_RING4_NUM TX_PKT_RING4_Q_BASE

 #undef _EMEM_INDEX

#elif (PCIE_EP_ISLAND_NUM == 5)

 #if !defined(TX_PKT_RING5_ISLAND)
  #define TX_PKT_RING5_ISLAND 24
 #else
  #if !(_nfp_has_island(TX_PKT_RING5_ISLAND))
   #error "TX_PKT_RING5_ISLAND "TX_PKT_RING5_ISLAND" not present"
  #endif
 #endif

 #define_eval _EMEM_INDEX (TX_PKT_RING5_ISLAND-24)

 #if !defined(TX_PKT_RING5_MEM_TYPE)
  #define TX_PKT_RING5_MEM_TYPE emem_cache_upper
 #endif

 #if !defined(TX_PKT_RING5_LOC)
  #define TX_PKT_RING5_LOC MU_LOCALITY_HIGH
 #endif

 ; ----------------------------------------------------------------------------
 ; allocate ring numbner 
 ; ----------------------------------------------------------------------------

 .alloc_resource TX_PKT_RING5_NUM emem/**/_EMEM_INDEX/**/_queues global 1

 ; ----------------------------------------------------------------------------
 ; allocate queue descriptor and base address for work queue
 ; ----------------------------------------------------------------------------

 .alloc_mem TX_PKT_RING5_QD_BASE \
    i/**/TX_PKT_RING5_ISLAND/**/./**/TX_PKT_RING5_MEM_TYPE global 16 16 reserved

 .alloc_mem TX_PKT_RING5_Q_BASE  \
    i/**/TX_PKT_RING5_ISLAND/**/./**/TX_PKT_RING5_MEM_TYPE global (4*TX_PKT_RING_SZ) (4*TX_PKT_RING_SZ) reserved

 ; ----------------------------------------------------------------------------
 ; initialize the work queue
 ; ----------------------------------------------------------------------------

 .init_mu_ring TX_PKT_RING5_NUM TX_PKT_RING5_Q_BASE

 #undef _EMEM_INDEX

#elif (PCIE_EP_ISLAND_NUM == 6)

 #if !defined(TX_PKT_RING6_ISLAND)
  #define TX_PKT_RING6_ISLAND 24
 #else
  #if !(_nfp_has_island(TX_PKT_RING6_ISLAND))
   #error "TX_PKT_RING4_ISLAND "TX_PKT_RING6_ISLAND" not present"
  #endif
 #endif

 #define_eval _EMEM_INDEX (TX_PKT_RING6_ISLAND-24)

 #if !defined(TX_PKT_RING6_MEM_TYPE)
  #define TX_PKT_RING6_MEM_TYPE emem_cache_upper
 #endif

 #if !defined(TX_PKT_RING6_LOC)
  #define TX_PKT_RING6_LOC MU_LOCALITY_HIGH
 #endif

 ; ----------------------------------------------------------------------------
 ; allocate ring numbner 
 ; ----------------------------------------------------------------------------

 .alloc_resource TX_PKT_RING6_NUM emem/**/_EMEM_INDEX/**/_queues global 1

 ; ----------------------------------------------------------------------------
 ; allocate queue descriptor and base address for work queue
 ; ----------------------------------------------------------------------------

 .alloc_mem TX_PKT_RING6_QD_BASE \
    i/**/TX_PKT_RING6_ISLAND/**/./**/TX_PKT_RING6_MEM_TYPE global 16 16 reserved

 .alloc_mem TX_PKT_RING6_Q_BASE  \
    i/**/TX_PKT_RING6_ISLAND/**/./**/TX_PKT_RING6_MEM_TYPE global (4*TX_PKT_RING_SZ) (4*TX_PKT_RING_SZ) reserved

 ; ----------------------------------------------------------------------------
 ; initialize the work queue
 ; ----------------------------------------------------------------------------

 .init_mu_ring TX_PKT_RING6_NUM TX_PKT_RING6_Q_BASE

 #undef _EMEM_INDEX

#elif (PCIE_EP_ISLAND_NUM == 7)

 #if !defined(TX_PKT_RING7_ISLAND)
  #define TX_PKT_RING7_ISLAND 24
 #else
  #if !(_nfp_has_island(TX_PKT_RING7_ISLAND))
   #error "TX_PKT_RING7_ISLAND "TX_PKT_RING7_ISLAND" not present"
  #endif
 #endif

 #define_eval _EMEM_INDEX (TX_PKT_RING7_ISLAND-24)

 #if !defined(TX_PKT_RING7_MEM_TYPE)
  #define TX_PKT_RING7_MEM_TYPE emem_cache_upper
 #endif

 #if !defined(TX_PKT_RING7_LOC)
  #define TX_PKT_RING7_LOC MU_LOCALITY_HIGH
 #endif

 ; ----------------------------------------------------------------------------
 ; allocate ring numbner 
 ; ----------------------------------------------------------------------------

 .alloc_resource TX_PKT_RING7_NUM emem/**/_EMEM_INDEX/**/_queues global 1

 ; ----------------------------------------------------------------------------
 ; allocate queue descriptor and base address for work queue
 ; ----------------------------------------------------------------------------

 .alloc_mem TX_PKT_RING7_QD_BASE \
    i/**/TX_PKT_RING7_ISLAND/**/./**/TX_PKT_RING7_MEM_TYPE global 16 16 reserved

 .alloc_mem TX_PKT_RING7_Q_BASE  \
    i/**/TX_PKT_RING7_ISLAND/**/./**/TX_PKT_RING7_MEM_TYPE global (4*TX_PKT_RING_SZ) (4*TX_PKT_RING_SZ) reserved

 ; ----------------------------------------------------------------------------
 ; initialize the work queue
 ; ----------------------------------------------------------------------------

 .init_mu_ring TX_PKT_RING7_NUM TX_PKT_RING7_Q_BASE

 #undef _EMEM_INDEX

#endif

; -----------------------------------------------------------------------------
; Allocate CLS event filter indices
; -----------------------------------------------------------------------------

#if defined(PCIE_DMA_DIR_FROM_HOST)

 .declare_resource EVENT_FILTER_INDICES island 12 +2
 .alloc_resource EVENT_FILTER_INDEX EVENT_FILTER_INDICES me 1

#endif

; -----------------------------------------------------------------------------
; context number issuer
; -----------------------------------------------------------------------------

#define CTX_NUMBER_RING_SZ 512

#if !defined(CTX_NUMBER_RING_ISLAND)
  #define CTX_NUMBER_RING_ISLAND 24
#else
 #if !(__nfp_has_island(CTX_NUMBER_RING_ISLAND))
  #error "CTX_NUMBER_RING_ISLAND "CTX_NUMBER_RING_ISLAND" not present"
 #endif
#endif

#if !defined(CTX_NUMBER_RING_MEM_TYPE)
 #define CTX_NUMBER_RING_MEM_TYPE emem_cache_upper
#endif

#if !defined(CTX_NUMBER_RING_LOC)
 #define CTX_NUMBER_RING_LOC MU_LOCALITY_HIGH
#endif

; ----------------------------------------------------------------------------
; allocate ring numbner 
; ----------------------------------------------------------------------------
#define_eval _EMEM_INDEX (CTX_NUMBER_RING_ISLAND-24)
.alloc_resource CTX_NUMBER_RING/**/PCIE_EP_ISLAND_NUM/**/_NUM emem/**/_EMEM_INDEX/**/_queues global 1
#undef _EMEM_INDEX

; ----------------------------------------------------------------------------
; allocate queue descriptor and base address for work queue
; ----------------------------------------------------------------------------

.alloc_mem CTX_NUMBER_RING/**/PCIE_EP_ISLAND_NUM/**/_QD_BASE \
   i/**/CTX_NUMBER_RING_ISLAND/**/./**/CTX_NUMBER_RING_MEM_TYPE global 16 16 reserved

.alloc_mem CTX_NUMBER_RING/**/PCIE_EP_ISLAND_NUM/**/_Q_BASE  \
   i/**/CTX_NUMBER_RING_ISLAND/**/./**/CTX_NUMBER_RING_MEM_TYPE global (4*CTX_NUMBER_RING_SZ)  (4*CTX_NUMBER_RING_SZ) reserved

; ----------------------------------------------------------------------------
; initialize the work queue
; ----------------------------------------------------------------------------

.init_mu_ring CTX_NUMBER_RING/**/PCIE_EP_ISLAND_NUM/**/_NUM CTX_NUMBER_RING/**/PCIE_EP_ISLAND_NUM/**/_Q_BASE

; -----------------------------------------------------------------------------
; "host-side" buffers 
; -----------------------------------------------------------------------------

#if defined(PCIE_DMA_NFP_ROOT_COMPLEX)

 #define USE_EMEM_HOST_BUFFERS
 #if defined(USE_EMEM_HOST_BUFFERS)

  #info "Using EMEM host-side buffers"

  #define HOST_BUFFER_ALIGN	 (2 * 1024)
  #define HOST_BUFFER_LEN	(10 * 1024)
  #define HOST_BUFFER_CNT	512
  #define HOST_BUFFER_SZ         (HOST_BUFFER_LEN * HOST_BUFFER_CNT)
  #define HOST_BUFFER_MEM_TYPE   emem

 .alloc_mem HOST_BUFFER_BASE	i26.HOST_BUFFER_MEM_TYPE global HOST_BUFFER_SZ HOST_BUFFER_ALIGN reserved

 #else

  #info "Using CTM host-side buffers"

  #define HOST_BUFFER_LEN	(2 * 1024)
  #define HOST_BUFFER_CNT	64
  #define HOST_BUFFER_SZ         (HOST_BUFFER_LEN * HOST_BUFFER_CNT)
  #define HOST_BUFFER_MEM_TYPE   ctm

 .alloc_mem HOST_BUFFER_BASE	i5.ctm global HOST_BUFFER_SZ HOST_BUFFER_LEN reserved

 #endif

#else ;; defined(PCIE_DMA_IA_ROOT_COMPLEX)

  #define HOST_BUFFER_LEN	(10 * 1024)
  #define HOST_BUFFER_CNT	512

  #define FOUR_GBYTE		0x0100000000	;; 4 GB offset
                                                ;; must be reserved by kernel memmap

  #define HOST_BUFFER_OFFSET 	((PCIE_EP_ISLAND_NUM - 4) * HOST_BUFFER_LEN * HOST_BUFFER_CNT)

  #define HOST_BUFFER_BASE	(FOUR_GBYTE + HOST_BUFFER_OFFSET)

#endif

;------------------------------------------------------------------------------
; PCIe Definitions
;------------------------------------------------------------------------------

; PCIe XPB Target ID and Slave Index Definitions
#define PCIE_XPB_TARGET_ID_COMP_CFG     (0x10)
#define PCIE_XPB_TARGET_ID_PF_REGS	(0x20)
#define PCIE_XPB_TARGET_ID_RC_REGS	(0x21)
#define PCIE_XPB_TARGET_ID_LM_REGS	(0x22)

#define PCIE_XPB_SLAVE_INDEX_COMP_CFG   (0)
#define PCIE_XPB_SLAVE_INDEX_PF_REGS	(1)
#define PCIE_XPB_SLAVE_INDEX_RC_REGS	(1)
#define PCIE_XPB_SLAVE_INDEX_LM_REGS	(1)

; CPP Target IDs
#define CPP_TARGET_ID_MEM         	(7)

; Token corresponding to 64-bit Read/Writes	   
#define TOKEN_READ_WRITE_64		(0)

;------------------------------------------------------------------------------
; PCIe configuration
;------------------------------------------------------------------------------

/*
 * DMA descriptor queues used in each direction
 */
#if !defined(DMA_DESC_QUEUE_INDEX)
 #if defined(PCIE_DMA_DIR_TO_HOST) 
  #define DMA_DESC_QUEUE_INDEX (NFP_PCIE_DMA_NDX_TPCI_LOW)
 #elif defined(PCIE_DMA_DIR_FROM_HOST)
  #define DMA_DESC_QUEUE_INDEX (NFP_PCIE_DMA_NDX_FPCI_LOW)
 #endif
#endif

#if !defined(PCIE_DMA_RC_BASE_ADDRESS)
 #if defined(PCIE_DMA_NFP_ROOT_COMPLEX)
  #define PCIE_DMA_RC_BASE_ADDRESS 0x0040000000
 #elif defined(PCIE_DMA_IA_ROOT_COMPLEX)
  #define PCIE_DMA_RC_BASE_ADDRESS 0x0000000000
 #else
  #error "Either PCIE_DMA_NFP_ROOT_COMPLEX or PCIE_DMA_IA_ROOT_COMPLEX must be defined"
 #endif
#endif

#if !defined(PCIE_DMA_RC_APERTURE_BITS)
 #define PCIE_DMA_RC_APERTURE_BITS 27
#endif

#ifndef PCIE_DMA_MPS
 #define PCIE_DMA_MPS  256
#endif

#ifndef PCIE_DMA_MRRS
 #define PCIE_DMA_MRRS 512
#endif

#ifndef PCIE_DMA_RCB
 #define PCIE_DMA_RCB  128
#endif

#ifndef PCIE_DMA_DOC
 #define PCIE_DMA_DOC 1		; disable ordering check
#endif

#ifndef PCIE_DMA_DRXNPSP
 #define PCIE_DMA_DRXNPSP 1 	; disable RX NP Starvation Prevention
#endif

#ifndef PCIE_DWRR_CREDITS
 #define PCIE_DWRR_CREDITS 2
#endif

/*
 * PMHeader CSR Indices
 */
#define PMH_NDX_INVALID_IPV4_EXCEPTION_PKT  0
#define PMH_NDX_INVALID_IPV4_PKT            1
#define PMH_NDX_INVALID_ARP_PKT             2
#define PMH_NDX_INVALID_NON_IPV4_PKT        3
#define PMH_NDX_INVALID_ERROR_PKT           4

#define PMH_NDX_VALID_IPV4_EXCEPTION_PKT    8
#define PMH_NDX_VALID_IPV4_PKT		    9
#define PMH_NDX_VALID_ARP_PKT              10
#define PMH_NDX_VALID_NON_IPV4_PKT         11
#define PMH_NDX_VALID_ERROR_PKT            12

/*
 * DMADescConfig CSR indices
 */
#define DDC_TO_PCIE_NDX_NORMAL    6
#define DDC_TO_PCIE_NDX_FREE_CTM  7
#define DDC_TO_PCIE_NDX_SIG_ONLY 14


; -----------------------------------------------------------------------------
; Event Filter
; -----------------------------------------------------------------------------

; Event filter parameters
#define EVENT_FILTER_TIMEOUT     6
#define EVENT_FILTER_OVERRIDE   16

/*
 * Macros for formatting event
 */
#define NFP_EVENT_TYPE(type)		((type) & 0x0f)
#define NFP_EVENT_SOURCE(src) 		(((src) & 0x0fff) << 4)
#define NFP_EVENT_PROVIDER(prv)		(((prv) & 0x0ff) << 16)

#define NFP_EVENT(prv, src, type)	(NFP_EVENT_PROVIDER(prv) | \
                                         NFP_EVENT_SOURCE(src)   | \
                                         NFP_EVENT_TYPE(type))

/*
 * Standard event bus types
 */
#define NFP_EVENT_TYPE_FIFO_NOT_EMPTY		0
#define NFP_EVENT_TYPE_FIFO_NOT_FULL		1
#define NFP_EVENT_TYPE_DMA			2
#define NFP_EVENT_TYPE_PROCESS			3
#define NFP_EVENT_TYPE_STATUS			4
#define NFP_EVENT_TYPE_FIFO_UNDERFLOW		8
#define NFP_EVENT_TYPE_FIFO_OVERFLOW		9
#define NFP_EVENT_TYPE_ECC_SINGLE_CORRECTION   10
#define NFP_EVENT_TYPE_ECC_MULTI_ERROR	       11
#define NFP_EVENT_TYPE_ECC_SINGLE_ERROR	       12

;------------------------------------------------------------------------------
; Misc definitions
;------------------------------------------------------------------------------

#define PCIE_INIT_START     0x7731
#define PCIE_INIT_COMPLETE  0x7732

#define CPP_TARGET_ID_MEM       (7)
#define TOKEN_READ_WRITE_64	(0)

;------------------------------------------------------------------------------
; Global variables 
;------------------------------------------------------------------------------

.reg volatile $event_data
.sig volatile event_sig

.sig volatile stop_signal
.addr stop_signal 15

.sig volatile thread_sgnl
.addr thread_sgnl 14

.reg fl_addr_39_32
.reg tx_addr_39_32
.reg ctx_addr_39_32

.reg @ctm_offset

.reg four_k
.reg minus_one

.reg pcie_ep_addr_39_32
.reg pcie_desc_addr_31_00

.reg cl_num
.reg me_num

.reg ctx ;;; REMOVE - DEBUG ONLY

.reg @dma_avail

.reg @dma_count
.reg @pkt_count


;------------------------------------------------------------------------------
; Source
;------------------------------------------------------------------------------

/**
 * Blocks while waiting for signal to assert
 * @sgnl the signal to wait upon
 */
#macro pcie_pkt_mode_signal_spin(sgnl)
.begin
spin_loop#:
    br_!signal[sgnl, spin_loop#]
.end
#endm

/**
 * Synchronizes threads
 */
#macro pcie_pkt_mode_thread_synch()
.begin
    threads_reorder_once(thread_sgnl)
.end
#endm

/**
 * Populates register with upper 8 address bits (of 40-bit address)
 * @ADDRESS    40-bit (constant) address
 * @addr_39_32 register containing the upper 8 address bits of
 *             input address in bits 31_24 upon return
 */
#macro pcie_pkt_mode_get_addr_39_32(ADDRESS, addr_39_32)
.begin
    alu[addr_39_32, --, B, (ADDRESS >> 32), <<24]
.end
#endm

/**
 * Adds freelist descriptor as work item to associated work queue
 * @$desc the freelist descriptor array
 * @sgnl  signal to indicate operation completed
 */
#macro pcie_pkt_mode_freelist_put($desc, sgnl)
.begin

    mem[qadd_work, $desc[0], fl_addr_39_32, <<8, FL_PKT_RING/**/PCIE_EP_ISLAND_NUM/**/_NUM, FL_BUF_DESC_SZ], sig_done[sgnl]

.end
#endm

/**
 * Adds freelist descriptor as work item to associated work queue
 * @$desc the freelist descriptor array
 * @sgnl  signal to indicate operation completed
 */
#macro pcie_pkt_mode_freelist_get($desc, sgnl)
.begin
   
    mem[qadd_thread, $desc[0], fl_addr_39_32, <<8, FL_PKT_RING/**/PCIE_EP_ISLAND_NUM/**/_NUM, FL_BUF_DESC_SZ], sig_done[sgnl]

.end
#endm

/**
 * Adds rx/tx descriptor as work item to associated work queue
 * @$desc the rx/tx descriptor array
 * @sgnl  signal to indicate operation completed
 */
#macro pcie_pkt_mode_rxtxq_put($desc, sgnl)
.begin

    mem[qadd_work, $desc[0], tx_addr_39_32, <<8, TX_PKT_RING/**/PCIE_EP_ISLAND_NUM/**/_NUM, TX_BUF_DESC_SZ], sig_done[sgnl]

.end
#endm

/**
 * Adds freelist descriptor as work item to associated work queue
 * @$desc the rx/tx descriptor array
 * @sgnl  signal to indicate operation completed
 */
#macro pcie_pkt_mode_rxtxq_get($desc, sgnl)
.begin

    mem[qadd_thread, $desc[0], tx_addr_39_32, <<8, TX_PKT_RING/**/PCIE_EP_ISLAND_NUM/**/_NUM, TX_BUF_DESC_SZ], sig_done[sgnl]

.end
#endm

/**
 * Fills specified memory with zeroes. Assumes all 8 contexts are available for
 * the operation
 *
 * @param BUFFER_BASE   40-bit constant address providing base address
 *                      of host-side buffers
 * @param BUFFER_LENGTH the total length of host memory available for providing
 *                      host-side buffers
 */
#macro pcie_pkt_mode_clear_host_buffers(BUFFER_BASE, BUFFER_LENGTH)
.begin

    .reg tmp
    .reg ctx
    .reg len
    .reg a39_32
    .reg a31_00

    .reg write $x[16]
    .xfer_order $x
    .sig sgnl

    // clear the transfer register
    immed[ $x[0], 0x00]
    immed[ $x[1], 0x00]
    immed[ $x[2], 0x00]
    immed[ $x[3], 0x00]
    immed[ $x[4], 0x00]
    immed[ $x[5], 0x00]
    immed[ $x[6], 0x00]
    immed[ $x[7], 0x00]
    immed[ $x[8], 0x00]
    immed[ $x[9], 0x00]
    immed[$x[10], 0x00]
    immed[$x[11], 0x00]
    immed[$x[12], 0x00]
    immed[$x[13], 0x00]
    immed[$x[14], 0x00]
    immed[$x[15], 0x00]

    // get buffer address as 32-bit words
    alu[a39_32, --, B, (0xFF & (BUFFER_BASE>>32)), <<24]
    immed32[a31_00, (0xFFFFFFFF & BUFFER_BASE)]

    // determine length in bytes that each context clears
    immed32[len, (BUFFER_LENGTH >> 3)]

    // determine region to clear based on context number
    local_csr_rd[ACTIVE_CTX_STS]
    immed[tmp, 0]

    alu[ctx, 0x07, AND, tmp]
    alu[tmp, --, B, ctx, <<(log2(BUFFER_LENGTH)-3)]

    alu[a31_00, a31_00, +, tmp]

clear_buffer_top#:

    mem[write, $x[0], a39_32, <<8, a31_00, 8], ctx_swap[sgnl]
    alu[a31_00, a31_00, +, 64]

    alu[len, len, -, 64]
    bgt[clear_buffer_top#]

.end
#endm

#ifdef RANDOMIZE_FROM_SEGMENTS

#ifndef NUMBER_RANDOM_SEGMENTS
 #error "NUMBER_RANDOM_SEGMENTS must be defined"
#endif

#if NUMBER_RANDOM_SEGMENTS < 2 || NUMBER_RANDOM_SEGMENTS > 3
 #error "NUMBER_RANDOM_SEGMENTS must be 2 or 3"
#endif

;------------------------------------------------------------------------------
; pseudo-random number gernerator
;------------------------------------------------------------------------------

.reg r_prand

/*
 * generate the next random data word
 */
#macro prand_next_rand()
.begin

    local_csr_rd[PSEUDO_RANDOM_NUMBER]
    immed[r_prand, 0]

.end
#endm

/*
 * generate the first random data word
 */
#macro prand_seed_randomizer()
.begin

    ; each ME uses active context status of context0 as randomizer seed
    .if (ctx() == 0)

        local_csr_rd[ACTIVE_CTX_STS]
        immed[r_prand, 0]

        local_csr_wr[PSEUDO_RANDOM_NUMBER, r_prand]
        nop
        nop
        nop

        prand_next_rand()
        prand_next_rand()
        prand_next_rand()

    .endif

.end
#endm

;------------------------------------------------------------------------------
; Support for testing random FromPcie stage lengths
;------------------------------------------------------------------------------

#define CLS_ENTRIES_PER_CTX  (16)
#define CLS_ENTRY_SIZE_LW    (4)
#define CLS_ENTRY_SIZE_B     (4 * CLS_ENTRY_SIZE_LW)
#define CLS_NUMBER_CTX       (8)
#define CLS_BYTES_PER_CTX    (CLS_ENTRIES_PER_CTX * CLS_ENTRY_SIZE_B)
#define CLS_STATS_LENGTH     (CLS_BYTES_PER_CTX * CLS_NUMBER_CTX)

#if CLS_BYTES_PER_CTX & (CLS_BYTES_PER_CTX-1)
 #error "CLS_BYTES_PER_CTX must be power of 2"
#else
 #define CLS_STATS_CTX_SHIFT  LOG2(CLS_BYTES_PER_CTX)
#endif

.alloc_mem CLS_STAGE_STATS cls me CLS_STATS_LENGTH 16 reserved

.reg cls_slen_base
.reg cls_slen_offset
.reg cls_slen_entry
.reg cls_slen_stage
.reg @cls_slen_pktnum

#macro log_stage_init()
.begin

    .reg ctx

    local_csr_rd[ACTIVE_CTX_STS]
    immed[ctx, 0]
    alu[ctx, ctx, AND, 0x7]

    move(cls_slen_base, CLS_STAGE_STATS)
    alu[cls_slen_offset, --, B, ctx, <<(CLS_STATS_CTX_SHIFT)]

    immed[cls_slen_entry, 0]

    .if (ctx() == 0)
        immed[@cls_slen_pktnum, 0]
    .endif

    ; initialize and seed prn generator
    prand_seed_randomizer()

.end
#endm

#macro log_stage_header(ctxnum)
.begin

    .reg tmp
    .reg $xfer
    .sig sgnl

    alu[tmp, --, B, @cls_slen_pktnum]
    alu[$xfer, tmp, OR, ctxnum, <<16]

    cls[write, $xfer, cls_slen_base, cls_slen_offset, 1], sig_done[sgnl]

    alu[cls_slen_offset, cls_slen_offset, +, 4]
    alu[@cls_slen_pktnum, @cls_slen_pktnum, +, 1]

    ctx_arb[sgnl]

.end
#endm

#macro log_stage_length(stglen)
.begin

    .reg $xfer
    .sig sgnl

    #if isnum(stglen)
     immed[$xfer, stglen]
    #else
     alu[$xfer, stglen, +, 0]
    #endif

    cls[write, $xfer, cls_slen_base, cls_slen_offset, 1], sig_done[sgnl]
    ctx_arb[sgnl]

    alu[cls_slen_offset, cls_slen_offset, +, 4]

.end
#endm

#macro log_stage_increment()
.begin

    .reg tmp

    alu[cls_slen_entry, cls_slen_entry, +, 1]
    alu[--, cls_slen_entry, -, CLS_ENTRIES_PER_CTX]
    blt[entry_updated#]

    immed[tmp, CLS_BYTES_PER_CTX]
    immed[cls_slen_entry, 0]
    alu[cls_slen_offset, cls_slen_offset, -, tmp]

entry_updated#:

.end
#endm

/*
 * Returns a random stage length between 32B and packet_length/2 + 31B
 * when packet length is power of 2. When packet length is not a power 
 * of 2, the returned value is between 32B and 31B + the greatest power
 * of 2 that is less than packet length/2.
 * @param pktlen length of packet (input)
 * @param stglen random stage length (output)
 */
#macro get_stage_length(pktlen, stglen)
.begin

    alu[stglen, --, B, pktlen, >>1]

    fls[stglen, stglen], one_hot
    alu[stglen, stglen, -, 1]

    ; get random number and mask it
    prand_next_rand()
    alu[stglen, stglen, AND, r_prand]
    alu[stglen, stglen, +, 32]

.end
#endm

#endif

;------------------------------------------------------------------------------
; PCIe configuration
;------------------------------------------------------------------------------

/**
 * Configures EP from RC
 * @param rc_island identifies the PCIe Root Complex island number
 */
#macro enable_pcie_ep(rc_island)
.begin
    
    .reg bar
    .reg addr_msb
    .reg offset
    .reg $xfer
    .sig sgnl

    // get offset to reach the PF command status CSR
    immed[offset, NFP_PCIEX_PF_i_pcie_base_I_COMMAND_STATUS]

    // configure CppToPcieBAR0 for config-space accesses
    move(bar, 0x80000000)
    configure_cpp_to_pcie_bar(rc_island, 0, bar)

    // enable Bus Master Enable, Mem-Space Enable and IO-Space Enable
    // and w1c upper error bits
    immed[tmp, 0x07]
    alu[$xfer, tmp, OR, 0x1f, <<27]

    // enable Bus Master Enable, Mem-Space Enable and IO-Space Enable
    alu[addr_msb, --, B, rc_island, <<30]
    pcie[write, $xfer, addr_msb, <<8, offset, 1], sig_done[sgnl]
    ctx_arb[sgnl]

.end
#endm

/**
 * Clears DPE, SSE, RMA, RTA and STA error fields in RC command status CSR
 *
 * @param rc_island the PCIe Root Complex island to be configured
 */
#macro clear_rc_error_fields(rc_island)
.begin

    .reg base
    .reg offset
    .reg $xfer
    .reg tmp
    .sig sgnl

    // get offset to reach RC cmd status CSR
    immed[offset, NFP_PCIEX_RC_i_rc_pcie_base_I_COMMAND_STATUS]

    // Set base addressing for accessing the RC registers via XPB
    immed[base, 0x0]
    immed_w1[base, PCIE_XPB_TARGET_ID_RC_REGS]
    alu_shf[base, base, OR, PCIE_XPB_SLAVE_INDEX_RC_REGS, <<22]
    alu_shf[base, base, OR, rc_island, <<24]

    // retrieve contents
    ct[xpb_read, $xfer, base, offset, 1], sig_done[sgnl]
    ctx_arb[sgnl]
    alu[tmp, --, B, $xfer]

    // error fields are cleared by writing '0' for RC
    alu[$xfer, tmp, AND~, 0xff, <<24]

    // write back
    ct[xpb_write, $xfer, base, offset, 1], sig_done[sgnl]
    ctx_arb[sgnl]

    // wait for XPB write to complete before returning
    ct[xpb_read, $xfer, base, offset, 1], sig_done[sgnl]
    ctx_arb[sgnl]

.end
#endm

/**
 * Disables the PCIe Target Expansion BAR selection of the CPP
 * Master Command Bus preference
 * @param rc_island the PCIe Root Complex island to be configured
 */
#macro disable_rc_cpp_bus_preference(rc_island)
.begin

    .reg base
    .reg offset
    .reg $xfer
    .reg tmp
    .sig sgnl

    // get offset to reach RC cmd status CSR
    immed[offset, NFP_PCIEX_COMPCFG_CFG0]

    // Set base addressing for accessing the RC registers via XPB
    immed[base, 0x0]
    immed_w1[base, PCIE_XPB_TARGET_ID_COMP_CFG]
    alu_shf[base, base, OR, PCIE_XPB_SLAVE_INDEX_COMP_CFG, <<22]
    alu_shf[base, base, OR, rc_island, <<24]

    // retrieve contents
    ct[xpb_read, $xfer, base, offset, 1], sig_done[sgnl]
    ctx_arb[sgnl]
    alu[tmp, --, B, $xfer]

    // error fields are cleared by writing '0' for RC
    alu[$xfer, tmp, OR, 1, <<25]

    // write back
    ct[xpb_write, $xfer, base, offset, 1], sig_done[sgnl]
    ctx_arb[sgnl]

    // wait for XPB write to complete before returning
    ct[xpb_read, $xfer, base, offset, 1], sig_done[sgnl]
    ctx_arb[sgnl]

.end
#endm

/**
 * Configures the debug_mux_control_reg via xpb. Allows for enabling/disabling
 * Disable Order Checks (DOC) and Disable RX NP Starvation Prevention (DRXNPSP)
 * @pciei   PCIe island number
 * @doc	    if set, disables ordering checks 
 * @drxnpsp if set, disables RX NP Starvation Prevention
 */
#macro configure_debug_mux_control_csr(pciei, doc, drxnpsp)
.begin

    .reg addr
    .reg $xfer
    .sig sgnl

    move(addr, NFP_XPB_ISLAND(pciei) + NFP_PCIEX_LM + NFP_PCIEX_LM_i_regf_lm_pcie_base_I_DEBUG_MUX_CONTROL_REG)
    ct[xpb_read, $xfer, addr, 0, 1], sig_done[sgnl]
    ctx_arb[sgnl]

    alu[tmp, $xfer, AND~, 1, <<30]	; mask away DOC
    alu[tmp, tmp,   AND~, 1, <<10]	; mask away DRXNPSP
    
    alu[tmp,   tmp, OR, doc,     <<30] 	; disable DOC
    alu[$xfer, tmp, OR, drxnpsp, <<10]	; disable DRXNPSP

    ct[xpb_write, $xfer, addr, 0, 1], sig_done[sgnl]
    ctx_arb[sgnl]

.end
#endm


/**
 * Configures the DWRR credits for the Shared Bus Master Arbitration
 * @PCIEI   PCIe island to be configured
 * @CREDITS DWRR credits to apply
 */
#macro configure_dwrr(PCIEI, CREDITS)
.begin

    .reg addr
    .reg value
    .reg $xfer
    .sig sgnl

    #define _ECCGPR 0x3c
    #define _DWRR_CFG_LOAD 0xc0000000
    #define _DWRR_ENABLE   0x0

    move(addr, NFP_XPB_ISLAND(PCIEI) + NFP_PCIEX_SRAM_ECC + _ECCGPR)

    move(value, (_DWRR_CFG_LOAD | (0 << 28) | (CREDITS << 24)))	; load DWRR port A credits
    alu[$xfer, --, B, value]
    ct[xpb_write, $xfer, addr, 0, 1], sig_done[sgnl]
    ctx_arb[sgnl]

    move(value, (_DWRR_CFG_LOAD | (1 << 28) | (CREDITS << 24)))	; load DWRR port B credits
    alu[$xfer, --, B, value]
    ct[xpb_write, $xfer, addr, 0, 1], sig_done[sgnl]
    ctx_arb[sgnl]

    move(value, (_DWRR_CFG_LOAD | (2 << 28) | (CREDITS << 24)))	; load DWRR port C credits
    alu[$xfer, --, B, value]
    ct[xpb_write, $xfer, addr, 0, 1], sig_done[sgnl]
    ctx_arb[sgnl]

    move(value, (_DWRR_CFG_LOAD | (3 << 28) | (CREDITS << 24)))	; load DWRR port D credits
    alu[$xfer, --, B, value]
    ct[xpb_write, $xfer, addr, 0, 1], sig_done[sgnl]
    ctx_arb[sgnl]

    move(value, _DWRR_ENABLE)					; enable DWRR logic
    alu[$xfer, --, B, value]
    ct[xpb_write, $xfer, addr, 0, 1], sig_done[sgnl]
    ctx_arb[sgnl]

    #undef _ECCGPR
    #undef _DWRR_CFG_LOAD
    #undef _DWRR_ENABLE

.end
#endm


/**
 * Configures a CppToPcie BAR with the specified value
 *
 * @param island  Identifies the PCIe island hosting the BAR to be 
 *                configured
 * @param bar_ndx Identifies the CppToPcie BAR {0, 1, ..., 7
 * @param value   The new value to be written to the specified BAR
 */
#macro configure_cpp_to_pcie_bar(island, bar_ndx, value)
.begin

    .reg addr_msb
    .reg offset
    .reg $xfer
    .sig sgnl

    // set address MSB
    alu_shf[addr_msb, --, B, island, <<30]

    // BAR configuration is performed by writing to PCIe internal target
    // at offset 0x3 0000
    immed[offset, 0x0]
    immed_w1[offset, 0x3]

    // CppToPcieBARs start at offset 0x600
    alu_shf[offset, offset, OR, 0x6, <<8]

    // determine offset to the specific CppToPcie BAR
    alu_shf[offset, offset, OR, bar_ndx, <<2]

    // write new configuration
    alu[$xfer, --, B, value]
    pcie[write_pci, $xfer, addr_msb, <<8, offset, 1], sig_done[sgnl]
    ctx_arb[sgnl]

    // wait for write to complete
    pcie[read_pci, $xfer, addr_msb, <<8, offset, 1], sig_done[sgnl]
    ctx_arb[sgnl]

.end
#endm

/**
 * Configures a PcieToCpp Expansion BAR with the specified value
 * 
 * @param island  The PCIe island hosting the BAR to be configured
 * @param pf_bar  Identifies the PF BAR { 0, 1, 2 }
 * @param exp_bar Identifies the expansion BAR { 0, 1, ..., 7 }
 * @param value   The new value to be written to the given BAR
 */
#macro configure_pcie_to_cpp_bar(island, pf_bar, exp_bar, value)
.begin

    .reg addr_msb
    .reg offset
    .reg $xfer
    .sig sgnl

    // set address MSB
    alu_shf[addr_msb, --, B, island, <<30]

    // BAR configuration is performed by writing to PCIe internal target
    // at offset 0x3 0000
    immed[offset, 0x0]
    immed_w1[offset, 0x3]

    // determine offset to the specific PcieToCpp Expansion BAR (PF0)
    alu_shf[offset, offset, OR, pf_bar,  <<5]
    alu_shf[offset, offset, OR, exp_bar, <<2]

    // write new configuration
    alu[$xfer, --, B, value]
    pcie[write_pci, $xfer, addr_msb, <<8, offset, 1], sig_done[sgnl]
    ctx_arb[sgnl]

    // wait for write to complete
    pcie[read_pci, $xfer, addr_msb, <<8, offset, 1], sig_done[sgnl]
    ctx_arb[sgnl]

.end
#endm

/**
 * Configures Root Complex BAR
 *
 * @param island     The PCIe Root Complex island to be configured
 *
 * @param addr_63_32 The upper 32 bits of the address to be configured
 * @param addr_31_00 The lower 32 bits of the address to be configured
 */
#macro configure_rc_bar(island, addr_63_32, addr_31_00)
.begin

    .reg base
    .reg offset
    .reg $xfer
    .reg mask
    .reg tmp
    .sig sgnl

    // get offset based on BAR index
    immed[offset, NFP_PCIEX_RC_i_rc_pcie_base_I_RC_BAR_0]

    // Set base addressing for accessing the RC registers via XPB
    immed[base, 0x0]
    immed_w1[base, PCIE_XPB_TARGET_ID_RC_REGS]
    alu_shf[base, base, OR, PCIE_XPB_SLAVE_INDEX_RC_REGS, <<22]
    alu_shf[base, base, OR, island, <<24]

    // set RC_BAR_0 to lower 32-bits of desired BAR
    move(tmp, addr_31_00)
    move(mask, 0xffffff00)
    alu[$xfer, tmp, AND, mask]
    ct[xpb_write, $xfer, base, offset, 1], sig_done[sgnl]
    ctx_arb[sgnl]

    // set RC_BAR_1 to upper 32-bits of desired BAR
    move($xfer, addr_63_32)    
    alu[offset, 4, +, offset]				
    ct[xpb_write, $xfer, base, offset, 1], sig_done[sgnl]
    ctx_arb[sgnl]

    // wait for XPB write to complete before returning
    ct[xpb_read, $xfer, base, offset, 1], sig_done[sgnl]
    ctx_arb[sgnl]

.end
#endm

/**
 * Configures aperture and enables the RC BAR
 *
 * @param island      The island number corresponding to the PCIe island to be
 *                    configured
 * @param aperture_sz The size (in bits) of the aperture to be configured for 
 *                    the RC BAR
 */
#macro configure_rc_aperture(island, aperture_sz)
.begin

    .reg base
    .reg $xfer
    .reg mask
    .reg value
    .reg tmp
    .sig sgnl

    // Set base addressing for accessing the LM registers via XPB
    immed[base, 0x0]
    immed_w1[base, PCIE_XPB_TARGET_ID_LM_REGS]
    alu_shf[base, base, OR, PCIE_XPB_SLAVE_INDEX_LM_REGS, <<22]
    alu_shf[base, base, OR, island, <<24]

    // read current value
    move(tmp, NFP_PCIEX_LM_i_regf_lm_pcie_base_I_RC_BAR_CONFIG_REG)
    ct[xpb_read, $xfer, base, tmp, 1], sig_done[sgnl]
    ctx_arb[sgnl]
    alu[value, --, B, $xfer]

    // normalize the aperture size
    #if isnum(aperture_sz)
        immed[tmp, aperture_sz]
        alu[tmp, tmp, -, 2]
    #else
        alu[tmp, aperture_sz, -, 2]
    #endif

    ; mask off aperture and control bits
    immed[mask, 0x1ff]
    alu[value, value, AND~, mask]

#define BAR_CONTROL_DISABLED 			0
#define BAR_CONTROL_32_BIT_IO 			1
#define BAR_CONTROL_RESERVED0   		2
#define BAR_CONTROL_RESERVED1   		3
#define BAR_CONTROL_32_BIT_MEM 			4
#define BAR_CONTROL_32_BIT_MEM_PREFETCHABLE 	5
#define BAR_CONTROL_64_BIT_MEM			6
#define BAR_CONTROL_64_BIT_MEM_PREFETCHABLE	7

    ; format new value
    alu[value, value, OR, BAR_CONTROL_64_BIT_MEM, <<6]
    alu[value, value, OR, tmp]
    alu[value, value, OR, 1, <<31] 	; RCBCE

update_aperture#:
    // write new value
    move(tmp, NFP_PCIEX_LM_i_regf_lm_pcie_base_I_RC_BAR_CONFIG_REG)
    alu[$xfer, --, B, value]
    ct[xpb_write, $xfer, base, tmp, 1], sig_done[sgnl]
    ctx_arb[sgnl]

    // wait for write to complete before returning
    ct[xpb_read, $xfer, base, tmp, 1], sig_done[sgnl]
    ctx_arb[sgnl]

.end
#endm

;------------------------------------------------------------------------------
; Root Complex PCIe link configuration
;------------------------------------------------------------------------------

/**
 * Sets MPS for PCIe island configured as a Root Complex
 * @param rc_island identifies the PCIe RC island number
 * @param mps       MPS to be configured [powers of 2 between 128 - 4096]
 */
#macro set_mps_rc(rc_island, mps)
.begin

    .reg base
    .reg offset
    .reg contents
    .reg mask
    .reg field
    .reg $xfer
    .sig sgnl

    // Set base addressing for accessing the RC registers via XPB
    immed[base, 0x0]
    immed_w1[base, PCIE_XPB_TARGET_ID_RC_REGS]
    alu_shf[base, base, OR, PCIE_XPB_SLAVE_INDEX_RC_REGS, <<22]
    alu_shf[base, base, OR, rc_island, <<24]

    // read current value
    move(offset, NFP_PCIEX_PF_i_PCIE_cap_struct_I_PCIE_DEV_CTRL_STATUS)
    ct[xpb_read, $xfer, base, offset, 1], sig_done[sgnl]
    ctx_arb[sgnl]
    alu[contents, --, B, $xfer]
    
    // ensure requested MPS is valid
    move(field, mps)
    immed[mask, 0x1f80]
    alu[field, field, AND, mask] 
    beq[done#]

    // modify
    ffs[field, field]						; log2(mps)
    alu[field, field, -, 7]					; log2(mps) - 7
    alu[contents, contents, AND~, 0x00e0]
    alu_shf[contents, contents, OR, field, <<5]  

    // write
    alu[$xfer, contents, OR, 0]
    ct[xpb_write, $xfer, base, offset, 1], sig_done[sgnl]
    ctx_arb[sgnl]

done#:

.end
#endm


/**
 * Sets MRRS for PCIe island configured as a Root Complex
 * @param rc_island identifies the PCIe RC island number
 * @param mrrs      MRRS to be configured [power of 2 in range 128-4096]
 */
#macro set_mrrs_rc(rc_island, mrrs)
.begin

    .reg base
    .reg offset
    .reg contents
    .reg mask
    .reg field
    .reg $xfer
    .sig sgnl

    // Set base addressing for accessing the RC registers via XPB
    immed[base, 0x0]
    immed_w1[base, PCIE_XPB_TARGET_ID_RC_REGS]
    alu_shf[base, base, OR, PCIE_XPB_SLAVE_INDEX_RC_REGS, <<22]
    alu_shf[base, base, OR, rc_island, <<24]

    // read current value
    move(offset, NFP_PCIEX_PF_i_PCIE_cap_struct_I_PCIE_DEV_CTRL_STATUS)
    ct[xpb_read, $xfer, base, offset, 1], sig_done[sgnl]
    ctx_arb[sgnl]
    alu[contents, --, B, $xfer]
    
    // ensure requested MRRS is valid
    move(field, mrrs)
    immed[mask, 0x1f80]
    alu[field, field, AND, mask]
    beq[done#]

    // modify
    ffs[field, field]					; log2(mrrs)
    alu[field, field, -, 7]				; log2(mrrs) - 7
    immed[mask, 0x7000]
    alu[contents, contents, AND~, mask]
    alu_shf[contents, contents, OR, field, <<12]  

    // write
    alu[$xfer, contents, OR, 0]
    ct[xpb_write, $xfer, base, offset, 1], sig_done[sgnl]
    ctx_arb[sgnl]

done#:

.end
#endm


/**
 * Sets RCB for PCIe island configured as a Root Complex
 * @param rc_island identifies the PCIe RC island number
 * @param rcb       RCB to be configured [64, 128]
 */
#macro set_rcb_rc(rc_island, rcb)
.begin

    .reg base
    .reg offset
    .reg contents
    .reg field
    .reg $xfer
    .sig sgnl

    // Set base addressing for accessing the RC registers via XPB
    immed[base, 0x0]
    immed_w1[base, PCIE_XPB_TARGET_ID_RC_REGS]
    alu_shf[base, base, OR, PCIE_XPB_SLAVE_INDEX_RC_REGS, <<22]
    alu_shf[base, base, OR, rc_island, <<24]

    // read current value
    move(offset, NFP_PCIEX_PF_i_PCIE_cap_struct_I_LINK_CTRL_STATUS)
    ct[xpb_read, $xfer, base, offset, 1], sig_done[sgnl]
    ctx_arb[sgnl]
    alu[contents, --, B, $xfer]

    // ensure requested RCB is valid
    move(field, rcb)
    alu[field, field, AND, 0xc0]
    beq[done#]
    
    // modify
    ffs[field, field]      					; log2(field)
    alu[field, field, -, 6]
    alu[contents, contents, AND~, 0x08]
    alu_shf[contents, contents, OR, field, <<3]  

    // write
    alu[$xfer, contents, OR, 0]
    ct[xpb_write, $xfer, base, offset, 1], sig_done[sgnl]
    ctx_arb[sgnl]

done#:

.end
#endm

;------------------------------------------------------------------------------
; Endpoint PCIe link configuration
;------------------------------------------------------------------------------

/**
 * Sets MPS for PCIe island configured as an endpoint
 * @param ep_island identifies the PCIe EP island number
 * @param mps       MPS to be configured [powers of 2 between 128 - 4096]
 */
#macro set_mps_ep(ep_island, mps)
.begin

    .reg base
    .reg offset
    .reg contents
    .reg mask
    .reg field
    .reg $xfer
    .sig sgnl

    // Set base addressing for accessing the PF registers via XPB
    immed[base, 0x0]
    immed_w1[base, PCIE_XPB_TARGET_ID_PF_REGS]
    alu_shf[base, base, OR, PCIE_XPB_SLAVE_INDEX_PF_REGS, <<22]
    alu_shf[base, base, OR, ep_island, <<24]

    // read current value
    move(offset, NFP_PCIEX_PF_i_PCIE_cap_struct_I_PCIE_DEV_CTRL_STATUS)
    ct[xpb_read, $xfer, base, offset, 1], sig_done[sgnl]
    ctx_arb[sgnl]
    alu[contents, --, B, $xfer]
    
    // ensure requested MPS is valid
    move(field, mps)
    immed[mask, 0x1f80]
    alu[field, field, AND, mask] 
    beq[done#]

    // modify
    ffs[field, field]						; log2(field)
    alu[field, field, -, 7]                 ; log2(field) - 7
    alu[contents, contents, AND~, 0x00e0]
    alu_shf[contents, contents, OR, field, <<5]  

    // write
    alu[$xfer, contents, OR, 0]
    ct[xpb_write, $xfer, base, offset, 1], sig_done[sgnl]
    ctx_arb[sgnl]

done#:

.end
#endm


/**
 * Sets MRRS for PCIe island configured as an Endpoint
 * @param ep_island identifies the PCIe EP island number
 * @param mrrs      MRRS to be configured [power of 2 in range 128-4096]
 */
#macro set_mrrs_ep(ep_island, mrrs)
.begin

    .reg base
    .reg offset
    .reg contents
    .reg mask
    .reg field
    .reg $xfer
    .sig sgnl

    // Set base addressing for accessing the PF registers via XPB
    immed[base, 0x0]
    immed_w1[base, PCIE_XPB_TARGET_ID_PF_REGS]
    alu_shf[base, base, OR, PCIE_XPB_SLAVE_INDEX_PF_REGS, <<22]
    alu_shf[base, base, OR, ep_island, <<24]

    // read current value
    move(offset, NFP_PCIEX_PF_i_PCIE_cap_struct_I_PCIE_DEV_CTRL_STATUS)
    ct[xpb_read, $xfer, base, offset, 1], sig_done[sgnl]
    ctx_arb[sgnl]
    alu[contents, --, B, $xfer]
    
    // ensure requested MRRS is valid
    move(field, mrrs)
    immed[mask, 0x1f80]
    alu[field, field, AND, mask]
    beq[done#]

    // modify
    ffs[field, field]							; log2(field)
    alu[field, field, -, 7]						; log2(field) - 7
    immed[mask, 0x7000]
    alu[contents, contents, AND~, mask]
    alu_shf[contents, contents, OR, field, <<12]  

    // write
    alu[$xfer, contents, OR, 0]
    ct[xpb_write, $xfer, base, offset, 1], sig_done[sgnl]
    ctx_arb[sgnl]

done#:

.end
#endm


/**
 * Sets RCB for PCIe island configured as an Endpoint
 * @param ep_island identifies the PCIe EP island number
 * @param rcb       RCB to be configured [64, 128]
 */
#macro set_rcb_ep(ep_island, rcb)
.begin

    .reg base
    .reg offset
    .reg contents
    .reg field
    .reg $xfer
    .sig sgnl

    // Set base addressing for accessing the PF registers via XPB
    immed[base, 0x0]
    immed_w1[base, PCIE_XPB_TARGET_ID_PF_REGS]
    alu_shf[base, base, OR, PCIE_XPB_SLAVE_INDEX_PF_REGS, <<22]
    alu_shf[base, base, OR, ep_island, <<24]

    // read current value
    move(offset, NFP_PCIEX_PF_i_PCIE_cap_struct_I_LINK_CTRL_STATUS)
    ct[xpb_read, $xfer, base, offset, 1], sig_done[sgnl]
    ctx_arb[sgnl]
    alu[contents, --, B, $xfer]

    // ensure requested RCB is valid
    move(field, rcb)
    alu[field, field, AND, 0x00c0]
    beq[done#]
    
    // modify
    ffs[field, field]						; log2(field)
    alu[field, field, -, 6]					; log2(field) - 6
    alu[contents, contents, AND~, 0x08]
    alu_shf[contents, contents, OR, field, <<3]  

    // write
    alu[$xfer, contents, OR, 0]
    ct[xpb_write, $xfer, base, offset, 1], sig_done[sgnl]
    ctx_arb[sgnl]

done#:

.end
#endm

;------------------------------------------------------------------------------
; Context number issuer 
;------------------------------------------------------------------------------

/**
 * Initializes the context number issuer
 */
#macro ctx_number_issuer_init()
.begin

    .reg $xfer
    .sig sgnl

    #define _CTX 1
    #while (_CTX < 16)

      immed[$xfer, _CTX]
      mem[qadd_work, $xfer, ctx_addr_39_32, <<8, CTX_NUMBER_RING/**/PCIE_EP_ISLAND_NUM/**/_NUM, 1], sig_done[sgnl]
      ctx_arb[sgnl]
      nop  ;;; to maintain distance of 3 instructions between br_!signal[]

    #define_eval _CTX (_CTX+1)
    #endloop
    #undef _CTX

.end
#endm

/**
 * allocates a context number 
 * @param ctx_number the issued context number provided upon return
 */
#macro ctx_number_issuer_alloc(ctx_number)
.begin

    .reg $xfer
    .sig sgnl

    mem[qadd_thread, $xfer, ctx_addr_39_32, <<8, CTX_NUMBER_RING/**/PCIE_EP_ISLAND_NUM/**/_NUM, 1], sig_done[sgnl]

check_signals#:
    ; check if we got a context number
    br_signal[sgnl, got_ctx_num#]

    ; handle oldest dma completion signal if asserted
    check_oldest()

    ; repeat
    ctx_arb[voluntary], br[check_signals#]

got_ctx_num#:
    alu[ctx_number, --, B, $xfer]

.end
#endm

/**
 * frees a previously issued context number making it available to others
 * @param ctx_number the context number to be freed
 */
#macro ctx_number_issuer_free(ctx_number)
.begin

    .reg $xfer
    .sig sgnl

    alu[$xfer, --, B, ctx_number]
    beq[ctx_freed#]

    mem[qadd_work, $xfer, ctx_addr_39_32, <<8, CTX_NUMBER_RING/**/PCIE_EP_ISLAND_NUM/**/_NUM, 1], ctx_swap[sgnl]

ctx_freed#:

.end
#endm

/**
 * Populates PM Header Data CSR
 * @island Identifies the PCIe island
 * @index  Identifies the register
 * @VALID  Valid bit
 * @MTYPE  Metadata type 
 * @NFP    NFP
 * @PTYPE  Identifies the packet type
 */
#macro pcie_pkt_mode_configure_pm_header_data(island, index, VALID, MTYPE, NFP, PTYPE)
.begin

    .reg offset
    .reg tmp
    .reg addr_msb
    .reg $xfer[2]
    .xfer_order $xfer
    .sig sgnl

    // set address MSB
    alu_shf[addr_msb, --, B, island, <<30]

    // BAR configuration is performed by writing to PCIe internal target
    // at offset 0x3 0000
    immed32[offset, (NFP_PCIE_DMA+NFP_PCIE_DMA_DMA_PM_HEADER(0))]

    // determine offset to the specific PcieToCpp Expansion BAR
    alu_shf[offset, offset, OR, index,  <<3]

    // write new configuration
    ; a) 1st 32-bit word
    alu[tmp,      --,  B,  VALID, <<0]
    alu[tmp,      tmp, OR, MTYPE, <<1]
    alu[$xfer[0], tmp, OR, NFP,   <<8]

    ; b) 2nd 32-bit word
    alu[$xfer[1], --, B,  PTYPE, <<0]

    ; PCIe internal target only support 4B writes except for DMA descriptors which 
    ; allow 16B writes
    pcie[write_pci, $xfer[0], addr_msb, <<8, offset, 1], sig_done[sgnl]
    ctx_arb[sgnl]

    alu[offset, offset, +, 4]
    pcie[write_pci, $xfer[1], addr_msb, <<8, offset, 1], sig_done[sgnl]
    ctx_arb[sgnl]

.end
#endm

/**
 * Determines DMADescriptorConfig (and DMA PMHeaderData) CSR index associated
 * with given packet type and valid status
 * @_out_index   the calculated index (provided upon return)
 * @_in_pkt_type the input packet type
 * @_in_valid    specifies if input is valid (1) or not (0)
 */
#macro pcie_pkt_mode_get_dma_config_csr_index(_out_index, _in_pkt_type, _in_valid)
.begin

    immed[_out_index, 0]

    .if   (_in_pkt_type == IPV4_PKT)
        immed[_out_index, PMH_NDX_INVALID_IPV4_PKT]
    .elif (_in_pkt_type == ARP_PKT)
        immed[_out_index, PMH_NDX_INVALID_ARP_PKT]
    .elif (_in_pkt_type == IPV4_EXCEPTION_PKT)
        immed[_out_index, PMH_NDX_INVALID_IPV4_EXCEPTION_PKT]
    .elif (_in_pkt_type == NON_IPV4_PKT)
        immed[_out_index, PMH_NDX_INVALID_NON_IPV4_PKT]
    .elif (_in_pkt_type == ERROR_PKT)
        immed[_out_index, PMH_NDX_INVALID_ERROR_PKT]
    .endif

    ; if valid, set bit
    alu[_out_index, _out_index, OR, _in_valid, <<3]

.end
#endm

/**
 * Writes an "empty" descriptor to CLS ring 
 * @ring   the ring array to which the CLS descriptor is to be written
 * @sgnl   the completion signal associated with the ring access
 * @LENGTH the number of longwords comprising the CLS descriptor
 */
#macro pcie_pkt_mode_write_empty_cls_descriptor(ring, sgnl, LENGTH)
.begin

#define_eval _NDX 0
#while (_NDX < LENGTH)
    alu[ring[_NDX], --, B, minus_one]
    #define_eval _NDX (_NDX + 1)
#endloop
#undef _NDX

    ru_cls_ring_put(ring[0], LOCAL_DESC_RING_NUM, --, LENGTH, sgnl, SIG_WAIT)

.end
#endm

;------------------------------------------------------------------------------
; PCIe DMA support
;------------------------------------------------------------------------------

/*
 * DMA Mode definitions
 */

#define DMA_MODE_AUTOPUSH_0  		   0
#define DMA_MODE_AUTOPUSH_1  		   1
#define DMA_MODE_EVENT       		   2
#define DMA_MODE_GATHER      		   3

/**
 * Returns with DMA Mode and ModeSelect bits
 * @mode provides the DmaMode and ModeSelect bits necessary to signal the
 *       current context via autopush
 * @sg   the signal to be asserted
 */
#macro pcie_pkt_mode_get_dma_mode(mode, sg)
.begin

    .reg ctx
    .reg meid

    local_csr_rd[ACTIVE_CTX_STS]
    immed[ctx, 0]

    alu[meid, 0x0f, AND,   ctx, >>3]
    alu[ctx,  0x07, AND,   ctx]

    alu[mode, --,   B,     &sg, <<14]
    alu[mode, mode, OR,    ctx, <<18]
    alu[mode, mode, OR, cl_num, <<21]
    alu[mode, mode, OR,   meid, <<27]

.end
#endm

/**
 * Resources used by ToPCIe DMAs
 */
#if defined(PCIE_DMA_DIR_TO_HOST) || defined(PCIE_DMA_DIR_FROM_HOST)

 #define NDX MAX_PCIE_DMA_SIGNALS
 #while (NDX > 0)
  #define_eval NDX (NDX-1)

  ; ToPCIe DMA completion signal
  .sig volatile dsgnl/**/NDX

 #endloop
 #undef NDX

 ; maintains number of total ME dma credits used
 .reg @credits_avail
   
 ; maintains thread credits
 .reg credits_used

 ; maintains starting index in local memory for each context
 .reg lm0_ctx_offset
 
 ; maintain starting index in local memory for descriptor table
 .reg lm1_ctx_offset

 ; index of current entry to write
 .reg lm1_cur_index

 ; index of current entry to read
 .reg lm2_cur_index

 ; maintains current CLS address to be written
 .reg lm1_cls_addr

 ; maintains current CLS address to be rad
 .reg lm2_cls_addr

 ; next signal to be waited upon
 .reg oldest_dsgnl_index

 ; next signal to be issued
 .reg newest_dsgnl_index

 ; dma index for current packet 
 .reg dma_index

 ; number of times a wait was required before issuing DMA
 .reg @num_pkt_waits

 .reg lm0_value

#endif

/**
 * Resets state used during ToPcie DMAs
 */
#macro pcie_pkt_mode_reset_init_state()
.begin

    .if (ctx() == 0)
        immed[@credits_avail, PCIE_DMA_CREDITS]
    .endif

    immed[credits_used, 0]

    immed[oldest_dsgnl_index, 0]
    immed[newest_dsgnl_index, 0]
    immed[dma_index, 0]

    .if (ctx() == 0)
        immed[@num_pkt_waits, 0]
    .endif

.end
#endm


#define MAX_CONTEXTS       8
#define LM0_WORDS_PER_CTX  8	; allow up to 8 signals
#define LM0_BYTES_PER_CTX (4 * LM0_WORDS_PER_CTX)
#define LM0_TOTAL_BYTES   (MAX_CONTEXTS * LM0_BYTES_PER_CTX)

/**
 * Initializes the DMA Modes for each ToPCIe credit allocated
 */
#macro pcie_pkt_mode_initialize_to_pcie_dma_modes()
.begin

    .reg mode

    ; each context has it's own range of LM of length LM_BYTES_PER_CTX bytes
    immed[lm0_ctx_offset, 0]
#define _CTX 0
#while _CTX < 8
    .if (ctx() == _CTX)
        immed[lm0_ctx_offset, (_CTX * LM0_BYTES_PER_CTX)]
    .endif
 #define_eval _CTX (_CTX+1)
#endloop
#undef _CTX

    local_csr_wr[ACTIVE_LM_ADDR_0, lm0_ctx_offset]    
    nop
    nop
    nop

#define NDX 0
#while (NDX < MAX_PCIE_DMA_SIGNALS)
    pcie_pkt_mode_get_dma_mode(mode, dsgnl/**/NDX)
    alu[*l$index0++, --, B, mode]
 #define_eval NDX (NDX+1)
#endloop
#undef NDX

    ; reset address
    local_csr_wr[ACTIVE_LM_ADDR_0, lm0_ctx_offset]    
    nop
    nop
    nop

.end
#endm

#if defined(PCIE_DMA_DIR_TO_HOST) || defined(PCIE_DMA_DIR_FROM_HOST)

 #if defined(PCIE_DMA_DIR_TO_HOST)
  #define LM1_WORDS_PER_ENTRY       8
 #else
  #define LM1_WORDS_PER_ENTRY       4
 #endif

 #define LM1_BYTES_PER_ENTRY      (4 * LM1_WORDS_PER_ENTRY)
 #define LM1_NUM_ENTRIES_PER_CTX   10
 #define LM1_BYTES_PER_CTX        (LM1_NUM_ENTRIES_PER_CTX * LM1_BYTES_PER_ENTRY)  
 #define LM1_BASE_BYTE_OFFSET      LM0_TOTAL_BYTES

 #define LM1_LENGTH 		 (LM1_BYTES_PER_CTX * MAX_CONTEXTS)

 .alloc_mem CLS_DESC_BASE \
    cls me LM1_LENGTH 16 reserved


 #if ((LM1_BASE_BYTE_OFFSET+LM1_LENGTH)>(4*1024))
  #error "LM requested exceeds 4k"
 #endif

 #info "LM1 length "LM1_LENGTH

#endif

/**
 * Initializes the offset in local scratch used to maintain
 * the descriptor table
 */
#macro pcie_pkt_mode_initialize_descriptor_table()
.begin

    .reg tmp

    ; each context has it's own range of LM of length LM_BYTES_PER_CTX bytes
    immed[lm1_ctx_offset, 0]
#define _CTX 0
#while _CTX < 8
    .if (ctx() == _CTX)
        immed[lm1_ctx_offset, (_CTX * LM1_BYTES_PER_CTX)]
    .endif
 #define_eval _CTX (_CTX+1)
#endloop
#undef _CTX

    immed[tmp, LM1_BASE_BYTE_OFFSET]
    alu[lm1_ctx_offset, lm1_ctx_offset, +, tmp] 

    local_csr_wr[ACTIVE_LM_ADDR_1, lm1_ctx_offset]
    local_csr_wr[ACTIVE_LM_ADDR_2, lm1_ctx_offset] 

    immed[lm1_cur_index, 0]
    immed[lm2_cur_index, 0]

    nop

.end
#endm

#if defined(PCIE_DMA_DIR_TO_HOST)

/**
 * 'push' packet metadata and FL descriptors on local memory 
 * @param fldesc     - freelist descriptor
 * @param header     - packet metadata descriptor
 * @param dest_port  - destination port number
 * @param pkt_type   - packet type identifier
 * @param pkt_valid  - packet valid attribute
 * @param seq_number - sequencer number from header 
 */ 
#macro pcie_pkt_mode_lm_push(fldesc, header, dest_port, pkt_type, pkt_valid, seq_number)
.begin

    .reg tmp

    ; push header
    ; format
    ; - header[31:31] - 0 if last portion of packet, 1 o.w.
    ; - header[19: 8] - sequence number
    ; - header[ 7: 0] - dma index (packet segment, when multiple DMAs/packet)
    alu[tmp, dma_index, OR, seq_number, <<8]
    alu[*l$index1++, --, B, tmp]

    ; add header/metadata
    alu[*l$index1++, --, B, $header[0]]  	
    alu[*l$index1++, --, B, $header[1]]
    alu[*l$index1++, --, B, $header[2]]
    alu[*l$index1++, --, B, $header[3]]		

    ; add freelist descriptor
    alu[*l$index1++, --, B, $fldesc[0]]  	;;; DMA Address Hi
    alu[*l$index1++, --, B, $fldesc[1]]		;;; DMA Address Lo

    ; add additional information (formatted)
    alu[tmp, dest_port, OR, pkt_type, <<16]
    alu[tmp, tmp, OR, pkt_valid, <<24]
    alu[*l$index1++, --, B, tmp]		;;; pkt valid | pkt type | -- | dest port

    ; fixup index
    alu[lm1_cur_index, lm1_cur_index, +, 1]
    alu[--, lm1_cur_index, -, LM1_NUM_ENTRIES_PER_CTX]

    blt[lm1_index_updated#]

    local_csr_wr[ACTIVE_LM_ADDR_1, lm1_ctx_offset]
    immed[lm1_cur_index, 0]

    nop

lm1_index_updated#:
    immed[dma_index, 0]

.end
#endm

/**
 * 'push' empty packet metadata and FL descriptors to local memory 
 */ 
#macro pcie_pkt_mode_lm_push_placeholder()
.begin

    .reg tmp

    ; add header/metadata
    alu[tmp, dma_index, OR, 1, <<31]

    ; header
    alu[*l$index1++, --, B, tmp]  	

    ; data
    immed[*l$index1++, 0]
    immed[*l$index1++, 0]
    immed[*l$index1++, 0]		
    immed[*l$index1++, 0]
    immed[*l$index1++, 0]
    immed[*l$index1++, 0]
    immed[*l$index1++, 0]

    ; fixup index
    alu[lm1_cur_index, lm1_cur_index, +, 1]

    alu[--, lm1_cur_index, -, LM1_NUM_ENTRIES_PER_CTX]
    blt[lm1_index_updated#]

    local_csr_wr[ACTIVE_LM_ADDR_1, lm1_ctx_offset]
    immed[lm1_cur_index, 0]
    
    nop

lm1_index_updated#:

    ; increment local DMA index for packet
    alu[dma_index, dma_index, +, 1]

.end
#endm

/**
 * 'pops' frelist descriptor and packet metadata descriptor off local memory,
 * frees associated MU pointer and enqueues RXTX descriptor
 */ 
#macro pcie_pkt_mode_lm_pop()
.begin
    
    .reg mask
    .reg header
    .reg seq_number_shl8b
    .reg phdr[4]
    .reg $desc[4]
    .xfer_order $desc
    .sig sgnl

    ; retrieve header
    alu[header, --, B,  *l$index2++]
    alu[--, header, AND, 1, <<31]
    beq[real_header#]

    ; dummy read 
    alu[--, --, B, *l$index2++]
    alu[--, --, B, *l$index2++]
    alu[--, --, B, *l$index2++]
    alu[--, --, B, *l$index2++]
    alu[--, --, B, *l$index2++]
    alu[--, --, B, *l$index2++]
    alu[--, --, B, *l$index2++]
    br[update_lm2_index#]

real_header#:

    ; get sequence number (seq_number_shr8b = seq_number << 8)
    ld_field_w_clr[seq_number_shl8b, 0110, header]

    ; retrieve stashed away packet header
    alu[phdr[0], --, B, *l$index2++]
    alu[phdr[1], --, B, *l$index2++]
    alu[phdr[2], --, B, *l$index2++]
    alu[phdr[3], --, B, *l$index2++]

    wintf_free_muptr(phdr)

#if !defined(TO_PCIE_DROP_PACKET)
    ; retrieve FL descriptor
    alu[$desc[0], seq_number_shl8b, OR, *l$index2++] 	; dma_addr_hi | (seqnum << 8)
    alu[$desc[1], --, B, *l$index2++] 			; dma_addr_lo

    immed[mask, 0x3fff]
    alu[$desc[2], mask, AND, phdr[0]]  			; strip packet length
    alu[$desc[3], --, B, *l$index2++]			; pkt_valid << 24 | pkt_type << 16 | dest_port

    ; add RXTX descriptor to RXTX work queue
    ; add RX/TX buffer descriptor to work queue to make available to NBI egress
    pcie_pkt_mode_rxtxq_put($desc, sgnl]
#else
    ; retrieve FL descriptor
    alu[$desc[0], --, B, *l$index2++] 			; dma_addr_hi
    alu[$desc[1], --, B, *l$index2++]			; dma_addr_lo

    pcie_pkt_mode_freelist_put($desc, sgnl)
    alu[tmp, --, B, *l$index2++] 			; dummy read
#endif

    ; increment global counter
    alu[@pkt_count, @pkt_count, +, 1]
    local_csr_wr[mailbox3, @pkt_count]

    ; wait for work queue write to comp[lete
    ctx_arb[sgnl]

update_lm2_index#:

    ; fixup indice
    alu[lm2_cur_index, lm2_cur_index, +, 1]
    alu[--, lm2_cur_index, -, LM1_NUM_ENTRIES_PER_CTX]

    blt[lm2_index_updated#]

    local_csr_wr[ACTIVE_LM_ADDR_2, lm1_ctx_offset]
    immed[lm2_cur_index, 0]
    nop

lm2_index_updated#:

    ; increment credits available
    alu[@credits_avail, @credits_avail, +, 1]
    alu[credits_used, credits_used, -, 1]

.end
#endm

#elif defined(PCIE_DMA_DIR_FROM_HOST)

/**
 * 'push' packet metadata and FL descriptors on local memory 
 * @param txdesc - rxtx descriptor (length 4, only 1st 2 elements accesed)
 */ 
#macro frompcie_pkt_mode_lm_push($txdesc)
.begin

    .reg tmp

    ; push header
    alu[*l$index1++, --, B, dma_index]

    ; add header/metadata
    alu[*l$index1++, 0xff, AND, $txdesc[0]]	; mask only upper 8-bits of DMA address
    alu[*l$index1++, --, B, $txdesc[1]]		; lower 32 bits of DMA address
    immed[*l$index1++, 0]			; not used

    ; fixup index
    alu[lm1_cur_index, lm1_cur_index, +, 1]
    alu[--, lm1_cur_index, -, LM1_NUM_ENTRIES_PER_CTX]

    blt[lm1_index_updated#]

    local_csr_wr[ACTIVE_LM_ADDR_1, lm1_ctx_offset]

    immed[lm1_cur_index, 0]
    alu[lm1_cls_addr, --, B, lm1_ctx_offset]

    nop

lm1_index_updated#:
    immed[dma_index, 0]

.end
#endm

/**
 * 'push' empty packet metadata and FL descriptors to local memory 
 */ 
#macro frompcie_pkt_mode_lm_push_placeholder()
.begin

    .reg tmp

    ; add header/metadata
    alu[tmp, dma_index, OR, 1, <<31]

    ; header
    alu[*l$index1++, --, B, tmp]  	

    ; data
    immed[*l$index1++, 0]
    immed[*l$index1++, 0]
    immed[*l$index1++, 0]		

    ; fixup index
    alu[lm1_cur_index, lm1_cur_index, +, 1]

    alu[--, lm1_cur_index, -, LM1_NUM_ENTRIES_PER_CTX]
    blt[lm1_index_updated#]

    local_csr_wr[ACTIVE_LM_ADDR_1, lm1_ctx_offset]
    immed[lm1_cur_index, 0]

    nop

lm1_index_updated#:

    ; increment local DMA index for packet
    alu[dma_index, dma_index, +, 1]

.end
#endm

/**
 * 'pops' frelist descriptor and packet metadata descriptor off local memory,
 * frees associated MU pointer and enqueues RXTX descriptor
 */ 
#macro pcie_pkt_mode_lm_pop()
.begin
    
    .reg mask
    .reg done
    .reg $fldesc[2]
    .xfer_order $fldesc
    .sig sgnl

pop_top#:

    ; check if this is real packet
    alu[done, --, B,  *l$index2++]
    alu[done, done, AND, 1, <<31]
    bne[update_lm2_index#], defer[3]

      ; -- defer[3] shadow --
      ; retrieve stashed away freelist descriptor
      alu[$fldesc[0], --, B, *l$index2++]
      alu[$fldesc[1], --, B, *l$index2++]
      alu[--, --, B, *l$index2++]
      ; -- defer[3] shadow --

real_header#:

    ; free buffer
    pcie_pkt_mode_freelist_put($fldesc, sgnl)

    ; wait for work queue write to complete
    ctx_arb[sgnl], defer[2]

      ; -- defer[2] shadow --
      ; increment global counter
      alu[@pkt_count, @pkt_count, +, 1]
      local_csr_wr[mailbox3, @pkt_count]
      ; -- defer[2] shadow --

update_lm2_index#:

    ; fixup indice
    alu[lm2_cur_index, lm2_cur_index, +, 1]
    alu[--, lm2_cur_index, -, LM1_NUM_ENTRIES_PER_CTX]

    blt[lm2_index_updated#], defer[2]

      ; -- defer[2] shadow --
      ; increment credits available
      ; alu[@credits_avail, @credits_avail, +, 1]
      immed[@credits_avail, PCIE_DMA_CREDITS]
      alu[credits_used, credits_used, -, 1]
      ; -- defer[2] shadow --

    local_csr_wr[ACTIVE_LM_ADDR_2, lm1_ctx_offset]
    immed[lm2_cur_index, 0]
    nop

lm2_index_updated#:

    ; loop while not done
    br_bset[done, 31, pop_top#]

.end
#endm

#endif

/**
 * Handles reception of a dsgnl by popping local memory contents
 * and updating index of next signal to be waited upon
 */ 
#macro pcie_pkt_mode_dsgnl_received()
.begin

    pcie_pkt_mode_lm_pop()

    ; increment indices
    alu[oldest_dsgnl_index, oldest_dsgnl_index, +, 1]
    alu[--, oldest_dsgnl_index, -, MAX_PCIE_DMA_SIGNALS]
    blt[dsgnl_rcvd_done#]
    
    immed[oldest_dsgnl_index, 0]

dsgnl_rcvd_done#:

.end
#endm


/**
 * Checks if oldest dsgnl has been asserted and 
 */ 
#macro check_oldest()
.begin

    .reg tmp

#define _POW2 1
#while _POW2 < MAX_PCIE_DMA_SIGNALS
 #define_eval _POW2 (2 * _POW2)
#endloop

    alu[tmp, oldest_dsgnl_index, AND, (_POW2-1)]

#undef _POW2

    alu[tmp, --, B, tmp, <<1]
    jump[tmp, jump_table#], targets[jump_check_dsgnl0#, \
                                    jump_check_dsgnl1#, \
                                    jump_check_dsgnl2#, \
                                    jump_check_dsgnl3#, \
                                    jump_check_dsgnl4#, \
                                    jump_dummy5#,       \
                                    jump_dummy6#,       \
                                    jump_dummy7#]

jump_table#:        \
jump_check_dsgnl0#: \
    br_signal[dsgnl0, handle_dsgnl#]
    br[done#]
jump_check_dsgnl1#: \
    br_signal[dsgnl1, handle_dsgnl#]
    br[done#]
jump_check_dsgnl2#: \
    br_signal[dsgnl2, handle_dsgnl#]
    br[done#]
jump_check_dsgnl3#: \
    br_signal[dsgnl3, handle_dsgnl#]
    br[done#]
jump_check_dsgnl4#: \
    br_signal[dsgnl4, handle_dsgnl#]
    br[done#]
jump_dummy5#:       \
    ctx_arb[voluntary]
    br[jump_dummy5#]
jump_dummy6#:       \
    ctx_arb[voluntary]
    br[jump_dummy6#]
jump_dummy7#:       \
    ctx_arb[voluntary]
    br[jump_dummy7#]

handle_dsgnl#:
    pcie_pkt_mode_dsgnl_received()

done#:

.end
#endm

/**
 * Wait until oldest DMA completion signal is asserted
 */ 
#macro pcie_pkt_mode_wait_for_oldest()
.begin

    .reg tmp

#define _POW2 1
#while _POW2 < MAX_PCIE_DMA_SIGNALS
 #define_eval _POW2 (2 * _POW2)
#endloop

    alu[tmp, oldest_dsgnl_index, AND, (_POW2-1)]
    alu[tmp, --, B, tmp, <<1]

    jump[tmp, jump_table#], targets[jump_check_dsgnl0#, \
                                    jump_check_dsgnl1#, \
                                    jump_check_dsgnl2#, \
                                    jump_check_dsgnl3#, \
                                    jump_check_dsgnl4#, \
                                    jump_spin5#,        \
                                    jump_spin6#,        \
                                    jump_spin7#]

jump_table#:        \
jump_check_dsgnl0#: \
    ctx_arb[dsgnl0]
    br[done#]       
jump_check_dsgnl1#: \
    ctx_arb[dsgnl1]
    br[done#]       
jump_check_dsgnl2#: \
    ctx_arb[dsgnl2]
    br[done#]       
jump_check_dsgnl3#: \
    ctx_arb[dsgnl3]
    br[done#]       
jump_check_dsgnl4#: \
    ctx_arb[dsgnl4]
    br[done#]
jump_spin5#:        \
    ctx_arb[voluntary]
    br[jump_spin5#]
jump_spin6#:        \
    ctx_arb[voluntary]
    br[jump_spin6#]
jump_spin7#:        \
    ctx_arb[voluntary]
    br[jump_spin7#]

#undef _POW2

done#:

.end
#endm

; CLS_DEBUG 
; Each context is 4B
.alloc_mem CLS_DEBUG cls me 32 32 reserved

#macro dbg(entry)
.begin

    .reg $x
    .reg addr
    .reg offset
    .sig sgnl

    alu[$x, --, B, entry]
    move(addr, CLS_DEBUG)
    alu[offset, --, B, ctx, <<2] ; 2 bytes per entry
    alu[addr, addr, +, offset]

    cls[write, $x, addr, 0, 1], sig_done[sgnl]
    ctx_arb[sgnl]

.end
#endm


/**
 * Writes PCIe DMA descriptor to DMA descriptor queue at given address. If
 * DMA credits become exhausted, all signals are waited upon 
 * @$desc          array providing the 4 32-bit words for PCIe DMA legacy descriptor.
 *                 (2nd element is not used)
 * @desc1          provides 2nd element of PCIe DMA legacy descriptor (without 
 *                 DMA Mode and Mode Select fields configured)
 * @dma_addr_39_32 the upper 8 bits of the PCIe DMA descriptor queue address
 * @dma_addr_31_00 the lower 32 bits of the PCIe DMA descriptor queue address
 */
#macro pcie_pkt_mode_enqueue_pcie_descriptor($desc, desc1, addr_39_32, addr_31_00)
.begin

    .sig sgnl

    ; if we've issued the max DMAs, must wait for 1st to complete
    alu[--, credits_used, -, MAX_PCIE_DMA_SIGNALS]
    blt[check_credits#]

wait_for_oldest#:
    pcie_pkt_mode_wait_for_oldest()
    pcie_pkt_mode_dsgnl_received()

check_credits#:
    ; handle oldest signal if asserted
    check_oldest()

    ; at this point we have atleast one available signal
    ; chek if any credits avail
    alu[--, --, B, @credits_avail]
    bgt[ready_to_issue_dma#]

    ; repeat
    ctx_arb[voluntary], br[check_credits#]

ready_to_issue_dma#:
    ; issue DMA
    alu[$desc[1], desc1, OR, *l$index0++]
    pcie[write_pci, $desc[0], addr_39_32, <<8, addr_31_00, 4], sig_done[sgnl]

    ; increment index of next dsgnl to be issued
    alu[newest_dsgnl_index, newest_dsgnl_index, +, 1]
    alu[--, newest_dsgnl_index, -, MAX_PCIE_DMA_SIGNALS]
    blt[done#], defer[2]

      ; -- defer[2] shadow --
      ; decrement credits available
      alu[@credits_avail, @credits_avail, -, 1]
      alu[credits_used, credits_used, +, 1]
      ; -- defer[2] shadow --

    ; reset LM index
    local_csr_wr[ACTIVE_LM_ADDR_0, lm0_ctx_offset]
    immed[newest_dsgnl_index, 0]

done#:
    ; wait for DMA to be issued
    ctx_arb[sgnl], defer[2]

      ; -- defer[2] shadow --
      ; increment number of DMAs issued
      alu[@dma_count, @dma_count, +, 1]
      local_csr_wr[mailbox2, @dma_count]
      ; -- defer[2] shadow --

.end
#endm

/**
 * Writes PCIe DMA descriptor to DMA descriptor queue at given address. If
 * DMA credits become exhausted, all signals are waited upon 
 * @$desc          array providing the 4 32-bit words for PCIe DMA legacy descriptor.
 *                 (2nd element is not used)
 * @dma_addr_39_32 the upper 8 bits of the PCIe DMA descriptor queue address
 * @dma_addr_31_00 the lower 32 bits of the PCIe DMA descriptor queue address
 */
#macro frompcie_pkt_mode_enqdesc_no_completion($desc, addr_39_32, addr_31_00)
.begin

    .sig sgnl

    ; global credits are available, check if any signals available
;    alu[--, credits_used, -, MAX_PCIE_DMA_SIGNALS]      
;    blt[check_credits#]

;wait_for_oldest#:
;    pcie_pkt_mode_wait_for_oldest()
;    pcie_pkt_mode_dsgnl_received()

;check_credits#:
    ; handle oldest signal if asserted
;    check_oldest()

    ; at this point we have atleast one available signal
    ; chek if any credits avail
    alu[--, --, B, @credits_avail]
    bgt[issue_dma#]

    ; repeat
    ctx_arb[voluntary], br[check_credits#]

issue_dma#:
    ; issue DMA
    pcie[write_pci, $desc[0], addr_39_32, <<8, addr_31_00, 4], sig_done[sgnl]

    ; decrement credits available
    alu[@credits_avail, @credits_avail, -, 1]
    alu[credits_used, credits_used, +, 1]

    ; update LM with DMA information
    frompcie_pkt_mode_lm_push_placeholder()
 
done#:
    ; wait for pcie[write_pci] to hit the bus
    ctx_arb[sgnl], defer[2]

      ; -- defer[2] shadow --
      ; increment number of DMAs issued
      alu[@dma_count, @dma_count, +, 1]
      local_csr_wr[mailbox2, @dma_count]
      ; -- defer[2] shadow --

.end
#endm

/**
 * PCIe DMAs a packet to host-side buffer
 * @header provides packet descriptor
 * @dma_addr_hi the upper 8 bits of address of host-side buffer that will hold 
 *              the packet upon completion
 * @dma_addr_lo the lower 32 bits of address of  host-side buffer that will hold 
 *              the packet upon completion 
 */
#macro pcie_pkt_mode_dma_packet_to_host($header, dma_addr_hi, dma_addr_lo)
.begin

    .reg pktlen
    .reg pktnum
    .reg pktctm
    ; .reg blqnum
    .reg split_length
    
    .reg mask
    .reg offset
    .reg length
    .reg transfer_length
    .reg dma_cfg_ndx

    .reg cpp_addr_lo
    .reg cpp_addr_hi

    .reg desc1
    .reg $dma_desc[4]
    .xfer_order $dma_desc

    ; get packet attributes
    immed[mask, 0x3fff]
    alu[pktlen, mask, AND, $header[0]]

    ; in C0, CTM split length = 2 ^ (split_len_field+8)
    alu[tmp, 0x07, AND, $header[(NW_PKT_HEADER_SZ-1)], >>28]
    alu[tmp, tmp, +, 8]
    alu[--, tmp, OR, 0]
    alu[split_length, --, B, 1, <<indirect]

    ; -------------------------------------------------------------------------
    ; PCIe DMA the packet to host buffer
    ; -------------------------------------------------------------------------

    ; 
    ; 1) prepare/issue ToPCIe DMA descriptor for CTM
    ;    using packet addressing mode

    alu[offset, --, B, @ctm_offset]
    alu[length, --, B, pktlen]

    immed[dma_cfg_ndx, DDC_TO_PCIE_NDX_NORMAL]

ctm_transfer#:

    alu[transfer_length, --, B, length]

    ; transfer must not exceed end of CTM
    alu[tmp, offset, +, transfer_length]
    alu[--, tmp, -, split_length]
    blt[lt_ctm_split_length#], defer[3]

      ; -- defer[3] shadow --
      immed[mask, 0x1ff]
      alu[pktnum, mask, AND, $header[0], >>16]
      alu[pktctm, --, B, $header[0], >>26]
      ; -- defer[3] shadow --

    alu[transfer_length, split_length, -, offset]

lt_ctm_split_length#:

    ; test against 4kB limit
    alu[--, transfer_length, -, four_k]
    blt[le_4kb_ctm_transfer#], defer[3]

      ; -- defer[3] shadow --
      alu[desc1, 0x80, OR, pktctm]        	; CTM is direct access 
      alu[desc1, desc1, OR, 0x2, <<12]		; CPP Token=2 (since NBI byte swaps)
      alu[$dma_desc[2], --, B, dma_addr_lo]
      ; -- defer[3] shadow --

    alu[transfer_length, --, B, four_k]

le_4kb_ctm_transfer#:

    ; check if last portion of CTM buffer to DMA
    alu[--, tmp, -, split_length]
    blt[check_end_of_ctm#], defer[3]

      ; -- defer[3] shadow --
      alu[tmp, --, B, pktnum, <<16]
      alu[tmp, tmp, OR, offset]
      alu[$dma_desc[0], tmp, OR, 0xC0, <<24]
      ; -- defer[3] shadow --

    ; offset + transfer_length >= split_length 
    ; so, we've reached end of CTM
    immed[dma_cfg_ndx, DDC_TO_PCIE_NDX_FREE_CTM]    
    br[prepare_ctm_dmadesc#]

check_end_of_ctm#:

    ; offset + transfer_length < split_length 
    ; so, check if end of data
    alu[--, transfer_length, -, length]
    blt[prepare_ctm_dmadesc#]

    ; transfer_length >= length, so we've reached end of data/CTM
    immed[dma_cfg_ndx, DDC_TO_PCIE_NDX_FREE_CTM]    

prepare_ctm_dmadesc#:

    ; [ModeSelect|DmaMode] populated when enqueuing desscriptor
    alu[desc1, desc1, OR, dma_cfg_ndx, <<8]
    alu[tmp, transfer_length, -, 1]
    alu[$dma_desc[3], dma_addr_hi, OR, tmp, <<20]

    ; enqueue descriptor
    pcie_pkt_mode_enqueue_pcie_descriptor($dma_desc, desc1, pcie_ep_addr_39_32, pcie_desc_addr_31_00)

    ; update length and offset
    alu[length, length, -, transfer_length]
    ble[packet_transferred#], defer[3]

      ; -- defer[3] shadow --
      ; update offset into packet
      alu[offset, offset, +, transfer_length]
    
      ; update DMA (pcie-space) address
      alu[dma_addr_lo, dma_addr_lo, +, transfer_length]
      alu[dma_addr_hi, dma_addr_hi, +carry, 0]
      ; -- defer[3] shadow --

    ; push placeholder
    pcie_pkt_mode_lm_push_placeholder()

    ; more CTM to transfer?
    alu[--, offset, -, split_length]
    blt[ctm_transfer#], defer[3]

      ; -- defer[3] shadow --
      ; convert 29-bit MU ptr to CPP address
      alu[cpp_addr_lo, --, B, $header[1], <<11]
      alu[cpp_addr_hi, --, B, $header[1], >>21]
      alu[cpp_addr_lo, cpp_addr_lo, +, split_length]
      ;;; alu[cpp_addr_hi, cpp_addr_hi, +carry, 0]
      ; -- defer[3] shadow --

    immed[dma_cfg_ndx, DDC_TO_PCIE_NDX_NORMAL]

mu_transfer#:

    ;
    ; 2) prepare/issue
    ;

    alu[transfer_length, --, B, length]

    ; transfer_length must not exceed 4k
    alu[--, transfer_length, -, four_k]
    ble[have_xfer_length_mu#], defer[3]

      ; -- defer[3] shadow --
      alu[$dma_desc[0], --, B, cpp_addr_lo]

      ; [ModeSelect|DmaMode] populated when enqueuing descriptor
      alu[desc1, --, B, cpp_addr_hi]
      alu[desc1, desc1, OR, dma_cfg_ndx, <<8]
      ; -- defer[3] shadow --

    alu[transfer_length, --, B, four_k]

have_xfer_length_mu#:

    alu[$dma_desc[2], --, B, dma_addr_lo]

    alu[tmp, transfer_length, -, 1]
    alu[$dma_desc[3], dma_addr_hi, OR, tmp, <<20]

    ; enqueue DMA descriptor
    pcie_pkt_mode_enqueue_pcie_descriptor($dma_desc, desc1, pcie_ep_addr_39_32, pcie_desc_addr_31_00)

    ; update length and offset
    alu[length, length, -, transfer_length]
    ble[packet_transferred#], defer[3]

      ; -- defer[3] shadow --
      ; update addresses
      alu[dma_addr_lo, dma_addr_lo, +, transfer_length]
      ;;; alu[dma_addr_hi, dma_addr_hi, +carry, 0]

      alu[cpp_addr_lo, cpp_addr_lo, +, transfer_length]
      alu[cpp_addr_hi, cpp_addr_hi, +carry, 0]
      ; -- defer[2] shadow --

    ; push placeholder
    pcie_pkt_mode_lm_push_placeholder()

    ; handle remaining MU buffer
    br[mu_transfer#]

packet_transferred#:

.end
#endm

/**
 * PCIe DMAs a packet from host-side buffer to local CTM/MU buffers
 * @txdesc  RX/TX Buffer descriptor providing host-side buffer information
 */
#macro frompcie_pkt_mode_issue_dma($txdesc)
.begin

    .reg dma_addr_lo
    .reg dma_addr_hi

    .reg mask
    .reg length
    .reg transfer_length

    .reg stage

    .reg pkt_type
    .reg pkt_valid
    .reg dest_port
    .reg dmacfg_ndx
    .reg ctx_num
    .reg seq_number

    .reg dmadesc0
    .reg dmadesc1
    .reg dmadesc3

    .reg $dmadesc[4]
    .xfer_order $dmadesc

    .reg tmp

    ; get pkt length and host-side pcie-space address
    immed[mask, 0x03fff]
    alu[length, mask, AND, $txdesc[2]]

    ld_field_w_clr[dma_addr_hi, 0001, $txdesc[0]]
    alu[dma_addr_lo, --, B, $txdesc[1]]

    ld_field_w_clr[seq_number, 0011, $txdesc[0], >>8]

    ; determine if we need to allocate a context number
    immed[ctx_num, 0]
#ifndef RANDOMIZE_FROM_SEGMENTS
    alu[--, length, -, four_k]
    ble[got_ctx_number#], defer[3]

      ; -- defer[3] shadow --
      ; get packet valid, packet type and dest port 
      ld_field_w_clr[pkt_valid, 1000, $txdesc[3]]
      ld_field_w_clr[pkt_type,  0100, $txdesc[3]]
      ld_field_w_clr[dest_port, 0001, $txdesc[3]]
      ; -- defer[3] shadow --
#else
    immed[cls_slen_stage, 0]

    ; get packet valid, packet type and dest port 
    ld_field_w_clr[pkt_valid, 1000, $txdesc[3]]
    ld_field_w_clr[pkt_type,  0100, $txdesc[3]]
    ld_field_w_clr[dest_port, 0001, $txdesc[3]]
#endif

    ctx_number_issuer_alloc(ctx_num)

#ifdef RANDOMIZE_FROM_SEGMENTS
    log_stage_header(ctx_num)
#endif

got_ctx_number#:

    ; get DMA config index
    pcie_pkt_mode_get_dma_config_csr_index(dmacfg_ndx, pkt_type, pkt_valid)

    ; construct dma descriptor
    alu[dmadesc0, length, -, 1]                  		; dmadesc0 = packet length-1
    alu[dmadesc0, dmadesc0, OR, seq_number, <<16]		; dmadesc0 = seq_number << 16 | pktlength - 1 
    alu[dmadesc1, --, B, 0x3, <<30]              		; BP=0, set 
    alu[dmadesc1, dmadesc1, OR, WIRE_SEQUENCER_NUMBER, <<17]	; seqrnum = WIRE_SEQUENCER_NUMBER
    alu[dmadesc1, dmadesc1, OR, dmacfg_ndx, <<8] 		; dma config index
    alu[dmadesc1, dmadesc1, OR, 0x2, <<12]	 		; CPP Token=2 (since NBI byte swaps)

    immed[stage, 0]

frompcie_next_segment#:
    
    alu[transfer_length, length, OR, 0]

#ifndef RANDOMIZE_FROM_SEGMENTS
    alu[--, length, -, four_k]
    ble[got_transfer_length#], defer[3]

      ; -- defer[3] shadow --
      alu[dmadesc1, dmadesc1, OR, ctx_num, <<23] 
      alu[dmadesc1, dmadesc1, OR, dest_port]	 	; QID goes to metadata[1][8;40]
      alu[dmadesc3, --, B, dma_addr_hi]            	; OvRID=0, RID=0
      ; -- defer[3] shadow --

    alu[transfer_length, --, B, four_k]
#else
    alu[--, cls_slen_stage, -, (NUMBER_RANDOM_SEGMENTS-1)]
    bge[got_stage_length#], defer[3]

      ; -- defer[3] shadow --
      alu[dmadesc1, dmadesc1, OR, ctx_num, <<23] 
      alu[dmadesc1, dmadesc1, OR, dest_port]	 	; QID goes to metadata[1][8;40]
      alu[dmadesc3, --, B, dma_addr_hi]            	; OvRID=0, RID=0
      ; -- defer[3] shadow --

    get_stage_length(length, transfer_length)
got_stage_length#:
#endif

got_transfer_length#:

    alu[$dmadesc[0], dmadesc0, OR, 0] 			; packet_length | seq_number << 16
    alu[$dmadesc[1], dmadesc1, OR, stage, <<28]
    alu[$dmadesc[2], --, B, dma_addr_lo]

    alu[tmp, transfer_length, -, 1]
    alu[$dmadesc[3], dmadesc3, OR, tmp, <<20]

#ifdef RANDOMIZE_FROM_SEGMENTS
    log_stage_length(transfer_length)
    alu[cls_slen_stage, cls_slen_stage, +, 1]
#endif

    ; enqueue DMA descriptor
    frompcie_pkt_mode_enqdesc_no_completion($dmadesc, pcie_ep_addr_39_32, pcie_desc_addr_31_00)

    ; update length and offset
    alu[tmp, transfer_length, +, 0]
    alu[length, length, -, tmp]
    ble[packet_transferred#], defer[3]

      ; -- defer[3] shadow --
      alu[dma_addr_lo, dma_addr_lo, +, tmp]
      alu[dma_addr_hi, dma_addr_hi, +carry, 0]

      ; initialize next stage
#ifndef RANDOMIZE_FROM_SEGMENTS
      alu[tmp, length, -, four_k]
#else
      alu[tmp, (NUMBER_RANDOM_SEGMENTS-1), -, cls_slen_stage]
#endif
      ; -- defer[3] shadow --

    ; if length - 4k <= 0, the next DMA will be the last for the packet
    ; in which case stage number must be 2. 
    ; This can be achieved by adding sign bit (bit31) of (length-1)-4k
    ; such that stage = 1 + sign[(length-1)-4k]
    alu[tmp, tmp, -, 1]
    br[frompcie_next_segment#], defer[2]

      ; -- defer[2] shadow --
      alu[tmp, --, B, tmp, >>31]
      alu[stage, 1, +, tmp]
      ; -- defer[2] shadow --

packet_transferred#:

    ; free context number
    ctx_number_issuer_free(ctx_num)

ctx_number_freed#:

#ifdef RANDOMIZE_FROM_SEGMENTS
 #if NUMBER_RANDOM_SEGMENTS == 2
    log_stage_length(0)
 #endif
    log_stage_increment()
#endif

.end
#endm


/**
 * Waits for requested packet to become available from associated work queue.
 * Handles reception of any DMA completion signals (dsgnl) while waiting for packet
 * @sgnl signal asserted when packet is available
 */
#macro pcie_pkt_mode_wait_for_packet(sgnl)
.begin

    .reg tmp

#define _POW2 1
#while _POW2 < MAX_PCIE_DMA_SIGNALS
 #define_eval _POW2 (2 * _POW2)
#endloop

#ifdef _NDX
 #undef _NDX
#endif

wait_top#:
    ; if no outstanding signals, wait only on packet signals
    alu[--, --, B, credits_used]
    ble[wait_no_dsgnl#]

    ; get oldest DMA completion signal index
    alu[tmp, oldest_dsgnl_index, AND, (_POW2-1)]

    ; wait on packet available and, possibly, oldest dsgnl 
#if (MAX_PCIE_DMA_SIGNALS == 1)
    br[jump_wait_dsgnl0#]
#elif (MAX_PCIE_DMA_SIGNALS == 2)
    jump[tmp, dsgnl_jump_table#], targets[jump_wait_dsgnl0#, \
                                          jump_wait_dsgnl1#]
#elif (MAX_PCIE_DMA_SIGNALS == 3)
    jump[tmp, dsgnl_jump_table#], targets[jump_wait_dsgnl0#, \
                                          jump_wait_dsgnl1#, \
                                          jump_wait_dsgnl2#, \
                                          dummy_jump3#]
#elif (MAX_PCIE_DMA_SIGNALS == 4)
    jump[tmp, dsgnl_jump_table#], targets[jump_wait_dsgnl0#, \
                                          jump_wait_dsgnl1#, \
                                          jump_wait_dsgnl2#, \
                                          jump_wait_dsgnl3#]
#elif (MAX_PCIE_DMA_SIGNALS == 5)
    jump[tmp, dsgnl_jump_table#], targets[jump_wait_dsgnl0#, \
                                          jump_wait_dsgnl1#, \
                                          jump_wait_dsgnl2#, \
                                          jump_wait_dsgnl3#, \
                                          jump_wait_dsgnl4#, \
                                          dummy_jump5#,      \
                                          dummy_jump6#,	     \
                                          dummy_jump7#]
#endif

dsgnl_jump_table#:
#define _NDX 0
#while _NDX < MAX_PCIE_DMA_SIGNALS
jump_wait_dsgnl/**/_NDX#:
    br[wait_dsgnl/**/_NDX#]
 #define_eval _NDX (_NDX+1)
#endloop
#while _NDX < _POW2
dummy_jump/**/_NDX#:
    ctx_arb[voluntary], br[dummy_jump/**/_NDX#]
 #define_eval _NDX (_NDX+1)
#endloop

#undef _NDX
#define _NDX 0
#while _NDX < MAX_PCIE_DMA_SIGNALS
wait_dsgnl/**/_NDX#:
    .set_sig dsgnl/**/_NDX
    ctx_arb[sgnl, dsgnl/**/_NDX], any
    br_signal[dsgnl/**/_NDX, dsgnl_asserted#]
    br_signal[sgnl, done#]
    br[wait_top#]
 #define_eval _NDX (_NDX+1)
#endloop

#undef _NDX
#undef _POW2

dsgnl_asserted#:
    pcie_pkt_mode_dsgnl_received()
    br_signal[sgnl, done#]
    br[wait_top#]

wait_no_dsgnl#:
    ctx_arb[sgnl]

done#:

.end
#endm


; Define the following to drop packets at the host-side and return 
; host-side buffers directly to freelist buffer queue immediately
; after ToPcie DMA completes
;#define TO_PCIE_DROP_PACKET

;------------------------------------------------------------------------------
; 
;------------------------------------------------------------------------------

#macro pcie_pkt_mode_to_host()
.begin

    .reg dma_addr_lo
    .reg dma_addr_hi

    .reg split_length

    .reg mtype
    .reg qid

    .reg mask

    .reg tmp

    .reg dest_port

    .reg pkt_type
    .reg pkt_valid

    .reg seqnum

    .reg @terminate_app

    .reg $fldesc[FL_BUF_DESC_SZ]
    .xfer_order $fldesc

    .reg $txdesc[TX_BUF_DESC_SZ]
    .xfer_order $txdesc

    .reg $header[NW_PKT_HEADER_SZ]
    .xfer_order $header

#if defined(ENABLE_PCIE_REORDER) || defined(ENABLE_PCIE_REORDER_VM_QOS)
    .reg $pktdesc
#endif

    .reg $xfer
    .sig psgnl
    .sig bsgnl

    ; -------------------------------------------------------------------------
    ; initialization
    ; -------------------------------------------------------------------------

    immed[@terminate_app,  0]

    ; get active context
    local_csr_rd[ACTIVE_CTX_STS]
    immed[tmp, 0]

    ; initialize LM sections
    pcie_pkt_mode_initialize_to_pcie_dma_modes()
    pcie_pkt_mode_initialize_descriptor_table()

    ; Reset Software DMA state machine
    pcie_pkt_mode_reset_init_state()

process_next_pkt#:

    ; get host buffer
    pcie_pkt_mode_freelist_get($fldesc, bsgnl)

    ; -------------------------------------------------------------------------
    ; wait for packet to arrive from NBI
    ; -------------------------------------------------------------------------

#if !defined(ENABLE_PCIE_REORDER) && !defined(ENABLE_PCIE_REORDER_VM_QOS)
    wintf_get_packet($header, PCIE_EP_ISLAND_NUM, psgnl)
#else
    wintf_get_packet($pktdesc, PCIE_EP_ISLAND_NUM, psgnl)
#endif

    pcie_pkt_mode_wait_for_packet(psgnl)

#if defined(ENABLE_PCIE_REORDER) || defined(ENABLE_PCIE_REORDER_VM_QOS)
    wintf_retrieve_pkt_metadata($pktdesc, $header)
#endif

    ; extract sequence number from packet metadata
    alu[seqnum, --, B, $header[2], >>16]
    immed[mask, 0xfff]
    alu[seqnum, mask, AND, seqnum]

    ; extract destination port, pkt_valid and pkt_type from metadata
    ld_field_w_clr[dest_port, 0001, $header[5]]

    ; wait for free host-side buffer to become available
    ctx_arb[bsgnl], defer[2]

      ; -- defer[2] shadow --
      alu[pkt_valid, 0x01, AND, $header[2], >>3]
      ld_field_w_clr[pkt_type,  0001, $header[4]]
      ; -- defer[2] shadow --

    ; get host-buffer pcie-space address
    ld_field_w_clr[dma_addr_hi, 0001, $fldesc[0]]
    alu[dma_addr_lo, --, b, $fldesc[1]]

    ; PCIe DMA the packet to host/RC
    pcie_pkt_mode_dma_packet_to_host($header, dma_addr_hi, dma_addr_lo)

    ; stash away context information
    pcie_pkt_mode_lm_push(fldesc, header, dest_port, pkt_type, pkt_valid, seqnum)

    ; loop forever
    br[process_next_pkt#]

pcie_pkt_mode_to_host_done#:

    ; context 0 (management thread) resets Work Queues to allow worker threads
    ; to gracefully terminate
    .if (ctx() == 0)

        ; freelist/rx descriptor queue
        ru_emem_ring_setup(FL_PKT_RING/**/PCIE_EP_ISLAND_NUM/**/_QD_BASE, 
                           FL_PKT_RING/**/PCIE_EP_ISLAND_NUM/**/_Q_BASE, 
                           FL_PKT_RING/**/PCIE_EP_ISLAND_NUM/**/_NUM, 
                           FL_PKT_RING_SZ,  
                           FL_PKT_RING/**/PCIE_EP_ISLAND_NUM/**/_LOC)

#if !defined(ENABLE_PCIE_REORDER) && !defined(ENABLE_PCIE_REORDER_VM_QOS)

        ; freelist/rx descriptor queue
        ru_emem_ring_setup(NW_PKT_RING/**/PCIE_EP_ISLAND_NUM/**/_QD_BASE, 
                           NW_PKT_RING/**/PCIE_EP_ISLAND_NUM/**/_Q_BASE, 
                           NW_PKT_RING/**/PCIE_EP_ISLAND_NUM/**/_NUM, 
                           NW_PKT_RING_SZ,  
                           NW_PKT_RING/**/PCIE_EP_ISLAND_NUM/**/_LOC)

#else

        ; freelist/rx descriptor queue
        ru_emem_ring_setup(PCIE_REORDER_RING/**/PCIE_EP_ISLAND_NUM/**/_QD_BASE, 
                           PCIE_REORDER_RING/**/PCIE_EP_ISLAND_NUM/**/_Q_BASE, 
                           PCIE_REORDER_RING/**/PCIE_EP_ISLAND_NUM/**/_NUM, 
                           PCIE_REORDER_RING_SZ,  
                           PCIE_REORDER_RING/**/PCIE_EP_ISLAND_NUM/**/_LOC)

#endif

    .endif

pcie_pkt_mode_to_host_end#:

.end
#endm

;------------------------------------------------------------------------------
; 
;------------------------------------------------------------------------------

#macro pcie_pkt_mode_from_host()
.begin

    .reg event_count

    .reg mode

    .reg pcie_addr_39_32
    .reg pcie_addr_31_00

    .reg length
    .reg tmp

    .reg mask

    .reg @terminate_app

    .reg $txdesc[TX_BUF_DESC_SZ]
    .xfer_order $txdesc

    .reg $dma_signal_only_desc[4]
    .xfer_order $dma_signal_only_desc

    .reg desc1

    .reg $xfer
    .sig sgnl

    ; -------------------------------------------------------------------------
    ; initialization
    ; -------------------------------------------------------------------------

    immed[@terminate_app, 0]

    ; get active context
    local_csr_rd[ACTIVE_CTX_STS]
    immed[tmp, 0]

    ; initialize signal-only PCIe DMA descriptor
    alu[pcie_addr_39_32, --, B, ((PCIE_DMA_RC_BASE_ADDRESS >> 32) & 0x0ff)]
    move(pcie_addr_31_00, (0x0ffffffff & PCIE_DMA_RC_BASE_ADDRESS))

    alu[desc1, --, B, DDC_TO_PCIE_NDX_SIG_ONLY, <<8]

    alu[$dma_signal_only_desc[0], --, B, 0] 
    alu[$dma_signal_only_desc[1], --, B, desc1] 
    alu[$dma_signal_only_desc[2], --, B, pcie_addr_31_00] 
    alu[$dma_signal_only_desc[3], --, B, pcie_addr_39_32] 

    ; initialize LM sections
    pcie_pkt_mode_initialize_to_pcie_dma_modes()
    pcie_pkt_mode_initialize_descriptor_table()

    ; Reset Software DMA state machine
    pcie_pkt_mode_reset_init_state()

#ifdef RANDOMIZE_FROM_SEGMENTS
    log_stage_init()
#endif

frompcie_process_next_pkt#:

    ; -------------------------------------------------------------------------
    ; wait for packet to arrive from RX/TX Buffer Descriptor work queue
    ; -------------------------------------------------------------------------

    pcie_pkt_mode_rxtxq_get($txdesc, sgnl)

    pcie_pkt_mode_wait_for_packet(sgnl)

    ; application termination requested?
    alu[--, --, B, @terminate_app]
    bne[pcie_pkt_mode_from_host_done#]

    ; -------------------------------------------------------------------------
    ; perform multi-stage Packet Mode PCIe DMA from host buffer
    ; -------------------------------------------------------------------------

    frompcie_pkt_mode_issue_dma($txdesc)

    .if (@credits_avail == 1)


        ; Since completion event/signal cannot be configured for Packet Mode
        ; DMA, a signal-only DMA is used to indicate the completion of DMA of 
        ; packet
        pcie_pkt_mode_enqueue_pcie_descriptor($dma_signal_only_desc, desc1, pcie_ep_addr_39_32, pcie_desc_addr_31_00)

    .endif

    ; push freelist descriptor to LM
    frompcie_pkt_mode_lm_push($txdesc)

    ; loop forever
    br[frompcie_process_next_pkt#]

pcie_pkt_mode_from_host_done#:

    ; context 0 (management thread) resets Work Queue to allow worker threads
    ; to gracefully terminate
    .if (ctx() == 0)

        ; rx/tx descriptor queue
        ru_emem_ring_setup(TX_PKT_RING/**/PCIE_EP_ISLAND_NUM/**/_QD_BASE, 
                           TX_PKT_RING/**/PCIE_EP_ISLAND_NUM/**/_Q_BASE, 
                           TX_PKT_RING/**/PCIE_EP_ISLAND_NUM/**/_NUM, 
                           TX_PKT_RING_SZ,  
                           TX_PKT_RING/**/PCIE_EP_ISLAND_NUM/**/_LOC)

    .endif

.end
#endm

;------------------------------------------------------------------------------
; NFP Root Complex Application
;------------------------------------------------------------------------------

#macro pcie_pkt_mode_nfp_rc_init()
.begin
    
    .reg mask
    .reg len
    .reg bar
    .reg tmp
    .reg count
    .reg value
    .reg cpp_addr_39_32
    .reg cpp_addr_31_00
    .reg pcie_addr_39_32
    .reg pcie_addr_31_00
    .reg $fldesc[FL_BUF_DESC_SZ]
    .xfer_order $fldesc
    .sig sgnl

    ; clear RC error fields
    clear_rc_error_fields(cl_num)

    ; disable the PCIe target expansion BAR selection of CPP master cmd bus preference
    disable_rc_cpp_bus_preference(cl_num)

    ; disable ordering checks and RX NP Starvation Prevention
    #define _PCIEI 4
    #while _PCIEI < 8
      .if (_PCIEI == cl_num)
        configure_debug_mux_control_csr(_PCIEI, PCIE_DMA_DOC, PCIE_DMA_DRXNPSP)
      .endif
      #define_eval _PCIEI (_PCIEI+1)
    #endloop
    #undef _PCIEI

    ; configure DWRR in Shared Master Bus Arbitration
    #define _PCIEI 4
    #while _PCIEI < 8
      .if (_PCIEI == cl_num)
        configure_dwrr(_PCIEI, PCIE_DWRR_CREDITS)
      .endif
      #define_eval _PCIEI (_PCIEI+1)
    #endloop
    #undef _PCIEI

    ; configure RC BAR
    configure_rc_bar(cl_num, 0x0, PCIE_DMA_RC_BASE_ADDRESS)    
    configure_rc_aperture(cl_num, PCIE_DMA_RC_APERTURE_BITS)

    ; configure link
    set_mps_rc(cl_num,  PCIE_DMA_MPS)
    set_mrrs_rc(cl_num, PCIE_DMA_MRRS)
    set_rcb_rc(cl_num,  PCIE_DMA_RCB)

    ; enable EP accesses
    enable_pcie_ep(cl_num)

    ; configure Pcie2Cpp BAR0 for MapType=1 (Bulk) and points to
    ; base of host buffer
    ; NOTE: In bulk mode, CPP Address[39:(aperture_sz - 3)] is provided by Pcie2CppBAR[20:(barsz-22)]
    ;                     CPP Address[(aperture_sz - 4)]    is provided by PCIe Address[(aperture_sz-4):0]
    alu[mask, --, B, 1, <<21]
    alu[mask, mask, -, 1]

    alu[tmp, --, B, 1, <<(PCIE_DMA_RC_APERTURE_BITS-22)]
    alu[tmp, tmp, -, 1]
    alu[mask, mask, AND~, tmp]

    move(tmp, (0x0ffffffff & (HOST_BUFFER_BASE >> (39-20))))
    alu[value, mask, AND, tmp]

    alu[value, value, OR, 1, <<29]	; bulk mapping
    alu[value, value, OR, 1, <<27]	; 64-bit CPP increments
    alu[value, value, OR, 7, <<23]	; memory target
    alu[value, value, OR, 0, <<21]	; token 0
    configure_pcie_to_cpp_bar(cl_num, 0, 0, value)

    ; P2C BAR does not provide for CPP addr[aperture_sz-4:0]
    ; NOTE assume aperture size - 4 is less than 32 bits
    ;      for calculations below to be valid
    alu[mask, --, B, 1, <<(PCIE_DMA_RC_APERTURE_BITS-3)]
    alu[mask, mask, -, 1]

    move(pcie_addr_31_00, (0x0ffffffff & HOST_BUFFER_BASE))
    alu[pcie_addr_31_00, pcie_addr_31_00, AND, mask]

    ; PCIe access is through RC BAR0
    alu[pcie_addr_39_32, --, B, ((PCIE_DMA_RC_BASE_ADDRESS >> 32) & 0x0ff)]
    move(tmp, (0x0ffffffff & PCIE_DMA_RC_BASE_ADDRESS))
    alu[pcie_addr_31_00, pcie_addr_31_00, OR, tmp]

    ; populate freelist work queue with DMA (PCIe space) pointers
    move(len, HOST_BUFFER_LEN)
    move(count, HOST_BUFFER_CNT)

populate#:

    ; add to work queue
    alu[$fldesc[0], --, B, pcie_addr_39_32]
    alu[$fldesc[1], --, B, pcie_addr_31_00]
    pcie_pkt_mode_freelist_put($fldesc, sgnl)
    ctx_arb[sgnl]

    ; increment buffer address
    alu[pcie_addr_31_00, pcie_addr_31_00, +, len]
    alu[pcie_addr_39_32, pcie_addr_39_32, +carry, 0]

    ; any more
    alu[count, count, -, 1]
    bne[populate#]

.end
#endm

#macro configure_ia_host_buffers()
.begin

    .reg len
    .reg mask
    .reg count
    .reg tmp
    .reg pcie_addr_39_32
    .reg pcie_addr_31_00
    .reg $fldesc[FL_BUF_DESC_SZ]
    .xfer_order $fldesc

    ; PCIe access is direct
    alu[pcie_addr_39_32, --, B, ((HOST_BUFFER_BASE >> 32) & 0x0ff)]
    move(pcie_addr_31_00, (0x0ffffffff & HOST_BUFFER_BASE))

    ; populate freelist work queue with DMA (PCIe space) pointers
    move(len, HOST_BUFFER_LEN)
    move(count, HOST_BUFFER_CNT)

populate#:

    ; add to work queue
    alu[$fldesc[0], --, B, pcie_addr_39_32]
    alu[$fldesc[1], --, B, pcie_addr_31_00]
    pcie_pkt_mode_freelist_put($fldesc, sgnl)
    ctx_arb[sgnl]

    ; increment buffer address
    alu[pcie_addr_31_00, pcie_addr_31_00, +, len]
    alu[pcie_addr_39_32, pcie_addr_39_32, +carry, 0]

    ; any more
    alu[count, count, -, 1]
    bne[populate#]

.end
#endm

;------------------------------------------------------------------------------
; Application initialization 
;------------------------------------------------------------------------------

/**
 * Global PCIe initialization
 * performed by single context of single ME
 */
#macro pcie_pkt_mode_global_init()
.begin

    .reg addr_hi
    .reg addr_lo
    .reg tmp
    .reg value
    .reg $xfer
    .sig sgnl

    ; initialize PCIe

    ; configure PCIe link (if root complex is NFP)
#ifdef PCIE_DMA_NFP_ROOT_COMPLEX
    set_mps_ep(PCIE_EP_ISLAND_NUM,  PCIE_DMA_MPS)
    set_mrrs_ep(PCIE_EP_ISLAND_NUM, PCIE_DMA_MRRS)
    set_rcb_ep(PCIE_EP_ISLAND_NUM,  PCIE_DMA_RCB)
#endif

#if defined(PCIE_MASTER)
    ; disable ordering checks and RX NP Starvation Prevention
    configure_debug_mux_control_csr(PCIE_EP_ISLAND_NUM, PCIE_DMA_DOC, PCIE_DMA_DRXNPSP)
#endif

#if defined(PCIE_MASTER)
    ; configure Shared Master Bus Arbitration DWRR
    configure_dwrr(PCIE_EP_ISLAND_NUM, PCIE_DWRR_CREDITS)
#endif

    ; initialize PCIe PMHeader CSRs
    #define _MTYPE   PCIE_EP_ISLAND_NUM
    #define _NFP     0
    #define _VALID   1
    #define _INVALID 0

    pcie_pkt_mode_configure_pm_header_data(PCIE_EP_ISLAND_NUM, PMH_NDX_INVALID_IPV4_EXCEPTION_PKT, _INVALID, _MTYPE, _NFP, IPV4_EXCEPTION_PKT)
    pcie_pkt_mode_configure_pm_header_data(PCIE_EP_ISLAND_NUM, PMH_NDX_INVALID_IPV4_PKT,           _INVALID, _MTYPE, _NFP, IPV4_PKT)
    pcie_pkt_mode_configure_pm_header_data(PCIE_EP_ISLAND_NUM, PMH_NDX_INVALID_ARP_PKT, 	   _INVALID, _MTYPE, _NFP, ARP_PKT)
    pcie_pkt_mode_configure_pm_header_data(PCIE_EP_ISLAND_NUM, PMH_NDX_INVALID_NON_IPV4_PKT, 	   _INVALID, _MTYPE, _NFP, NON_IPV4_PKT)
    pcie_pkt_mode_configure_pm_header_data(PCIE_EP_ISLAND_NUM, PMH_NDX_INVALID_ERROR_PKT, 	   _INVALID, _MTYPE, _NFP, ERROR_PKT)

    pcie_pkt_mode_configure_pm_header_data(PCIE_EP_ISLAND_NUM, PMH_NDX_VALID_IPV4_EXCEPTION_PKT,     _VALID, _MTYPE, _NFP, IPV4_EXCEPTION_PKT)
    pcie_pkt_mode_configure_pm_header_data(PCIE_EP_ISLAND_NUM, PMH_NDX_VALID_IPV4_PKT,               _VALID, _MTYPE, _NFP, IPV4_PKT)
    pcie_pkt_mode_configure_pm_header_data(PCIE_EP_ISLAND_NUM, PMH_NDX_VALID_ARP_PKT, 	             _VALID, _MTYPE, _NFP, ARP_PKT)
    pcie_pkt_mode_configure_pm_header_data(PCIE_EP_ISLAND_NUM, PMH_NDX_VALID_NON_IPV4_PKT, 	     _VALID, _MTYPE, _NFP, NON_IPV4_PKT)
    pcie_pkt_mode_configure_pm_header_data(PCIE_EP_ISLAND_NUM, PMH_NDX_VALID_ERROR_PKT, 	     _VALID, _MTYPE, _NFP, ERROR_PKT)

    #undef _INVALID
    #undef _VALID
    #undef _NFP
    #undef _MTYPE

    ; initialize PCIe DMA Descriptor Config CSRs
    alu[addr_hi, --, B, PCIE_EP_ISLAND_NUM, <<30]

    ; configure all DMADescrConfig CSRs for Target=7 (Memory), 64-bit accesses 
    #define _NDX 0
    #while (_NDX < 8)

      immed32[addr_lo, (NFP_PCIE_DMA+NFP_PCIE_DMA_CFG/**/_NDX/**/)]
      immed[tmp, 0x17] 			; CppTarget=7, Target64bit=1
      alu[value, --, B, tmp]
      alu[$xfer, value, OR, tmp, <<16]	; duplicate for upper word
      pcie[write_pci, $xfer, addr_hi, <<8, addr_lo, 1], sig_done[sgnl]
      ctx_arb[sgnl]

    #define_eval _NDX (_NDX+1)
    #endloop

    ; DMADescrConfig6/7 (6=normal, 7=free CTM buffer)
    immed32[addr_lo, (NFP_PCIE_DMA+NFP_PCIE_DMA_CFG3)]
    immed[tmp, 0x17] 			; CppTarget=7, Target64bit=1
    alu[value, --, B, tmp]
    alu[value, value, OR, tmp, <<16]	; copy to odd
    alu[$xfer, value, OR, 1, <<29]	; DMADescrConfig7 frees CTM packet
    pcie[write_pci, $xfer, addr_hi, <<8, addr_lo, 1], sig_done[sgnl]
    ctx_arb[sgnl]

    ; DMADescrConfig14/15 (14=sig only, 15=normal)
    immed32[addr_lo, (NFP_PCIE_DMA+NFP_PCIE_DMA_CFG7)]
    immed[tmp, 0x17] 			; CppTarget=7, Target64bit=1
    alu[value, --, B, tmp]
    alu[value, value, OR, tmp, <<16]
    alu[$xfer, value, OR, 1, <<12]	; DMADescrConfig14 is SignalOnly
    pcie[write_pci, $xfer, addr_hi, <<8, addr_lo, 1], sig_done[sgnl]
    ctx_arb[sgnl]

    ; initialize the NbiTmPcieCmdOutCfg CSR if pcie reorder/reorder-with-qos 
    ; enabled
#if defined(ENABLE_PCIE_REORDER) || defined(ENABLE_PCIE_REORDER_VM_QOS)

    #define QADD_WORK_IMM_ACTION 19
    #define QADD_WORK_IMM_TOKEN   3
    #define QADD_WORK_IMM_TARGET  7 ; memory unit

    ; format:
    ;  [31:30] - not used
    ;  [29:20] - ring number
    ;  [19:19] - output command port 
    ;  [18:11] - destination island for PCIe command out
    ;  [10: 9] - token
    ;  [ 8: 4] - action
    ;  [ 3: 0] - target
    alu[value, --, B, PCIE_REORDER_RING/**/PCIE_EP_ISLAND_NUM/**/_NUM, <<20]
    alu[value, value, OR, NBI_NUMBER, <<19]
    alu[value, value, OR, PCIE_REORDER_RING/**/PCIE_EP_ISLAND_NUM/**/_ISLAND, <<11]
    alu[value, value, OR, QADD_WORK_IMM_TOKEN, <<9]
    alu[value, value, OR, QADD_WORK_IMM_ACTION, <<4]
    alu[$xfer, value, OR, QADD_WORK_IMM_TARGET]
    
    #if defined(IS_DUAL_ISLAND)
     #if defined(PCIE_MASTER)
      #define _PCIE_NBI_TM_CMD_CFG_NDX PCIE_NBI_TM_CMD_CFG_MASTER_NDX
     #else
      #define _PCIE_NBI_TM_CMD_CFG_NDX PCIE_NBI_TM_CMD_CFG_SLAVE_NDX
     #endif
    #else
     #define _PCIE_NBI_TM_CMD_CFG_NDX PCIE_NBI_TM_CMD_CFG_NDX
    #endif

    ; NbiTmPcieCmdOutCfg0 starts at address 0x14091c
    move(addr_lo, (NFP_XPB_ISLAND(8+NBI_NUMBER) + NFP_NBI_TMX + NFP_NBI_TMX_CSR_NBITMPCIECMDOUTCFG(_PCIE_NBI_TM_CMD_CFG_NDX)))
    ct[xpb_write, $xfer, addr_lo, 0, 1], sig_done[sgnl]
    ctx_arb[sgnl]

#endif

#if defined (ENABLE_PCIE_REORDER_VM_QOS)

    #if defined(IS_DUAL_ISLAND)
     #if defined(PCIE_MASTER)
      #define _TM_DROP_PRECEDENCE_NDX TM_DROP_PRECEDENCE_MASTER_NDX
     #else
      #define _TM_DROP_PRECEDENCE_NDX TM_DROP_PRECEDENCE_SLAVE_NDX
     #endif
    #else
     #define _TM_DROP_PRECEDENCE_NDX TM_DROP_PRECEDENCE_NDX
    #endif

    ; Configure the DropProfile CSR being referenced
    move(addr_lo, (NFP_XPB_ISLAND(8+NBI_NUMBER) + NFP_NBI_TMX + NFP_NBI_TMX_CSR_DROP_PROFILE(_TM_DROP_PRECEDENCE_NDX)))
    immed[$xfer, 0] ; Configure for no drops
    ct[xpb_write, $xfer, addr_lo, 0, 1], sig_done[sgnl]
    ctx_arb[sgnl]

#endif

    ; initialize context number issuer
    ctx_number_issuer_init()

#if defined(PCIE_DMA_IA_ROOT_COMPLEX)
    configure_ia_host_buffers()
#endif

.end
#endm

/**
 * Local initialization performed by every PCIe ME
 */
#macro pcie_pkt_mode_local_init()
.begin

    .reg tmp
    .reg dma_status_addr
    .reg $xfer
    .reg $dma_queue_status[2]
    .xfer_order $dma_queue_status
    .sig sgnl

    ; useful constants
    alu[four_k, --, B, 1, <<12]
    move(minus_one, -1)

    ; get address MSB for accessing PCIe island
    alu_shf[pcie_ep_addr_39_32, --, B, PCIE_EP_ISLAND_NUM, <<30]

    ; get DMA descriptor queue address for given queue
    pcie_dma_enqueue_addr(pcie_desc_addr_31_00, DMA_DESC_QUEUE_INDEX)

    ; single context initialization
    .if (ctx() == 0)

        ; store CTM offset
        move(@ctm_offset, CTM_OFFSET)

    .endif

.end
#endm

;------------------------------------------------------------------------------
; Application entry point
;------------------------------------------------------------------------------

application#:

.begin
    
    .reg expected
    .reg cmd

    ; consume outstanding signals
    .if (signal(stop_signal))
    .endif

    .if (ctx() == 0)

        local_csr_wr[mailbox0, 0]
        local_csr_wr[mailbox1, 0]
        local_csr_wr[mailbox2, 0]
        local_csr_wr[mailbox3, 0]

        immed[@dma_count, 0]
        immed[@pkt_count, 0]

    .endif

    local_csr_rd[ACTIVE_CTX_STS]
    immed[me_num, 0]

    ; get island and ME numbers
    alu[cl_num, 0x3f, AND, me_num, >>25]
    alu[me_num, 0x0f, AND, me_num, >>3]

    ; normalize ME number
    alu[me_num, me_num, -, 4]

    ; require context-relative local memory for LM0 ... LM3
    ; disable LM ECC
    ; PRN updated only upon reads (not every clock cyle)
    local_csr_rd[CTX_ENABLES]
    immed[cmd, 0]
    alu[cmd, cmd, AND~, 0x23, <<16]
    alu[cmd, cmd, AND~, 0x1, <<30]
    local_csr_wr[CTX_ENABLES, cmd]

    ; get island numbers hosting each work queue
    pcie_pkt_mode_get_addr_39_32(FL_PKT_RING/**/PCIE_EP_ISLAND_NUM/**/_Q_BASE, fl_addr_39_32)
    pcie_pkt_mode_get_addr_39_32(TX_PKT_RING/**/PCIE_EP_ISLAND_NUM/**/_Q_BASE, tx_addr_39_32)

    pcie_pkt_mode_get_addr_39_32(CTX_NUMBER_RING/**/PCIE_EP_ISLAND_NUM/**/_Q_BASE, ctx_addr_39_32)

    ; wait for command to start
    .if (ctx() == 0)
        immed[expected, PCIE_INIT_START]
wait_for_start_cmd#:
        local_csr_rd[mailbox0]
        immed[cmd, 0]
        alu[--, cmd, -, expected]
        bne[wait_for_start_cmd#]
    .endif

    ; global initialization (performed by single ME)
#define CTM_LENGTH (256*1024)
#if defined(PCIE_DMA_DIR_TO_HOST)
 #if PCIE_PKT_MODE_ME_INDEX == 0

    .if (ctx() == 0)
        pcie_pkt_mode_global_init()
    .endif

    pcie_clear_sram(PCIE_EP_ISLAND_NUM)
    pcie_clear_memory(PCIE_EP_ISLAND_NUM, CTM_LENGTH)

 #endif
#elif defined(PCIE_DMA_RC_MODE)

    .if (ctx() == 0)
        pcie_pkt_mode_nfp_rc_init()
    .endif

    ; The RC_MODE ME always runs on PCIe island
    pcie_clear_sram(cl_num)
    pcie_clear_memory(cl_num, CTM_LENGTH)

 #if !defined(USE_EMEM_HOST_BUFFERS)
    ; clear host buffer memory
    pcie_pkt_mode_clear_host_buffers(HOST_BUFFER_BASE, HOST_BUFFER_SZ)
 #endif
#endif

    ; local initialization (performed by all MEs)
#if defined(PCIE_DMA_DIR_TO_HOST) || defined(PCIE_DMA_DIR_FROM_HOST)
    pcie_pkt_mode_local_init()
#endif

    ; synchronization point
    pcie_pkt_mode_thread_synch()
    pcie_pkt_mode_thread_synch()


    ; indicate initialization has completed
    .if (ctx() == 0)
#if defined(REAL_HW)
        ; when running on real hardware, add delay before reply
        move(cmd, 0x10000)
        .while cmd
            alu[cmd, cmd, -, 1]
            nop
        .endw
#endif
        immed[cmd, PCIE_INIT_COMPLETE]
        local_csr_wr[mailbox0, cmd]
    .endif

    // start application
#if defined(PCIE_DMA_DIR_TO_HOST)
    pcie_pkt_mode_to_host()
#elif defined(PCIE_DMA_DIR_FROM_HOST)
    pcie_pkt_mode_from_host()
#endif

    ; synchronization point
    pcie_pkt_mode_thread_synch()
    pcie_pkt_mode_thread_synch()

    ; done
    ctx_arb[bpt]
.end

#endif __PCIE_PKT_MODE_UC__

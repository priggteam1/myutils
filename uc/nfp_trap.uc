/*
 * Copyright (C) 2016 Netronome Systems, Inc. All rights reserved.
 *
 * File: nfp_trap.uc
 * Description: Tests NFP PCIe transaction trapping capability.
 */

#include <stdmac.uc>
#include <nfp_chipres.h>
#include <ov.uc>
#include <bitfields.uc>
#include <aggregate.uc>
#include "nfp_trap.h"


/***************dbg_jnl.uc***************/
#if __nfp_has_island("emem0")
    #define DBG_JNL_Q_EMEM_TYPE emem0_cache_upper
#elif __nfp_has_island("emem1")
    #define DBG_JNL_Q_EMEM_TYPE emem1_cache_upper
#elif __nfp_has_island("emem2")
    #define DBG_JNL_Q_EMEM_TYPE emem2_cache_upper
#else
    #error "EMEM required for Queue Engines."
#endif

#define DBG_JNL_Q_SZ (4096)

#macro dbg_jnl_alloc()
#if DBG_JNL_EN == 1
    // Allocate queue index
    #if __nfp_has_island("emem0")
        .alloc_resource DBG_JNL_Q_IDX emem0_queues+10 global 1
    #elif __nfp_has_island("emem1")
        .alloc_resource DBG_JNL_Q_IDX emem1_queues global 1
    #elif __nfp_has_island("emem2")
        .alloc_resource DBG_JNL_Q_IDX emem2_queues global 1
    #else
        #error "EMEM required for Queue Engines."
    #endif // __nfp_has_island

    .alloc_mem DBG_JNL_Q_BASE DBG_JNL_Q_EMEM_TYPE global DBG_JNL_Q_SZ DBG_JNL_Q_SZ
#endif
#endm // dbg_jnl_alloc

#macro dbg_jnl_init()
#if DBG_JNL_EN == 1
    .init_mu_ring DBG_JNL_Q_IDX DBG_JNL_Q_BASE 0
#endif
#endm // dbg_jnl_init

#macro dbg_jnl_log(in_data, IN_LW_CNT)
#if DBG_JNL_EN == 1
.begin
    .reg dbg_addr_hi
    .reg dbg_num
    .reg $dbg_data[IN_LW_CNT]
    .xfer_order $dbg_data
    .sig dbg_sig

    alu[dbg_addr_hi, --, B, (DBG_JNL_Q_BASE >> 32), <<24]
    immed[dbg_num, DBG_JNL_Q_IDX]

    #if IN_LW_CNT > 1
        aggregate_copy($dbg_data, in_data, IN_LW_CNT)
    #else
        move($dbg_data[0], in_data)
    #endif

    mem[journal, $dbg_data[0], dbg_addr_hi, <<8, dbg_num, IN_LW_CNT], ctx_swap[dbg_sig]
.end
#endif
#endm // dbg_jnl_init
/***************dbg_jnl.uc***************/


/* EAS_pcie_x8: 2.2.1.1.1.15 */
#define PCIE_TRAP_TIMEOUT_RANGE_1_US    0x0
#define PCIE_TRAP_TIMEOUT_RANGE_8_US    0x1
#define PCIE_TRAP_TIMEOUT_RANGE_128_US  0x2
#define PCIE_TRAP_TIMEOUT_mask          0x1f

#if __nfp_has_island("emem0")
    #define NFP_TRAP_Q_EMEM_TYPE emem0_cache_upper
    #define NFP_TRAP_CFG_EMEM_TYPE emem0
#elif __nfp_has_island("emem1")
    #define NFP_TRAP_Q_EMEM_TYPE emem1_cache_upper
    #define NFP_TRAP_CFG_EMEM_TYPE emem1
#elif __nfp_has_island("emem2")
    #define NFP_TRAP_Q_EMEM_TYPE emem2_cache_upper
    #define NFP_TRAP_CFG_EMEM_TYPE emem2
#else
    #error "EMEM required for Queue Engines."
#endif

/* EAS_pcie_x8: 2.1.1.1.8 Trap Mode PCIe Target Address BAR Mapping (MapType=6) */
/* Size of each trapped PCIe transaction work queue entry */
#define NFP_TRAP_Q_ENTRY_SZ         (64)
#define_eval NFP_TRAP_Q_ENTRY_SZ_LW (NFP_TRAP_Q_ENTRY_SZ>>2)
/* Number of work queue entries - i.e. no. of pending PCIe transactions */
#define NFP_TRAP_Q_NUM              (512)
/* Total size of work queue in emem */
#define NFP_TRAP_Q_SZ               (NFP_TRAP_Q_ENTRY_SZ * NFP_TRAP_Q_NUM)


#macro nfp_trap_alloc()

    // Allocate queue index
    #if __nfp_has_island("emem0")
        .alloc_resource NFP_TRAP_Q_IDX emem0_queues+20 global 1
    #elif __nfp_has_island("emem1")
        .alloc_resource NFP_TRAP_Q_IDX emem1_queues global 1
    #elif __nfp_has_island("emem2")
        .alloc_resource NFP_TRAP_Q_IDX emem2_queues global 1
    #else
        #error "EMEM required for Queue Engines."
    #endif // __nfp_has_island

    .alloc_mem NFP_TRAP_Q_BASE NFP_TRAP_Q_EMEM_TYPE global NFP_TRAP_Q_SZ NFP_TRAP_Q_SZ
    .alloc_mem NFP_TRAP_CFG NFP_TRAP_CFG_EMEM_TYPE global NFP_TRAP_CFG_SZ 128

#endm // nfp_trap_alloc


/* Initialise registers and mem for PF0
 * @todo init_pf(PF_NUM), init_vf(VF_NUM) */
#macro nfp_trap_init(PF_NUM)

    /* UG_nfp6000_nfas: 2.8.21 Init-CSR */
    .init_csr xpb:i4.PcieCtrlIslandXpbTopL0.Pcie_Pf/**/PF_NUM/**/_Reg.i_pcie_base.i_vendor_id_device_id.VID  NFP_TRAP_VID
    .init_csr xpb:i4.PcieCtrlIslandXpbTopL0.Pcie_Pf/**/PF_NUM/**/_Reg.i_pcie_base.i_vendor_id_device_id.DID NFP_TRAP_DID

//    /* Set 64-bit BAR apertures to 1 MB and disable BAR4-5
//     * EAS_pcie_ctrl_island: 2.2.1.5.39.43 pf_BAR_config_0_reg */
//    /* BAR0-1 */
//    .init_csr xpb:i4.PcieCtrlIslandXpbTopL0.Pcie_Lm_Reg.i_regf_lm_pcie_base.i_pf_/**/PF_NUM/**/_BAR_config_0_reg.BAR0C 0x6
//    .init_csr xpb:i4.PcieCtrlIslandXpbTopL0.Pcie_Lm_Reg.i_regf_lm_pcie_base.i_pf_/**/PF_NUM/**/_BAR_config_0_reg.BAR0A 0xd
//    .init_csr xpb:i4.PcieCtrlIslandXpbTopL0.Pcie_Lm_Reg.i_regf_lm_pcie_base.i_pf_/**/PF_NUM/**/_BAR_config_0_reg.BAR1C 0x0
//    .init_csr xpb:i4.PcieCtrlIslandXpbTopL0.Pcie_Lm_Reg.i_regf_lm_pcie_base.i_pf_/**/PF_NUM/**/_BAR_config_0_reg.BAR1A 0x0
//    /* BAR2-3 */
//    .init_csr xpb:i4.PcieCtrlIslandXpbTopL0.Pcie_Lm_Reg.i_regf_lm_pcie_base.i_pf_/**/PF_NUM/**/_BAR_config_0_reg.BAR2C 0x6
//    .init_csr xpb:i4.PcieCtrlIslandXpbTopL0.Pcie_Lm_Reg.i_regf_lm_pcie_base.i_pf_/**/PF_NUM/**/_BAR_config_0_reg.BAR2A 0xd
//    .init_csr xpb:i4.PcieCtrlIslandXpbTopL0.Pcie_Lm_Reg.i_regf_lm_pcie_base.i_pf_/**/PF_NUM/**/_BAR_config_0_reg.BAR3C 0x0
//    .init_csr xpb:i4.PcieCtrlIslandXpbTopL0.Pcie_Lm_Reg.i_regf_lm_pcie_base.i_pf_/**/PF_NUM/**/_BAR_config_0_reg.BAR3A 0x0
//    /* BAR4-5 */
//    .init_csr xpb:i4.PcieCtrlIslandXpbTopL0.Pcie_Lm_Reg.i_regf_lm_pcie_base.i_pf_/**/PF_NUM/**/_BAR_config_1_reg.BAR4C 0x0
//    .init_csr xpb:i4.PcieCtrlIslandXpbTopL0.Pcie_Lm_Reg.i_regf_lm_pcie_base.i_pf_/**/PF_NUM/**/_BAR_config_1_reg.BAR4A 0x0
//    .init_csr xpb:i4.PcieCtrlIslandXpbTopL0.Pcie_Lm_Reg.i_regf_lm_pcie_base.i_pf_/**/PF_NUM/**/_BAR_config_1_reg.BAR5C 0x0
//    .init_csr xpb:i4.PcieCtrlIslandXpbTopL0.Pcie_Lm_Reg.i_regf_lm_pcie_base.i_pf_/**/PF_NUM/**/_BAR_config_1_reg.BAR5A 0x0

    /* MSI-X should use BAR2 >> PCI mem space */
    .init_csr xpb:i4.PcieCtrlIslandXpbTopL0.Pcie_Pf/**/PF_NUM/**/_Reg.i_MSIX_cap_struct.i_msix_tbl_offset.BARI NFP_TRAP_MSIX_BAR
    .init_csr xpb:i4.PcieCtrlIslandXpbTopL0.Pcie_Pf/**/PF_NUM/**/_Reg.i_MSIX_cap_struct.i_msix_pending_intrpt.BARI1 NFP_TRAP_MSIX_BAR

    /* Map Bar 2 to 0x8000000 of emem0 */
    .init_csr pcie:i4.PcieInternalTargets.ExpansionBARConfiguration.PcieToCppExpansionBar_PF/**/PF_NUM/**/.PCIeToCppExpansionBAR1_0 0x2b840100
    /* Enable MSI-X */
    //.init_csr xpb:i4.PcieCtrlIslandXpbTopL0.Pcie_Pf/**/PF_NUM/**/_Reg.i_MSIX_cap_struct.i_msix_ctrl.MSIXE 0x1

    /* Set trap timeout
     * EAS_pcie_x8: 2.2.1.1.1.15 PcieTrapTypeTimeout */
    .init_csr xpb:i4.PcieXpb.PcieComponentCfgXpb.PcieTrapTypeTimeout.TrapTimeoutRange PCIE_TRAP_TIMEOUT_RANGE_128_US
    .init_csr xpb:i4.PcieXpb.PcieComponentCfgXpb.PcieTrapTypeTimeout.TrapTimeout (0x1f & PCIE_TRAP_TIMEOUT_mask)

    /* Init PCIe trap work queue */
    .init_mu_ring NFP_TRAP_Q_IDX NFP_TRAP_Q_BASE 0

    /* Map Bar 0 to qadd_work(NFP_VIRTIO_Q) */
    // .init_csr pcie:i4.PcieInternalTargets.ExpansionBARConfiguration.PcieToCppExpansionBar_PF/**/PF_NUM/**/.PCIeToCppExpansionBAR0_0 0x2b840000
    /* EAS_pcie_x8: Table 2.12. PCIe Target Trap Mode Mapping (MapType=6)
     * EAS_pcie_ctrl_island: 2.2.2.2.20.1 PcieToCppExpansionBar */
    .init_csr pcie:i4.PcieInternalTargets.ExpansionBARConfiguration.PcieToCppExpansionBar_PF/**/PF_NUM/**/.PCIeToCppExpansionBAR0_0.MapType 0x6
    .init_csr pcie:i4.PcieInternalTargets.ExpansionBARConfiguration.PcieToCppExpansionBar_PF/**/PF_NUM/**/.PCIeToCppExpansionBAR0_0.LengthSelect 0x2
    /* DB_nfp-6xxx0-xc: Table 3.5 CPP Target IDs */
    .init_csr pcie:i4.PcieInternalTargets.ExpansionBARConfiguration.PcieToCppExpansionBar_PF/**/PF_NUM/**/.PCIeToCppExpansionBAR0_0.Target_BaseAddress 0x7
    /* DB_nfp-6xxx0-xc: Table 9.8. MU External CPP command map: qadd_work */
    .init_csr pcie:i4.PcieInternalTargets.ExpansionBARConfiguration.PcieToCppExpansionBar_PF/**/PF_NUM/**/.PCIeToCppExpansionBAR0_0.Token_BaseAddress 0x2
    /* DB_nfp-6xxx0-xc: Table 9.8. MU External CPP command map: qadd_work */
    .init_csr pcie:i4.PcieInternalTargets.ExpansionBARConfiguration.PcieToCppExpansionBar_PF/**/PF_NUM/**/.PCIeToCppExpansionBAR0_0.Action_BaseAddress 0x13
    /* EAS_pcie_x8: Table 2.12. PCIe Target Trap Mode Mapping (MapType=6) */
    .init_csr pcie:i4.PcieInternalTargets.ExpansionBARConfiguration.PcieToCppExpansionBar_PF/**/PF_NUM/**/.PCIeToCppExpansionBAR0_0.BaseAddress ((((NFP_TRAP_Q_BASE>>32)<<8)&0xff00) | NFP_TRAP_Q_IDX)

    /* DRV_TRAP_CFG
     *       3                   2                   1
     *     1 0 9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0
     *    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     * 00 | byte0         | byte1         | byte2         | byte3         |
     *    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     * 04 | word0                         | word1                         |
     *    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     * 08 | dword0                                                        |
     *    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     * 0c | dword1                                                        |
     *    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     */
    /* NFP_TRAP_CFG
     *       3                   2                   1
     *     1 0 9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0
     *    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     * 00 |                                               | byte0         |
     *    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     * 04 |                               | byte1         |               |
     *    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     * 08 |               | byte2         |                               |
     *    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     * 0c | byte3         |                                               |
     *    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     * 10 |                               | word0                         |
     *    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     * 14 | word1                         |                               |
     *    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     * 18 | dword0                                                        |
     *    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     * 1c | dword1                                                        |
     *    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     */
    .init NFP_TRAP_CFG RD_BYTES_EXP_0 (RD_BYTES_EXP_1<<8) (RD_BYTES_EXP_2<<16) (RD_BYTES_EXP_3<<24) RD_WORDS_EXP_0 (RD_WORDS_EXP_1<<16) RD_DWORDS_EXP_0 RD_DWORDS_EXP_1

#endm // nfp_trap_init

#macro nfp_trap_source(out_xfr_trap_desc)
.begin
    .reg q_addr_hi
    .reg q_num
    .sig q_sig

    alu[q_addr_hi, --, B, (NFP_TRAP_Q_BASE >> 32), <<24]
    immed[q_num, NFP_TRAP_Q_IDX]

    ov_start(OV_LENGTH)
    ov_set(OV_LENGTH, NFP_TRAP_Q_ENTRY_SZ_LW, OVF_SUBTRACT_ONE)
    ov_use()

    mem[qadd_thread, out_xfr_trap_desc[0], q_addr_hi, <<8, q_num, max_/**/NFP_TRAP_Q_ENTRY_SZ_LW], indirect_ref, ctx_swap[q_sig]

    ov_clean()
.end
#endm // nfp_trap_source


/* EAS_pcie_x8: Table 2.13. PCIe Target Trap Descriptor Format
 * Note: bit 63 -> bit 31 @ 0x04
 *       3                   2                   1
 *     1 0 9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0
 *    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * 00 | data_ref[11:0]        |   length  |pcie | dmstr |  sig_ref    |
 *    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * 04 | page        | page_offset           | function      |bar|w|da-|
 *    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 */
#define TRAP_DESC_DREF_1_bf         0, 31, 20
#define TRAP_DESC_LENGTH_bf         0, 19, 14
#define TRAP_DESC_PCIE_ISL_bf       0, 13, 11
#define TRAP_DESC_DATA_MASTER_bf    0, 10, 7
#define TRAP_DESC_SIGNAL_REF_bf     0, 6, 0
#define TRAP_DESC_SIGNAL_CTX_bf     0, 6, 4
#define TRAP_DESC_SIGNAL_NUM_bf     0, 3, 0
#define TRAP_DESC_PAGE_bf           1, 31, 25
#define TRAP_DESC_PAGE_OFFSET_bf    1, 24, 13
#define TRAP_DESC_FUNC_bf           1, 12, 5
#define TRAP_DESC_BAR_bf            1, 4, 3
#define TRAP_DESC_WRnRD_bf          1, 2, 2
#define TRAP_DESC_DREF_0_bf         1, 1, 0
#define TRAP_DESC_PCI_QUEUE_SEL_bf  2, 31, 16
#macro nfp_trap_proc(out_addr_hi, out_addr_lo, in_xfr_trap_desc)
.begin
    .reg bar
    .reg page
    .reg page_off
    .reg addr_off
    .reg vq_off
    .reg mac_off

    bitfield_extract__sz1(bar, BF_AML(in_xfr_trap_desc, TRAP_DESC_BAR_bf))
    bitfield_extract__sz1(page, BF_AML(in_xfr_trap_desc, TRAP_DESC_PAGE_bf))
    bitfield_extract(page_off, BF_AML(in_xfr_trap_desc, TRAP_DESC_PAGE_OFFSET_bf))
    move(out_addr_hi, 0)
    move(out_addr_lo, 0)
    move(addr_off, 0)

    .if (bar == NFP_TRAP_BAR)
        .if (page == 0)

            immed40[out_addr_hi, out_addr_lo, NFP_TRAP_CFG]

            // map host addresses to NFP addresses
            .if (page_off == NT_BYTES_0)
                move(addr_off, NFP_BYTES_0)
            .elif (page_off == NT_BYTES_1)
                move(addr_off, NFP_BYTES_1)
            .elif (page_off == NT_BYTES_2)
                move(addr_off, NFP_BYTES_2)
            .elif (page_off == NT_BYTES_3)
                move(addr_off, NFP_BYTES_3)
            .elif (page_off == NT_WORDS_0)
                move(addr_off, NFP_WORDS_0)
            .elif (page_off == NT_WORDS_1)
                move(addr_off, NFP_WORDS_1)
            .elif (page_off == NT_DWORDS_0)
                move(addr_off, NFP_DWORDS_0)
            .elif (page_off == NT_DWORDS_1)
                move(addr_off, NFP_DWORDS_1)
            .endif

            alu[out_addr_lo, out_addr_lo, +, addr_off]

        .endif
    .endif

.end
#endm // nfp_trap_proc


#macro nfp_trap_sink(in_addr_hi, in_addr_lo, in_xfr_trap_desc)
.begin

    .reg wr_nrd
    .reg length
    .reg pcie_isl
    .reg data_master
    .reg sig_ctx
    .reg sig_num
    .reg data_ref
    .reg page_off
    .reg wr_data
    .reg $xfr_data[NFP_TRAP_Q_ENTRY_SZ_LW]
    .xfer_order $xfr_data
    .sig sig_write
    .sig sig_read

    // Ignore invalid address
    alu[--, --, B, in_addr_hi]
    beq[nfp_trap_sink_err#]

    bitfield_extract__sz1(wr_nrd, BF_AML(in_xfr_trap_desc, TRAP_DESC_WRnRD_bf))
    bitfield_extract__sz1(length, BF_AML(in_xfr_trap_desc, TRAP_DESC_LENGTH_bf))

    /* only used for read */
    bitfield_extract__sz1(pcie_isl, BF_AML(in_xfr_trap_desc, TRAP_DESC_PCIE_ISL_bf))
    bitfield_extract__sz1(data_master, BF_AML(in_xfr_trap_desc, TRAP_DESC_DATA_MASTER_bf))
    //bitfield_extract__sz1(sig_ref, BF_AML(in_xfr_trap_desc, TRAP_DESC_SIGNAL_REF_bf))
    bitfield_extract__sz1(sig_ctx, BF_AML(in_xfr_trap_desc, TRAP_DESC_SIGNAL_CTX_bf))
    bitfield_extract__sz1(sig_num, BF_AML(in_xfr_trap_desc, TRAP_DESC_SIGNAL_NUM_bf))

    alu[data_ref, BF_A(in_xfr_trap_desc, TRAP_DESC_DREF_0_bf), AND, ((1<<(BF_WIDTH(TRAP_DESC_DREF_0_bf)))-1)]
    alu[data_ref, --, B, data_ref, <<(BF_WIDTH(TRAP_DESC_DREF_1_bf))]
    alu[data_ref, data_ref, OR, BF_A(in_xfr_trap_desc, TRAP_DESC_DREF_1_bf), >>(BF_L(TRAP_DESC_DREF_1_bf))]

    bitfield_extract(page_off, BF_AML(in_xfr_trap_desc, TRAP_DESC_PAGE_OFFSET_bf))

    .if (wr_nrd == 1)

        /**@todo explain byte arrangement with examples*/
        /*
         * 0x0000000200:  0x0010e040 0x00000006 0x0000001f 0x00800080
         * 0x0000000210:  0x0810e041 0x00002006 0x0000ed00 0x00800084
         * 0x0000000220:  0x1010e042 0x00004006 0x00cb0000 0x00800088
         * 0x0000000230:  0x1810e043 0x00006006 0x9a000000 0x0080008c
         */
        .if (page_off == NT_BYTES_0)
            ld_field_w_clr[wr_data, 1000, in_xfr_trap_desc[2], <<24]
            move($xfr_data[0], wr_data)
        .elif (page_off == NT_BYTES_1)
            ld_field_w_clr[wr_data, 1000, in_xfr_trap_desc[2], <<16]
            move($xfr_data[0], wr_data)
        .elif (page_off == NT_BYTES_2)
            ld_field_w_clr[wr_data, 1000, in_xfr_trap_desc[2], <<8]
            move($xfr_data[0], wr_data)
        .elif (page_off == NT_BYTES_3)
            ld_field_w_clr[wr_data, 1000, in_xfr_trap_desc[2]]
            move($xfr_data[0], wr_data)
        .elif (page_off == NT_WORDS_0)
            ld_field_w_clr[wr_data, 1100, in_xfr_trap_desc[2], <<16]
            move($xfr_data[0], wr_data)
        .elif (page_off == NT_WORDS_1)
            ld_field_w_clr[wr_data, 1100, in_xfr_trap_desc[2]]
            move($xfr_data[0], wr_data)
        .elif (page_off == NT_DWORDS_0)
            move($xfr_data[0], in_xfr_trap_desc[2])
        .elif (page_off == NT_DWORDS_1)
            move($xfr_data[0], in_xfr_trap_desc[2])
        .else
            move($xfr_data[0], in_xfr_trap_desc[2])
        .endif

        // write data
        //aggregate_copy($xfr_data, 0, in_xfr_trap_desc, 2, (NFP_TRAP_Q_ENTRY_SZ_LW-2))
        aggregate_directive(.set, $xfr_data, 1, (NFP_TRAP_Q_ENTRY_SZ_LW-1))

        ov_start(OV_LENGTH)
        ov_set(OV_LENGTH, length)
        ov_use()

        mem[write8, $xfr_data[0], in_addr_hi, <<8, in_addr_lo, max_/**/NFP_TRAP_Q_ENTRY_SZ_LW], indirect_ref, ctx_swap[sig_write]

        ov_clean()

    .else

        // Make sure the read is 4-byte aligned. This should fill the transfer
        // register with the 4 bytes that follow the read address regardless of
        // the read length. If the data of interest is in the expected position
        // then the read should complete successfully.
        alu[in_addr_lo, in_addr_lo, AND~, 0x3]

        /* Issue indirect read to target specified by trap descriptor. */
        ov_start((OV_LENGTH | OV_MASTER_ISLAND | OV_DATA_MASTER | OV_DATA_REF | OV_SIGNAL_MASTER | OV_SIGNAL_CTX | OV_SIGNAL_NUM))

        ov_set_bits(OV_MASTER_ISLAND, 0b111000, 0b000000) // Clear top 3 bits
        ov_set(OV_MASTER_ISLAND, 2, 0, pcie_isl) // set bits[2:0] to pcie_isl

        ov_set(OV_DATA_MASTER, data_master)
        ov_set(OV_DATA_REF, data_ref)
        ov_set(OV_SIGNAL_MASTER, data_master)
        //ov_set(OV_SIGNAL_REF, sig_ref)
        ov_set(OV_SIGNAL_CTX, sig_ctx)
        ov_set(OV_SIGNAL_NUM, sig_num)

        ov_set(OV_LENGTH, length)

        ov_use()
        mem[read8, $xfr_data[0], in_addr_hi, <<8, in_addr_lo, max_/**/NFP_TRAP_Q_ENTRY_SZ_LW], indirect_ref, sig_done[sig_read]
        ov_clean()

        .io_completed sig_read // read completed on remote target

    .endif

    br[nfp_trap_sink_end#]

nfp_trap_sink_err#:
    dbg_jnl_log(0xbad, 1)

nfp_trap_sink_end#:

.end
#endm // nfp_trap_sink

nfp_trap_alloc()
dbg_jnl_alloc()
nfp_trap_init(PF)
dbg_jnl_init()

main#:

.begin

    .reg trap_addr_hi
    .reg trap_addr_lo
    .reg $pcie_trap_desc[NFP_TRAP_Q_ENTRY_SZ_LW]
    .xfer_order $pcie_trap_desc
    .sig q_sig

    /* Keep things a little simpler by using a single thread. */
    .if (ctx() != 0)
        ctx_arb[kill]
    .endif

    .while (1)

        nfp_trap_source($pcie_trap_desc)
        dbg_jnl_log($pcie_trap_desc, 3)

        nfp_trap_proc(trap_addr_hi, trap_addr_lo, $pcie_trap_desc)
        dbg_jnl_log(trap_addr_lo, 1)

        nfp_trap_sink(trap_addr_hi, trap_addr_lo, $pcie_trap_desc)

    .endw // (1)

.end // nfp_virtio_leg

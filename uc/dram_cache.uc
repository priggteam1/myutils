#define MUCFG                   0x3504000   // Memory unit configuration address in SHaC XPB address map
#define MUConfigCPP             0           // CPP MU Configuration Register
#define MUConfigDrams           0x14        // MUConfigDrams Register

#define MUTcache0               0x3540000   // Memory unit tag cache bank 0 in SHaC XPB address map
#define MUTcacheECC0            0x3544000   // Memory unit tag cache bank ECC 0 in SHaC XPB address map
#define MUTcache1               0x3548000   // Memory unit tag cache bank 1 in SHaC XPB address map
#define MUTcacheECC1            0x354c000   // Memory unit tag cache bank ECC 1 in SHaC XPB address map
#define MUTcache2               0x3550000   // Memory unit tag cache bank 2 in SHaC XPB address map
#define MUTcacheECC2            0x3554000   // Memory unit tag cache bank ECC 2 in SHaC XPB address map
#define MUTcache3               0x3558000   // Memory unit tag cache bank 3 in SHaC XPB address map
#define MUTcacheECC3            0x355c000   // Memory unit tag cache bank ECC 3 in SHaC XPB address map
#define MUTcache4               0x3560000   // Memory unit tag cache bank 4 in SHaC XPB address map
#define MUTcacheECC4            0x3564000   // Memory unit tag cache bank ECC 4 in SHaC XPB address map
#define MUTcache5               0x3568000   // Memory unit tag cache bank 5 in SHaC XPB address map
#define MUTcacheECC5            0x356c000   // Memory unit tag cache bank ECC 5 in SHaC XPB address map
#define MUTcache6               0x3570000   // Memory unit tag cache bank 6 in SHaC XPB address map
#define MUTcacheECC6            0x3574000   // Memory unit tag cache bank ECC 6 in SHaC XPB address map
#define MUTcache7               0x3578000   // Memory unit tag cache bank 7 in SHaC XPB address map
#define MUTcacheECC7            0x357c000   // Memory unit tag cache bank ECC 7 in SHaC XPB address map

#define MUTCacheConfig          0           // MU Tag Cache Bank Configuration Register
#define MUTCacheConfigClean     4           // MU Tag Cache Bank Configuration Register
#define MUTCacheCommandStatus   8           // Tag Bank N Command Status Register: Aborted, Valid, IntfErr
#define MUTCacheCommandControl  0xC         // Tag Bank N Command Control Register; WO Set, Ways, Type
#define MUTCacheWriteData       0x10        // Tag Bank N Write Data Register: State (way marked as locked, etc.), tag
#define MUTCacheCommandResponse 0x14        // Tag Bank N Command Response Register
#define MUTCacheReadData        0x18        // Tag Bank N Read Data Register: state, tag

#define MUPCtl0                 0x3508000   // Memory unit DRAM controller 0 in SHaC XPB address map
#define MUPCtl1                 0x350c000   // Memory unit DRAM controller 1 in SHaC XPB address map

// Table A.23. Memory Unit PCTL
#define MUPCtlSCfg              0           // State Configuration Register
#define MUPCtlSCtl              4           // State Control Register
#define MUPCtlStat              8           // State Status Register

// SCTL.state_cmd Values
#define SCTL_CFG                1
#define SCTL_GO                 2

// STAT.ctl_stat Values
#define STAT_config             1
#define STAT_access             3

// Type of reference
#define TYPE_HLR                0           // High locality of reference
#define TYPE_LLR                1           // Low locality of reference data
#define TYPE_DIRECT_ACCESS      2           // Direct access
#define TYPE_DISCARD            3           // Discard after read

#macro init_dram_cache()
// Set up to map 256K bytes = 4,096 64-byte cache lines/8 cache tag engines = 512 sets of one Way.
.begin
    .reg map_address num_addresses
    
    // Move DRAM controller 0 to Config state.  Write CFG to SCTL.state_cmd and poll STAT.ctl_stat = Config
    set_config_state(MUPCtl0)
    
    // Move DRAM controller 1 to Config state.  Write CFG to SCTL.state_cmd and poll STAT.ctl_stat = Config
    set_config_state(MUPCtl1)
    
    // Allow Way0 of the cache to direct access
    // When bit n in this field is set to 1, permit MU commands to direct access that way of the cache
    allow_way_direct_access(1)
    // the cache memory location should be mapped in the cache tag array to a locked location that is never accessed; 
    // this will prevent eviction of the cache line and dual use of the cache memory
    // Use address starting at 1 F000 0000 for locked location that is never used
    // Shift this right 6 to give 64-byte cache line starting at 7C00000
    // Shift this right 3 for 8 cache tage engines to give map_address starting at F80000 for each cache tag engine
    move(map_address, 0xF80000) 
    
   //move(num_addresses, 512)
    move(num_addresses, 8)  // This one breaks after a while
   // move(num_addresses, 16)  // This one breaks after a while
    //move(num_addresses, 64) // this one breaks
map_loop#: // one loop for each Set   
    lock_dram_cache(MUTcache0, map_address) // map_address will be incremented by 1 for each cache line locked
    lock_dram_cache(MUTcache1, map_address)
    lock_dram_cache(MUTcache2, map_address)
    lock_dram_cache(MUTcache3, map_address)
    lock_dram_cache(MUTcache4, map_address)
    lock_dram_cache(MUTcache5, map_address)
    lock_dram_cache(MUTcache6, map_address)
    lock_dram_cache(MUTcache7, map_address)
    
    // Next map_address
    alu[map_address, map_address, +, 1]
    alu[num_addresses, num_addresses, -, 1]
    bne[map_loop#]
    
    // Write GO to DRAM controller 0 SCTL.state_cmd and poll STAT.ctl_stat = Access.
    set_acces_state(MUPCtl0)
    
    // Write GO to DRAM controller 1 SCTL.state_cmd and poll STAT.ctl_stat = Access.
    set_acces_state(MUPCtl1)
            
.end
#endm // init_dram_cache

#macro set_config_state(MUPCtl)    
.begin
// Move DRAM controller 1 or 0 to Config state 
    .reg $cap_data  cap_base_address
    .sig sig_cap

    // Verify that we are in Access state
    move(cap_base_address, MUPCtl)
    cap[read, $cap_data, cap_base_address, MUPCtlStat, 1], ctx_swap[sig_cap] 
    alu[--, $cap_data, -, STAT_access]
    beq[set_state#]
    journal_trace(0xFFFFFF10, $cap_data) // not expected
    br[done#]
    
set_state#:
    move($cap_data, SCTL_CFG)
    cap[write, $cap_data, cap_base_address, MUPCtlSCtl, 1], ctx_swap[sig_cap] 
        
poll_state#:        
    cap[read, $cap_data, cap_base_address, MUPCtlStat, 1], ctx_swap[sig_cap] 
    alu[--, $cap_data, -, STAT_config]
    bne[poll_state#]
done#:
.end    
#endm //  set_config_state   

#macro set_acces_state(MUPCtl)    
.begin
// Move DRAM controller 1 or 0 to Access state.
    .reg $cap_data  cap_base_address
    .sig sig_cap

    // Verify that we are in Config state
    move(cap_base_address, MUPCtl)
    cap[read, $cap_data, cap_base_address, MUPCtlStat, 1], ctx_swap[sig_cap] 
    alu[--, $cap_data, -, STAT_config]
    beq[set_state#]
    journal_trace(0xFFFFFF20, $cap_data) // not expected
    br[done#]

set_state#:
    move($cap_data, SCTL_GO)
    cap[write, $cap_data, cap_base_address, MUPCtlSCtl, 1], ctx_swap[sig_cap] 
        
poll_state#:        
    cap[read, $cap_data, cap_base_address, MUPCtlStat, 1], ctx_swap[sig_cap] 
    alu[--, $cap_data, -, STAT_access]
    bne[poll_state#]
done#:
.end    
#endm //  set_acces_state   

#macro allow_way_direct_access(way_mask)    
.begin
    .reg $cap_data cap_base_address 
    .sig sig_cap

    move(cap_base_address, MUCFG)
    cap[read, $cap_data, cap_base_address, MUConfigCPP, 1], ctx_swap[sig_cap] 
    alu[$cap_data, $cap_data, OR, way_mask, <<8] 
    cap[write, $cap_data, cap_base_address, MUConfigCPP, 1], ctx_swap[sig_cap] 
.end
#endm //  allow_way_direct_access   
    
#macro lock_dram_cache(MUTcache, map_address)  
// the cache memory location should be mapped in the cache tag array to a locked location that is never accessed; 
// this will prevent eviction of the cache line and dual use of the cache memory
/*
Bit  3 3 3 2 2 2 2 2 2 2 2 2 2 1 1 1 1 1 1 1 1 1 1 0 0 0 0 0 0 0 0 0 0 
     2 1 0 9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0 
    +-|-+-+-+-+-+-+-+-|-+-+-+-+-+-+-+-|-+-+-+-+-+-+-+-|-+-+-+-+-+-+-+-|
      |                        S S S S S S S S S     B B B            |
     Y|Y Y Y Y Y Y Y Y Y Y Y Y                   X X                  |
    --|---------------------------------------------------------------|
    The above shows 33 bits of address
    BBB is bank: 8 cache tag engines 
    XX go to tag bits 0..1
    SSSSSSSSS specify Set bits to go to bits 16..24 of MUTCacheCommandControl
    YYYYYYYYYYYYY go to tag bits 2..14
Bit 3 3 3 2 2 2 2 2 2 2 2 2 2 1 1 1 1 1 1 1 1 1 1 0 0 0 0 0 0 0 0 0 0 
    1 0 9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0 
   |-+-+-+-+-+-+-+-|-+-+-+-+-+-+-+-|-+-+-+-+-+-+-+-|-+-+-+-+-+-+-+-|    Tag 15 bits 
   |                         |State| |        Tag                  |                                                               |
   |                         |     | |Y Y Y Y Y Y Y Y Y Y Y Y Y X X|                                                               |
   |---------------------------------------------------------------|
   The above shows the Tag that is written to MUTCacheWriteData
*/
.begin
    .reg $cap_data cap_base_address command_control command_control_read command_control_write $command_control set tag_low_bits poll_count  
    .reg $cap_data_before $cap_data_after $cap_status $cap_command_response
    .reg cap_write_data
    .sig sig_cap
    #define POLL_COUNT_MAX 20 // keep increasing this until we see command complete

    // Set up MUTCacheCommandControl Register.  This specifies which set we are writing and which way.
    move(cap_base_address, MUTcache)
    alu[command_control, --, B, 1, <<8] // Write to way 0
    move(mask, 0x1FF) // 9 bits for set
    alu[set, mask, AND, map_address, >>2] // SSSSSSSSS
    alu[command_control, command_control, OR, set, <<16] // set in bits 24..16; 1 in bit 8 to specify way 0; 4 in low bits to specify write command
    alu[command_control_read, command_control, OR, 5] // 5 to Read data from selected way of set
    alu[command_control_write, command_control, OR, 4] // 4 to Write data to enabled ways of set
    
#if 1
    // Read and save value before we remap
    move($command_control, command_control_read)
    cap[write, $command_control, cap_base_address, MUTCacheCommandControl, 1], ctx_swap[sig_cap] 
    cap[read, $cap_data_before, cap_base_address, MUTCacheReadData, 1], ctx_swap[sig_cap] 
#endif
    
    move($command_control, command_control_write)
    cap[write, $command_control, cap_base_address, MUTCacheCommandControl, 1], ctx_swap[sig_cap] 
    count32(NUM_WRITE_CACHE_CONTROL)
    
    // Set up to poll until we see the command has completed.
    move(poll_count, POLL_COUNT_MAX)
poll_command#:    
    alu[poll_count, poll_count, -, 1]
    bne[check_command_status#]
    count32(CACHE_WRITE_CONTROL_TIMEOUT]
    br[done#]
    
check_command_status#:    
    // Check the status of the command
    cap[read, $cap_status, cap_base_address, MUTCacheCommandStatus, 1], ctx_swap[sig_cap] 
    count32(NUM_READ_CACHE_CONTROL_STATUS)
    br_bclr[$cap_status, 0, check_abort#] // Check the error flag in Bit 0: Asserted if an error occured in a tcache command; cleared by a read
    count32(CACHE_WRITE_CONTROL_ERROR]
    br[done#]

check_abort#:
    br_bclr[$cap_status, 2, check_completed#] // Check the abort flag in Bit 2: Indicates a command aborted; cleared by a read
    count32(CACHE_WRITE_CONTROL_ABORT]
    br[done#]
    
check_completed#:    
    br_bclr[$cap_status, 1, poll_command#] // Check the command completed flag in Bit 1: Asserted when a tcache command has completed; cleared by a read
    count32(NUM_WRITE_CACHE_CONTROL_GOOD)

    // Set up tag data to write to way 0 of set for write command
    // State 3 bits starting at 16 = 0 to lock this cache line
    alu[cap_write_data, --, B, map_address, >>11] // YYYYYYYYYYYYY
    alu[tag_low_bits, 3, AND, map_address] // XX
    alu[cap_write_data, tag_low_bits, OR, cap_write_data, <<2] //YYYYYYYYYYYYYXX
    alu[$cap_data, --, B, cap_write_data] // YYYYYYYYYYYYYXX
    cap[write, $cap_data, cap_base_address, MUTCacheWriteData, 1], ctx_swap[sig_cap] 
    count32(NUM_WRITE_LOCK_DATA)

    // Set up to poll until the command has completed.
    move(poll_count, POLL_COUNT_MAX)
poll_write#:    
    alu[poll_count, poll_count, -, 1]
    bne[check_write_status#]
    count32(CACHE_WRITE_LOCK_TIMEOUT]
    br[done#]

check_write_status#:
    // Check the status of the command
    cap[read, $cap_command_response, cap_base_address, MUTCacheCommandResponse, 1], ctx_swap[sig_cap] 
    count32(NUM_READ_LOCK_DATA_STATUS)
#if 1
    // Read value after we remap
    move($command_control, command_control_read)
    cap[write, $command_control, cap_base_address, MUTCacheCommandControl, 1], ctx_swap[sig_cap] 
    cap[read, $cap_data_after, cap_base_address, MUTCacheReadData, 1], ctx_swap[sig_cap] 
    journal_trace(0xBBBBBB30, $cap_data_before, command_control_write, cap_write_data, $cap_command_response, $cap_data_after)
#endif
    br_bset[$cap_command_response, 31, cache_response_good#]
    count32(CACHE_WRITE_DATA_ERROR]
    br[done#]
cache_response_good#:    
    count32(NUM_WRITE_LOCK_DATA_GOOD)
    
.end    
done#:
#endm // lock_dram_cache    

#macro copy_rave_to_dram_cache()
.begin
    xbuf_alloc($rave_data, 16, read_write)
    .reg source_offset dest_offset rave_copy_size indirect_ref_reg
    .sig sig_rave_dram sig_rave_cls sig_code_size sig_node
    .reg ctx_status
    .reg max_cache_pc cluster cluster_bit $cluster_loaded_flags
    .reg num_nodes  source_state_counters_address direct_access_mode
    #define RAVE_COPY_BURST_SIZE 8

    // LM1 points to StateSource
    // See if we have a node map
    get_num_nodes(num_nodes)
    beq[no_nodes#]
    copy_tuneup_to_cluster_scratch(num_nodes)
    br[copy_to_cache_done#]

no_nodes#:
    init_dram_cache()
    // Read the size of the application to load.  The size is stored here by the Loader.
    move(source_offset, RAVE_CODE_SIZE_BYTES)
    scratch[read, $rave_data[0], source_offset, 0, 1], ctx_swap[sig_code_size]

    // Not to read in more than the size of the cache
    move(rave_copy_size, $rave_data[0])
    move(max_cache_pc, RAVE_DRAM_CACHE_SIZE) // ts_max_cache_pc is not available here
    //journal_trace(0xAAAAAA23, rave_copy_size)
    .if(rave_copy_size > max_cache_pc)
        move(rave_copy_size, max_cache_pc)
    .endif

    // Round up the size to a number of 64-byte bursts
    alu[rave_copy_size, rave_copy_size, +, 64]
    alu[rave_copy_size, rave_copy_size, AND~, 63]

    move(source_offset, DEPLOY_RAVE_CODE_BASE)
    move(dest_offset, RAVE_DRAM_CACHE_BASE)
    trace_get_packet(0xCCCCCC23, $rave_data[0], rave_copy_size, source_offset, dest_offset)

    move(direct_access_mode, DIRECT_ACCESS_ADDRESS) // The top two bits of a 40-bit memory unit address are '10' for direct access
copy_next_chunk#:
    //trace_get_packet(0xAAAAAA24, source_offset, dest_offset, rave_copy_size, max_cache_pc)
    mem[read, $rave_data[0], source_offset, 0, RAVE_COPY_BURST_SIZE], sig_done[sig_rave_dram]
    ctx_arb[sig_rave_dram]
    alu[$rave_data[0], --, B, $rave_data[0]]
    alu[$rave_data[1], --, B, $rave_data[1]]
    alu[$rave_data[2], --, B, $rave_data[2]]
    alu[$rave_data[3], --, B, $rave_data[3]]
    alu[$rave_data[4], --, B, $rave_data[4]]
    alu[$rave_data[5], --, B, $rave_data[5]]
    alu[$rave_data[6], --, B, $rave_data[6]]
    alu[$rave_data[7], --, B, $rave_data[7]]
    alu[$rave_data[8], --, B, $rave_data[8]]
    alu[$rave_data[9], --, B, $rave_data[9]]
    alu[$rave_data[10], --, B, $rave_data[10]]
    alu[$rave_data[11], --, B, $rave_data[11]]
    alu[$rave_data[12], --, B, $rave_data[12]]
    alu[$rave_data[13], --, B, $rave_data[13]]
    alu[$rave_data[14], --, B, $rave_data[14]]
    alu[$rave_data[15], --, B, $rave_data[15]]
    //journal_trace(0xAAAAAA40, $rave_data[0], source_offset)

    // Reference count in 4-byte words. Valid values are 1-32. Values
    // above 8 must be specified using indirect reference
#if 1    
    mem[write, $rave_data[0], dest_offset, direct_access_mode, <<8, 8], sig_done[sig_rave_dram]
    ctx_arb[sig_rave_dram]
    alu[source_offset, source_offset, +, (8*RAVE_COPY_BURST_SIZE)]
    alu[dest_offset, dest_offset, +, (8*RAVE_COPY_BURST_SIZE)]
    #if 0
        .reg offset
        alu[offset, dest_offset, -, (8*RAVE_COPY_BURST_SIZE)]
        cls[read, $rave_data[0], offset, 0, 8], ctx_swap[sig_rave_cls]
        journal_trace(0xAAAAAA00, offset, $rave_data[0], $rave_data[1], $rave_data[2], $rave_data[3] )
        journal_trace(0xAAAAAA01, $rave_data[4], $rave_data[5], $rave_data[6], $rave_data[7], $rave_data[8] )
        journal_trace(0xAAAAAA02, $rave_data[9], $rave_data[10], $rave_data[11], $rave_data[12], $rave_data[13] )
        journal_trace(0xAAAAAA03, $rave_data[14], $rave_data[15] )
    #endif
    trace_get_packet(0xBBBBBB22, $rave_data[0], $rave_data[1])
    alu[rave_copy_size, rave_copy_size, -, (8*RAVE_COPY_BURST_SIZE)]
    bgt[copy_next_chunk#]
#endif    
    //journal_trace(0xAAAAAA24, source_offset, dest_offset, max_cache_pc)
    xbuf_free($rave_data)
copy_to_cache_done#:
.end
#endm // copy_rave_to_dram_cache


